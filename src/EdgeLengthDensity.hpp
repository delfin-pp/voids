/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/EdgeLengthDensity.hpp
 * 
 */
#ifndef __EDGELENGTHDENSITY__H
#define __EDGELENGTHDENSITY__H
#include "EdgeDensity.hpp"
using namespace std;

/**
 * @brief class for the density of the edges in a 3D mesh
 * by its length and the volume of its sorrounding tetrahedra
 * 
 */
class EdgeLengthDensity : public EdgeDensity {
    public:
        /**
         * @brief Applies the density calculation on the tetrahedral mesh
         * 
         * Gets the number of edges in the mesh and resizes the edgeDensity
         * vector.
         * For every edge gets its length and the vector with its sorrounding 
         * tetrahedra and computes its volume to then compute the density.
         * From the density of every edge, the average density is calculated.
         * 
         * @param tetrahedraDict The dictionary that contains the mesh
         */
        void apply(TetrahedronDictionary*);
};

#endif