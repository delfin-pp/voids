/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/PointDensityBuilder.cpp
 * 
 */
#include "PointDensityBuilder.hpp"
#include "PointVolumeDensity.hpp"
#include "PointLengthDensity.hpp"
#include <algorithm>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include <iomanip>
#include <stack>

using namespace std;

void PointDensityBuilder::analyze(TetrahedronDictionary* tetrahedraDict){
    PointDensity * densityMetric = new PointVolumeDensity();
    analyze(tetrahedraDict, densityMetric, false, false);
}

void PointDensityBuilder::analyze(TetrahedronDictionary* tetrahedraDict, PointDensity* densityMetric, bool discardBorder, bool deletePoorQuality){
    cout << "Analyzing ...\n";
    densityMetric->apply(tetrahedraDict);
    vector<pair<int, float>> pointsDensity = densityMetric->getSortedPoints();
    float thresholdDensity;
    if (wallDensity)
    {
        cout << "wallDensity is ON" << endl;
        thresholdDensity = thresholdPercent * densityMetric->getMeanDensity();
    }
    else
    {
        thresholdDensity = pointsDensity[int(thresholdPercent * pointsDensity.size())].second;
    }
    meanDensity = densityMetric->getMeanDensity();
    int pointsUsed = 0;

    for(vector<pair<int, float>>::iterator it = pointsDensity.begin(); it != pointsDensity.end(); ++it){
        pair<int, float> id_dens = *it;

        if(id_dens.second > thresholdDensity){
            cout << "N points used " << pointsUsed << "\n";
            break;
        } 
        pointsUsed++;

        bool newVoid = true;
        vector<int> tetrahedraId = densityMetric->getPointIdTetrahedra(id_dens.first);
        for(auto f_it = tetrahedraId.begin(); f_it != tetrahedraId.end(); ++f_it){
            Tetrahedron tetrahedron = tetrahedraDict->getById(*f_it);
            if(tetrahedron.getVoidId() != -1) {
                addTetrahedraToVoid(tetrahedraDict, tetrahedraId, tetrahedron.getVoidId());
                newVoid = false;
                break;
            }
        }
        
        if (newVoid){
            VoidSpace * voidSpace = new VoidSpace();
            voidSpace->setTetrahedronDictionary(tetrahedraDict);
            voidSpace->setId(voids.size());
            voids.push_back(voidSpace);
            for(auto f_it = tetrahedraId.begin(); f_it != tetrahedraId.end(); ++f_it){
                voidSpace->addTetrahedronId(*f_it);
            }
        }

    }

    discardVoids(discardBorder, deletePoorQuality);
}

void PointDensityBuilder::printVoidsPoints(string filename, TetrahedronDictionary *tetrahedraDict, PointDensity *densityMetric){
    string outputFile = filename+string("_void_points.dat.out");
	ofstream ofs(outputFile.c_str());
	PointDictionary *p = tetrahedraDict->getPointDictionary();
	if(ofs.good()){
		int n_voids = voids.size();
		ofs << "# void ID, particle ID, x, y, z, density" << "\n";
		for (int i = 0; i < n_voids; i++)
		{
			vector<int> points = voids[i]->getPointsId();
            int vid = voids[i]->getId();
			for(vector<int>::iterator it = points.begin(); it !=points.end(); ++it){
				float * point_aux = p->getById(*it);
				ofs << vid << " " 
                    << (*it) << " " 
                    << point_aux[0] << " " 
                    << point_aux[1] << " " 
                    << point_aux[2] << " " 
                    << densityMetric->getPointIdDensity((*it)) << "\n";
			}
		}
	}
}