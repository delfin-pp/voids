/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/EdgeDictionary.hpp
 * 
 */
#ifndef __EDGEDICTIONARY__H
#define __EDGEDICTIONARY__H
#include <iostream>
#include "Dictionary.hpp"
#include "PointDictionary.hpp"
#include "Edge.hpp"
#include <map>
#include <utility>
#include <vector>
using namespace std;

/**
 * @brief class for a dictionary of edges
 * 
 */
class EdgeDictionary : public Dictionary<Edge>{
	private:
		map< pair< int, int> , int > edgesDict1; /* Map with pair of points of the edge and the id of the edge */
		vector< pair< int , int > > edgesDict2; /* Vector with pairs of points that are joined by the edge */
		vector< vector< int> > edgesTetrahedraId; /* Matrix with id of each tetrahedron of the edge */
		PointDictionary* pointsDict;
		vector< int > inBorder; /* Vector that indicates if an edge is in the border of the 3D mesh*/
	public:
		/**
		 * @brief Construct a new Edge Dictionary object
		 * 
		 */
		EdgeDictionary();
		/**
		 * @brief Destroy the Edge Dictionary object
		 * 
		 */
		~EdgeDictionary();
		/**
		 * @brief Adds and edge to the dictionary
		 * 
		 * @note unused/unimplemented method
		 * 
		 * @param _edge Edge to add to the dictionary
		 * @return int Id assigned to the edge
		 */
		int add(Edge*);
		/**
		 * @brief Adds and edge to the dictionary
		 * 
		 * First, the method sorts the points ids so that the lower point id
		 * is the first value of a pair. Then, searches the pair inside the 
		 * keymap edgesDict1. If the pair is found, the edge already exists,
		 * and the only action left is to add the tetrahedron id to
		 * the edge in edgesTetrahedraId. If the pair isn't found, the edge is
		 * added in edgesDict1, edgesDict2, and edgesTetrahedraId, where the
		 * tetrahedronId is added as the first tetrahedron related to that edge.
		 * 
		 * 
		 * @param x Id of the first point of the edge
		 * @param y Id of the second point of the edge
		 * @param tetrahedronId Id of a tetrahedron that has the edge (x, y)
		 * @return int Id assigned to the edge
		 */
		int add(int,int,int);
		/**
		 * @brief 
		 * 
		 * @note Void method
		 * 
		 * @param filename 
		 * 
		 */
		void load(string){};
		/**
		 * @brief Get an Edge by its id in the dictionary
		 * 
		 * @return Edge Object searched for
		 */
		Edge getById(int);
		/**
		 * @brief Set the Point Dictionary
		 * 
		 * @param _pointsDict Reference to the Dictionary of points
		 */
		void setPointDictionary(PointDictionary*);
		/**
		 * @brief Set the inBorder value for an edge
		 * 
		 * @param _id Id of the edge
		 * @param _inBorder inBorder value to set
		 */
		void setInBorder(int, int);
};




#endif