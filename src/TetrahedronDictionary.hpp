/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/TetrahedronDictionary.hpp
 * 
 */
#ifndef __TETRAHEDRONDICTIONARY__H
#define __TETRAHEDRONDICTIONARY__H
#include <iostream>
#include "Dictionary.hpp"
#include "Tetrahedron.hpp"
#include "PointDictionary.hpp"
#include "EdgeDictionary.hpp"
#include <fstream>
using namespace std;

/**
 * @brief class for a dictionary of tetrahedra
 * 
 */
class TetrahedronDictionary : public Dictionary<Tetrahedron>{
	private:
		int** tetrahedraNeighboursId; /* Matrix with id of each neighboring tetrahedron (nx4) */
		int** tetrahedraEdgesId; /* Matrix with id of each edge of the tetrahedron (nx6) */
		int** tetrahedraPointsId; /* Matrix with id of each point of the tetrahedron (nx4) */
		int* tetrahedraVoidId; /* Vector with the id of the void of which the tetrahedron is part of (n) */
		EdgeDictionary* edgesDict;
		PointDictionary* pointsDict;
		/**
		 * @brief Reads local file with ids of neighbours and loads it to the neighbours
		 * matrix
		 * 
		 * @param input_neighbours Name of the file with ids of neighbours per tetrahedron
		 * @param number Number of tetrahedrons
		 */
		void loadNeighbours(string,int);
		/**
		 * @brief Reads local file with vertex ids and loads data to the edges and
		 * points matrices
		 * 
		 * @param input_vertexes Name of the file with ids of vertexes per tetrahedron
		 * @param number Number of tetrahedrons
		 */
		void loadVertexesAndEdges(string,int);
	public:
		/**
		 * @brief Construct a new Tetrahedron Dictionary object
		 * 
		 */
		TetrahedronDictionary();
		/**
		 * @brief Destroy the Tetrahedron Dictionary object
		 * 
		 */
		~TetrahedronDictionary();
		/**
		 * @brief Reads local files and loads its content into the dictionary
		 * 
		 * Reads input files, creates the matrices and vectors with the correct dimensions
		 * as indicated by the files.
		 * 
		 * 
		 * @param input_file Prefix of the name of the files to load the dictionary 
		 */
		void load(string);
		/**
		 * @brief Get a Tetrahedron by its id in the dictionary
		 * 
		 * @return Tetrahedron Object searched for
		 */
		Tetrahedron getById(int);
		/**
		 * @brief Set the Point Dictionary
		 * 
		 * @param _pointsDict Reference to the Dictionary of points
		 */
		void setPointDictionary(PointDictionary*);
		/**
		 * @brief Get the Point Dictionary
		 * 
		 * @return PointDictionary* Reference to the Dictionary of points
		 */
		PointDictionary* getPointDictionary();
		/**
		 * @brief Set the Edge Dictionary
		 * 
		 * @param _edgesDict Reference to the Dictionary of edges
		 */
		void setEdgeDictionary(EdgeDictionary*);
		/**
		 * @brief Get the Edge Dictionary
		 * 
		 * @return EdgeDictionary* Reference to the Dictionary of edges
		 */
		EdgeDictionary* getEdgeDictionary();
		/**
		 * @brief Adds a tetrahedron to the dictionary
		 * 
		 * @note unused method
		 * 
		 * @param _tetrahedron Tetrahedron to add to the dictionary
		 * @return int Id assigned to the tetrahedron
		 */
		int add(Tetrahedron*);
		/**
		 * @brief Set the void id for a certain tetrahedron
		 * 
		 * @param tetrahedronId Id of the tetrahedron
		 * @param voidId Id of the void
		 */
		void setTetrahedronVoidId(int,int);
};

#endif