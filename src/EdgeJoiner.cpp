/**
 * DELFIN++ -- Delaunay Edge Void Finder (++) -- ./src/EdgeJoiner.cpp
 * 
 * EdgeJoiner is a class used in a previous version of the DELFIN algorithm
 * and is not used in DELFIN++.
 * Maintained in the files to preserve the completeness of the previous version.
 * 
 */
#include "EdgeJoiner.hpp"
#include <iomanip>
#include <map>
using namespace std;

EdgeJoiner::~EdgeJoiner(){}

EdgeJoiner::EdgeJoiner(float _minEdgeLength, bool _discardBorder):VoidJoiner(){
	minEdgeLength = _minEdgeLength;
	discardBorder = _discardBorder;
}

void EdgeJoiner::join(vector< vector< int > > subvoids, TetrahedronDictionary *tetrahedraDict){
	cout << "Joining...\n";
	EdgeDictionary* edgesDict = tetrahedraDict->getEdgeDictionary();
	
	// Last element will be joined to edge voids
	parent.resize(subvoids.size()+1);
	rank.resize(subvoids.size()+1);
	
	for (uint i = 0; i < subvoids.size()+1; i++) {
		parent[i] = i;
		rank[i] = 0;
	}
	
	cout << "Subvoids before joining: " << subvoids.size() << endl;

	// Union-find over subvoids
	for (int k = 0; k < edgesDict->getDataLength(); k++) {
		vector<int> adjVect = edgesDict->getById(k).getTetrahedraId();
		if (discardBorder && edgesDict->getById(k).isInBorder()) {
			// Subvoid is in border
			for (uint s = 0; s < adjVect.size(); s++) {
				if (tetrahedraDict->getById(adjVect[s]).getVoidId() >= 0) {
				djUnion(tetrahedraDict->getById(adjVect[s]).getVoidId(), subvoids.size());
				}
			}
		}
		else if (edgesDict->getById(k).getLength() > minEdgeLength) {
			// Edge is significant
			uint s;
			
			for (s = 0; s < adjVect.size(); s++) {
				if (tetrahedraDict->getById(adjVect[s]).getVoidId() >= 0) {
					break;
				}
			}
			// Subvoids share a significant edge; join
			for (uint p = s+1; p < adjVect.size(); p++) {
				djUnion(tetrahedraDict->getById(adjVect[s]).getVoidId(), tetrahedraDict->getById(adjVect[p]).getVoidId());
			}
		}
	}

	int border_subvoids = 0;
	map<int,int> parentToId;
	vector< vector< int > > voidsResult;
	for (uint i = 0; i < subvoids.size(); i++) {
		if (djFind(i) == djFind(subvoids.size())) {
			//Subvoid belongs to border void, ignore
			border_subvoids++;
			continue;
		}
		
		map<int,int>::iterator search = parentToId.find(djFind(i));
		if (search == parentToId.end()) {
			//New group
			parentToId.insert(pair<int,int>(djFind(i),voidsResult.size()));
			voidsResult.push_back(vector <int> (subvoids[i]));
		}
		else {
			//Merge with existing group
			voidsResult[search->second].insert(voidsResult[search->second].end(),subvoids[i].begin(),subvoids[i].end());
		}
	}
	cout << "Voids after joining: " << voidsResult.size() << " (" << border_subvoids << " subvoids in border)" << endl;
	
	result = voidsResult;
}

int EdgeJoiner::djFind(int i) {
	if (parent[i] != i) {
		parent[i] = djFind(parent[i]);
	}
	return parent[i];
}

void EdgeJoiner::djUnion(int i, int j) {
	if (i < 0 || j < 0 ) {
		return;
	}
	int pi = djFind(i);
	int pj = djFind(j);
	
	if (pi == pj)
		return;

	if (rank[pi] < rank[pj]) {
		parent[pi] = pj;
	}
	else if (rank[pj] < rank[pi]) {
		parent[pj] = pi;
	}
	else {
		parent[pi] = pj;
		rank[pj]++;
	}
}
