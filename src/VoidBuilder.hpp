/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/VoidBuilder.hpp
 * 
 */
#ifndef __VOIDBUILDER__H
#define __VOIDBUILDER__H
#include <iostream>
#include <vector>
#include "TetrahedronDictionary.hpp"
#include "VoidSpace.hpp"
#define PI 3.1415926
using namespace std;

/**
 * @brief abstract class to identify or build voids from a set of 
 * tetrahedra in a 3D mesh
 * 
 */
class VoidBuilder{
	protected:
		vector<VoidSpace *> voids; /* Set of identified voids as VoidSpace objects */
		vector<vector<int>> result; /* Set of identified voids as vectors of tetrahedra id */
		/**
		 * @brief Construct a new Void Builder object
		 * 
		 */
		VoidBuilder(){};
	public:
		/**
		 * @brief Destroy the Void Builder object
		 * virtual method
		 * 
		 */
		virtual ~VoidBuilder(){};
		/**
		 * @brief Identifies the voids in a set of tetrahedra
		 * virtual method
		 * 
		 * @param tetrahedraDict The dictionary that contains the mesh
		 */
		virtual void analyze(TetrahedronDictionary*) = 0;
		/**
		 * @brief Get the Result vector
		 * 
		 * Gets the result vector, that contains vectors that represents
		 * voids. Each contained vectors has the ids of the tetrahedra that
		 * are part of the void.
		 * 
		 * @return vector< vector< int > > Result vector
		 */
		vector< vector< int > > getResult();
		/**
		 * @brief Writes two OUT files with information of each void
		 * 
		 * Write two OUT files with information of each void.
		 * The first file contains position and general information of the voids,
		 * and is formatted as:
		 * center x,y,z, volume, radius, void ID, density
		 * The second file contains shape information of the voids, and is 
		 * formatted as:
		 * void ID, ellipticity, eig-values, eig-vectorss
		 * 
		 * @param filename String with the prefix of the name of the output OUT file
         * @param tetrahedronDictionary The dictionary that contains the mesh
		 */
		void printVoidInformation(string, TetrahedronDictionary*);
		/**
		 * @brief Write an OUT file with the particles of each void
		 * 
		 * Writes an OUT file of the points of each void, writing the id and the
		 * position of the point.
		 * This file is formatted as:
		 * void ID, particle ID, x, y, z
		 * The first line contains the following string:
		 * "# void ID, particle ID, x, y, z"
		 * 
		 * @param filename String with the prefix of the name of the output OUT file
         * @param tetrahedronDictionary The dictionary that contains the mesh
		 * 
		 */
		void printVoidsPoints(string, TetrahedronDictionary*);
		/**
		 * @brief Writes an OFF file of the voids
		 * virtual method
		 * Writes and OFF file of the polyhedra/voids identified, with a random
		 * color for each void.
		 * 
		 * @param filename String with the prefix of the name of the output OFF file
         * @param tetrahedronDictionary The dictionary that contains the mesh
		 */
		virtual void printResultOFF(string,TetrahedronDictionary*);
};



#endif
