/**
 * DELFIN++ -- Delaunay Edge Void Finder (++) -- ./src/DelfinBuilder.hpp
 * 
 * DelfinBuilder is a class used in a previous version of the DELFIN algorithm
 * and is not used in DELFIN++.
 * Maintained in the files to preserve the completeness of the previous version.
 * 
 */
#ifndef __DELFINBUILDER__H
#define __DELFINBUILDER__H
#include <iostream>
#include <vector>
#include "VoidBuilder.hpp"
using namespace std;

/**
 * @brief class to identify or build voids from a set of
 * tetrahedra in a 3D mesh using a criteria of expanding a
 * void from a local maxima of edge length.
 * 
 */
class DelfinBuilder : public VoidBuilder
{
protected:
	/**
	 * @brief Check if Edge A is longer than Edge B
	 * 
	 * @param a Edge A
	 * @param b Edge B
	 * @return true Length of edge A is greater than the length of edge B
	 * @return false Length of edge A is less than (or equal to) the length of edge B.
	 */
	static bool isBiggerThan(Edge, Edge);

private:
	/**
	 * @brief Construct a void starting from an edge
	 * 
	 * Starting from an edge, get the tetrahedra that share that edge
	 * and adds them to the void if they meet the algorithms conditions
	 * (tetrahedron are not in another void, the starting edge is the
	 * longest in the tetrahedra and the tetrahedra is big enough).
	 * The algorithm then adds the external edges of the void, and checks
	 * the external tetrahedra again. If the edge that the external tetrahedra
	 * shares with the void is its longest edge, and meets the conditions to
	 * be added to a void, it is joined. This is an iterative process.
	 * 
	 * @param edge Edge that starts the void
	 * @param tetrahedraDict The dictionary that contains the mesh
	 * @param voidResult Vector with the tetrahedra in the void
	 * @param tetrahedraInVoid Array of all tetrahedra in the mesh that marks
	 * all the used tetrahedra
	 * @param volume Final volume of the void in voidResult
	 */
	void checkNeighbours(Edge edge, TetrahedronDictionary *tetrahedraDict, 
	vector<int> *voidResult, int *tetrahedraInVoid, float *volume);
	float minVolume; /* Minimum volume to consider a void as valid */
	float minPercentVolume; /* Minimum percent volume to add tetrahedra to a void */
	float minEdgeLength; /* Minimum edge length to consider an edge to process its 
	tetrahedra into a void */

public:
	/**
	 * @brief Construct a new Delfin Builder object
	 * 
	 */
	DelfinBuilder();
	/**
	 * @brief Construct a new Delfin Builder object
	 * 
	 * @param _minVolume Value for minVolume
	 * @param _minPercentVolume Value for minPercentVolume
	 * @param _minEdgeLength Value for minEdgeLength
	 */
	DelfinBuilder(float, float, float);
	/**
	 * @brief Destroy the Delfin Builder object
	 * 
	 */
	~DelfinBuilder();
	/**
	 * @brief Identifies voids in a set of tetrahedra
	 * 
	 * Sort the list of edges of the mesh by length, from largest
	 * to smallest.
	 * The algorithm iterates over the sorted list of edges.
	 * If the edge is a longest local edge, creates a new void from it,
	 * then checks the neigbours to add the tetrahedra that shares
	 * their longest edge with a tetrahedron from the void.
	 * 
	 * @param tetrahedraDict The dictionary that contains the mesh
	 */
	void analyze(TetrahedronDictionary *);
};

#endif
