/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/main.cpp
 * 
 */

// =============================================================================
//
//   Program: delfin (main)
//
//   Description: Takes 
//                them.
//
// ============================================================================
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include "meshUtils.hpp"
#include "Tetrahedron.hpp"
#include "TetrahedronDictionary.hpp"
#include "PointDictionary.hpp"
#include "DelfinBuilder.hpp"
#include "EdgeJoiner.hpp"
#include "PointDensity.hpp"
#include "EdgeVolumeDensity.hpp"
#include "EdgeLengthDensity.hpp"
#include "EdgeDensityBuilder.hpp"
#include "PointDensityBuilder.hpp"
#include "PointVolumeDensity.hpp"
#include "PointLengthDensity.hpp"
using namespace std;

void syntax();
void usage();

/*
 * argv[1] = input_file_name
 * Example:
 * If you used the files called SDSS.dat SDSS_neighbours.dat SDSS_vertex.dat, input_file_name would be SDSS
 * and the program must be run in the following way:
 * ./main SDSS_preprocess -v100000 -p1 -c3
*/
int main(int argc, char **argv){
    /*
        Each file is written as follows:

            input.dat :=    x    y     z
                - first line means number of dimensions.
                - second line means how many points has the file.
                - this file contains the data of points in the space with (x,y,z) in float format.

            input_neighbours.dat := number_of_neighbours neighbour_id_1 neighbour_id_2 neighbour_id_3 neighbour_id_4
                - first line means how many tetrahedra has the file.
                - this file contains the data of neighbours of an specific tetrahedron. File's line specifies the tetrahedron's id.

            input_vertex.dat := number_of_vertexes vertex_id_1 vertex_id_2 vertex_id_3 vertex_id_4
                - first line means how many tetrahedra has the file.            
                - this file contains the data of vertexes of an specific tetrahedron. File's line specifies the tetrahedron's id. 
    */

    // File name into string
    string input_file = string(argv[1]);
    // Flag values
    float minVolume = 0.0;
    float maxVolume = 100000000000000.0; // %0 Mpc
    float minPercentVolume = 0.0;
    float minEdgeLength = 0.0; // 2 * sqrt(11/12) * 12 Mpc?
    float lambda = 2.0;
    float thresholdPercent = 0.2;
    bool wallDensity = false;
    bool discardBorder = true;
    bool deletePoorQuality = false;
    int indexMetric = 0;

    for(int k = 1; k < argc; ++k){
        if(argv[k][0] == '-'){
            switch(argv[k][1]) {
                case 'b':
                    discardBorder = false;
                    break;
                case 'q':
                    deletePoorQuality = true;
                    break;
                case 'v':
                    minVolume = atof(argv[k]+2);
                    break;
                case 'V':
                    maxVolume = atof(argv[k]+2);
                    break;
                case 'p':
                    minPercentVolume = atof(argv[k]+2);
                    break;
                case 'e':
                    minEdgeLength = atof(argv[k]+2);
                    break;
                case 't':
                    thresholdPercent = atof(argv[k]+2);
                    break;
                case 'm':
                    indexMetric = atoi(argv[k]+2);
                    break;
                case 'w':
                    wallDensity = true;
                    break;
                case 'h':
                    usage();
                    return 0;

            }
        } else {
            input_file = string(argv[k]);
        }
    }

    // Create points dictionary from file
    PointDictionary *pointsDict = new PointDictionary();
    pointsDict->load(input_file);

    cout << "N points " << pointsDict->getDataLength() << "\n";

    // Create tetrahedra dictionary from file

    TetrahedronDictionary *tetrahedraDict = new TetrahedronDictionary();
    tetrahedraDict->load(input_file);
    tetrahedraDict->setPointDictionary(pointsDict);


    PointDensity * pointDens;
    EdgeDensity * edgeDens;

    switch(indexMetric){
        case 0:
        {
            // Void Finder with point density (by volume of its tetraedra)
            pointDens = new PointVolumeDensity();
            PointDensityBuilder * analyzer = new PointDensityBuilder(thresholdPercent, wallDensity, minVolume, maxVolume);
            analyzer->analyze(tetrahedraDict, pointDens, discardBorder, deletePoorQuality);
            analyzer->printResultOFF(input_file,tetrahedraDict);
            analyzer->printVoidInformation(input_file,tetrahedraDict);
            //analyzer->printVoidsPoints(input_file, tetrahedraDict, pointDens);
            //pointDens->printResultOFF(input_file, tetrahedraDict);

            delete analyzer;
            delete pointDens;
            break;
        }
        case 1:
        {
            // Void Finder with point density (by length of its edges)
            pointDens = new PointLengthDensity();
            PointDensityBuilder *analyzer = new PointDensityBuilder(thresholdPercent, wallDensity, minVolume, maxVolume);
            analyzer->analyze(tetrahedraDict, pointDens, discardBorder, deletePoorQuality);
            analyzer->printResultOFF(input_file,tetrahedraDict);
            analyzer->printVoidInformation(input_file, tetrahedraDict);
            //analyzer->printVoidsPoints(input_file, tetrahedraDict, pointDens);
            //pointDens->printResultOFF(input_file, tetrahedraDict);

            delete analyzer;
            delete pointDens;
            break;
        }
        case 2:
        {
            // Void Finder with edge density (by volume of its tetraedra)
            edgeDens = new EdgeVolumeDensity();
            EdgeDensityBuilder *analyzer = new EdgeDensityBuilder(thresholdPercent, wallDensity, minVolume, maxVolume);
            analyzer->analyze(tetrahedraDict, edgeDens, discardBorder, deletePoorQuality);
            analyzer->printResultOFF(input_file,tetrahedraDict);
            //analyzer->printVoidInformation(input_file, tetrahedraDict);
            //analyzer->printVoidsPoints(input_file, tetrahedraDict);
            //edgeDens->printResultOFF(input_file, tetrahedraDict);
            
            delete analyzer;
            delete edgeDens;
            break;
        }
        case 3:
        {
            // Void Finder with edge density (by its length and volume of its tetraedra)
            edgeDens = new EdgeLengthDensity();
            EdgeDensityBuilder *analyzer = new EdgeDensityBuilder(thresholdPercent, wallDensity, minVolume, maxVolume);
            analyzer->analyze(tetrahedraDict, edgeDens, discardBorder, deletePoorQuality);
            analyzer->printResultOFF(input_file,tetrahedraDict);
            //analyzer->printVoidInformation(input_file, tetrahedraDict);
            //analyzer->printVoidsPoints(input_file, tetrahedraDict);
            //edgeDens->printResultOFF(input_file, tetrahedraDict);

            delete analyzer;
            delete edgeDens;
            break;
        }
        case 4:
        {
            // Void Finder
            float r3 = getR3(lambda, tetrahedraDict);
            VoidBuilder * analyzer;
            analyzer = new DelfinBuilder(minVolume, minPercentVolume, minEdgeLength);

            analyzer->analyze(tetrahedraDict);

            // Join pre-voids
            VoidJoiner *joiner;
            joiner = new EdgeJoiner(r3, discardBorder);

            joiner->join(analyzer->getResult(),tetrahedraDict);
            joiner->printResultOFF(input_file,tetrahedraDict,argc,argv);
            //joiner->printFeatures(input_file,tetrahedraDict);

            delete analyzer;
            delete joiner;
            break;            
        }

    }

    delete pointsDict;
    delete tetrahedraDict;
}


void syntax()
{
    printf("  delfin input_file [-bqv_p_e_m_wt_h]\n");
    printf("    -b  Doesn\'t discard voids with tetrahedrons in the edge of\n");
    printf("        the convex hull.\n");
    printf("    -q  Removes poor quality tetrahedra from the edge of voids with\n");
    printf("        a high percentage of area covered by these tetrahedra.\n");
    printf("    -v  Applies a minimum void volume constraint.\n");
    printf("    -V  Applies a maximum void volume constraint.\n");
    printf("    -p  Applies a minimum void percent volume constraint.\n");
    printf("    -e  Applies a minimun edge length constraint to generate a new\n");
    printf("        void.\n");
    printf("    -m  Specifies the type of void builder to use.\n");
    printf("        -m0  Builder by point density according to the volume of its\n");
    printf("             tetraedra.\n");
    printf("        -m1  Builder by point density according to the length of its\n");
    printf("             edges.\n");
    printf("        -m2  Builder by edge density according to the volume of its\n");
    printf("             tetraedra.\n");
    printf("        -m3  Builder by edge density according to its length and the\n");
    printf("             volume of its tetrahedra.\n");
    printf("        -m4  Builder by longest edge.\n");
    printf("    -w  Specifies that the threshold density is according to the\n");
    printf("        mean element density.\n");
    printf("    -t  Specifies the threshold density of the elements that are\n");
    printf("        part of a void\n");
    printf("    -h  Help:  A brief instruction for using DELFIN.\n");
}

void usage()
{
    printf("DELFIN\n");
    printf("A 3D Delaunay Tessellation Based Void Finder Algorithm\n");
    printf("July, 2021\n");
    printf("\n");
    printf("What Can DELFIN Do?\n");
    printf("\n");
    printf("  DELFIN finds a set of voids from density parameters given by\n");
    printf("  geometrics elements of the Delaunay tetrahedralizations of a\n");
    printf("  set of points.\n");
    printf("\n");
    printf("Command Line Syntax:\n");
    printf("\n");
    printf("  Below is the basic command line syntax of DELFIN with a list of\n");
    printf("  short descriptions. Underscores indicate that numbers follow\n");
    printf("  follow certain switches.  Do not leave any space between a ");
    printf("switch\n");
    printf("  and its numeric parameter.  \'input_file\' contains input data\n");
    printf("  and  to use it correctly the files \'input_file\'.dat, \n");
    printf("  \'input_file\'_neighbours.dat and \'input_file\'_vertex.dat");
    printf(" must\n");
    printf("  exist in the directory.\n");
    printf("  More details on the file formats are found in the README.\n");
    printf("\n");
    syntax();
    printf("\n");
    printf("\n");
    printf("Examples of How to Use DELFIN:\n");
    printf("\n");
    printf("  \'delfin data -m0 -w -t0.2 -v10000\' reads vertices from \n");
    printf("  data_vertex.dat, tetrahedra points from data_vertex.dat and\n");
    printf("  neighbours from data_neighbours.dat. Using the builder by point\n");
    printf("  density, evaluate points until their density is 20%% the mean\n");
    printf("  density\n");
    printf("\n");
    printf("  \'delfin data -m4 -v15000 -e20\' reads vertices from \n");
    printf("  data_vertex.dat, tetrahedra points from data_vertex.dat and\n");
    printf("  neighbours from data_neighbours.dat. Using the builder by longest\n");
    printf("  edge, evaluate its tetraedra until its length is 20.\n");
    printf("\n");
}
