/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/VoidContainer.cpp
 * 
 */
#include "VoidContainer.hpp"
#include <utility>


VoidContainer::VoidContainer(){
	data_n = 0;
}
VoidContainer::~VoidContainer(){}

int VoidContainer::addVoid(vector<int> newVoid){
	voidsMap.insert(make_pair(data_n,newVoid));
	int ret = data_n;
	++data_n;
	for(vector<int>::iterator it = newVoid.begin() ; it != newVoid.end() ; ++it){
		tetrahedraDict->setTetrahedronVoidId(*it,ret);
	}
	return ret;
}
void VoidContainer::deleteVoid(int id){
	voidsMap.erase(id);
}
void VoidContainer::setTetrahedronDictionary(TetrahedronDictionary* _tetrahedraDict){
	tetrahedraDict = _tetrahedraDict;
}
TetrahedronDictionary* VoidContainer::getTetrahedronDictionary(){
	return this->tetrahedraDict;
}
VoidSpace VoidContainer::getById(int id){
	VoidSpace voidSpace;
	voidSpace.setId(id);
	voidSpace.setTetrahedronDictionary(this->tetrahedraDict);
	voidSpace.setTetrahedraId(voidsMap[id]);
	return voidSpace;
}