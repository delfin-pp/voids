/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/Edge.hpp
 * 
 */
#ifndef __EDGE__H
#define __EDGE__H
#include "PointDictionary.hpp"
#include <vector>
using namespace std;

/**
 * @brief class for Edges in a 3D mesh
 * 
 */
class Edge{
	private:
		int id; /* Edge id */
		int pointsId[2]; /* Edge's vertex id */
		vector<int> tetrahedraId; /* Ids of tetrahedra containing the edge */
		float length; /* Length of the edge */
		PointDictionary* pointsDict;
		int inBorder; /* Edge is part of the surface of the mesh */
	public:
		/**
		 * @brief Construct a new Edge object
		 * 
		 */
		Edge();
		/**
		 * @brief Construct a new Edge object
		 * 
		 * @param x Id of the first vertex of the edge
		 * @param y Id of the second vertex of the edge
		 */
		Edge(int x, int y);
		/**
		 * @brief Destroy the Edge object
		 * 
		 */
		~Edge();
		/**
		 * @brief Set the Id of the Edge
		 * 
		 * @param _id The id of the edge
		 */
		void setId(int);
		/**
		 * @brief Get the Id of the Edge
		 * 
		 * @return int The id of the edge
		 */
		int getId();
		/**
		 * @brief Set the Points Id
		 * 
		 * Set the 2 points (vertex) id of the edge.
		 * The id of the first vertex must be less than the second vertex id.
		 * 
		 * @param _pointsId Array with vertex id
		 */
		void setPointsId(int[2]);
		/**
		 * @brief Get the Points Id
		 * 
		 * Get the 2 points (vertex) id of the edge.
		 * 
		 * @return int* Array of ids
		 */
		int* getPointsId();
		/**
		 * @brief Set the Tetrahedra Id
		 * 
		 * Set the id of the tetrahedra that have the edge as a part of them.
		 * 
		 * @param _tetrahedraId Vector with the tetrahedra ids
		 */
		void setTetrahedraId(vector<int>);
		/**
		 * @brief Add the id of a tetrahedron
		 * 
		 * Add the id of a tetrahedron to the vector containing all the
		 * tetrahedra that have the edge as a part of them.
		 * 
		 * @param _tetrahedronId Id of the tetrahedron
		 */
		void addTetrahedronId(int);
		/**
		 * @brief Get the Tetrahedra Id
		 * 
		 * Get the vector containing all the tetrahedra that have the edge
		 * as a part of them.
		 * 
		 * @return vector<int> Vector with the tetrahedra ids
		 */
		vector<int> getTetrahedraId();
		/**
		 * @brief Set the Length of the Edge
		 * 
		 * @param _length The length of the edge
		 */
		void setLength(float);
		/**
		 * @brief Get the Length of the Edge
		 * 
		 * @return float The length of the edge
		 */
		float getLength();
		/**
		 * @brief Set the Point Dictionary
		 * 
		 * @param pointsDict Reference to the Dictionary of points
		 * 
		 */
		void setPointDictionary(PointDictionary*);
		/**
		 * @brief Get the inBorder value
		 * 
		 * Get the inBorder value that indicates if the edge is 
		 * part of the surface of the mesh.
		 * 1 indicates that the edge is part of the border.
		 * 0 indicates that the edge is not part of the border.
		 * 
		 * @return int The inBorder value
		 */
		int isInBorder();
		/**
		 * @brief Set the inBorder value
		 * 
		 * Set the inBorder value that indicates if the edge is 
		 * part of the surface of the mesh.
		 * 
		 * @param _inBorder The inBorder value
		 * 
		 */
		void setInBorder(int);
};

#endif
