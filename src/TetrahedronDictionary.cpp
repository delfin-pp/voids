/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/TetrahedronDictionary.cpp
 * 
 */
#include "TetrahedronDictionary.hpp"
#include <fstream>
#include <stdlib.h>
#include <vector>
#include <thread>
#include <cmath>
using namespace std;

TetrahedronDictionary::TetrahedronDictionary(): Dictionary<Tetrahedron>(){
	edgesDict = new EdgeDictionary();
	data_n = 0;
}
TetrahedronDictionary::~TetrahedronDictionary(){
	delete edgesDict;
	for(int k = 0 ; k < data_n ; ++k){
		delete [] tetrahedraNeighboursId[k];
		delete [] tetrahedraPointsId[k];
		delete [] tetrahedraEdgesId[k];
	}
	delete [] tetrahedraNeighboursId;
	delete [] tetrahedraEdgesId;
	delete [] tetrahedraPointsId;
	delete [] tetrahedraVoidId;
}
Tetrahedron TetrahedronDictionary::getById(int id){
	Tetrahedron tetrahedron;
	tetrahedron.setId(id);
	tetrahedron.setPointsId(tetrahedraPointsId[id]);
	tetrahedron.setNeighboursId(tetrahedraNeighboursId[id]);
	tetrahedron.setEdgesId(tetrahedraEdgesId[id]);
	tetrahedron.setEdgeDictionary(this->edgesDict);
	tetrahedron.setPointDictionary(this->pointsDict);
	tetrahedron.setVoidId(tetrahedraVoidId[id]);
	return tetrahedron;
}
void TetrahedronDictionary::load(string input_file){
	// Input files strings.
	string input_vertexes = input_file+string("_vertex.dat");
	string input_neighbours = input_file+string("_neighbours.dat");
	ifstream vertexes_ifs;
	vertexes_ifs.open(input_vertexes,ifstream::in);

	if(!vertexes_ifs.good()){
		cout << "Error when tried to open '" << input_file << "'" << '\n';
	}else{
		string line1;
		getline(vertexes_ifs,line1); // first line : number of tetrahedra
		int tetrahedron_number = atoi(line1.c_str());
		vertexes_ifs.close();
		tetrahedraNeighboursId = new int*[tetrahedron_number];
		tetrahedraEdgesId = new int*[tetrahedron_number];
		tetrahedraPointsId = new int*[tetrahedron_number];
		tetrahedraVoidId = new int[tetrahedron_number];
		for(int k = 0 ; k < tetrahedron_number ; k++){
			tetrahedraVoidId[k] = -1;
		}
		data_n = tetrahedron_number;
		loadNeighbours(input_neighbours,tetrahedron_number);
		loadVertexesAndEdges(input_vertexes,tetrahedron_number);
	}
}
void TetrahedronDictionary::loadNeighbours(string input_neighbours,int number){
	string line;
	ifstream neighbours_ifs;
	neighbours_ifs.open(input_neighbours,ifstream::in);
	if(!neighbours_ifs.good()){
		cout << "Error when tried to open '" << input_neighbours << "'" << '\n';
	}else{
		getline(neighbours_ifs,line);
		// Getting neighbours' id in file
		for(int k = 0; k < number ; k++){
			getline(neighbours_ifs,line);
			vector<string> line_splited = this->split(line,' ');
			int j = 0;
			int* aux = new int[4];
			for(vector<string>::iterator it = line_splited.begin()+1; it != line_splited.end() ; it++){ // first col : number of neighbours
				aux[j] = atoi((*it).c_str());
				j++;
			}
			tetrahedraNeighboursId[k] = aux;
		}
		neighbours_ifs.close();
	}
	return;
}
// Load vertexes and edges to the dictionary
void TetrahedronDictionary::loadVertexesAndEdges(string input_vertexes,int number){
	string line;
	ifstream vertexes_ifs;
	vertexes_ifs.open(input_vertexes,ifstream::in);
	if(!vertexes_ifs.good()){
		cout << "Error when tried to open '" << input_vertexes << "'" << '\n';
	}else{
		getline(vertexes_ifs,line); // first line number of tetrahedra
		// Getting vertexes' id in file
		for(int k = 0; k < number ; k++){
			getline(vertexes_ifs,line);
			vector<string> line_splitted = this->split(line,' ');
			int j = 0;
			int *point_aux = new int[4];
			int *edge_aux = new int[6];
			for(vector<string>::iterator it = line_splitted.begin()+1; it != line_splitted.end() ; it++){
				point_aux[j] = atoi((*it).c_str());
				j++;
			}
			
			// Add the edges
			int edge_k = 0;
			for (int i = 0 ; i < 4 ;i++){
				for(int j = i+1; j < 4 ; ++j){
					edge_aux[edge_k] = edgesDict->add(point_aux[i],point_aux[j],k); // Add Edge to edgesDicts
					edge_k++;
				}
			}
			tetrahedraPointsId[k] = point_aux;
			tetrahedraEdgesId[k] = edge_aux; 
		}
		vertexes_ifs.close();
		
		// Set border edges
		for (int id = 0; id < data_n; id++){
			Tetrahedron btetrahedron = getById(id);
			if (!btetrahedron.isInBorder()) { // Ignore border tetrahedra
				continue;
			}
			//Counters for edge ocurrence in adjacent tetrahedra
			int *bedges = btetrahedron.getEdgesId();
			vector<int> cnt(6,0);
			
			for (int n = 0; n < 4; n++){
				if (tetrahedraNeighboursId[id][n] < 0)
					continue;
				Tetrahedron ntetrahedron = getById(tetrahedraNeighboursId[id][n]);
				int *nedges = ntetrahedron.getEdgesId();
				
				//check each edge and count it in the array if it exists
				for (int i = 0; i < 6; i++) {
					for (int j = 0; j < 6; j++) {
						if (bedges[i] == nedges[j]) {
							cnt[i]++;
							break;
						}
					}
				}

			}
			for (int i = 0; i < 6; i++) {
				if (cnt[i] < 2) {
					// Is border edge; mark edge as border
					edgesDict->setInBorder(bedges[i], 1);
				}
			}
		}
	}
	return;
}
void TetrahedronDictionary::setPointDictionary(PointDictionary* _pointsDict){
	pointsDict= _pointsDict;
	edgesDict->setPointDictionary(_pointsDict);
}
PointDictionary* TetrahedronDictionary::getPointDictionary(){
	return pointsDict;
}
EdgeDictionary* TetrahedronDictionary::getEdgeDictionary(){
	return edgesDict;
}
int TetrahedronDictionary::add(Tetrahedron* _tetrahedron){
	return 0;
}
void TetrahedronDictionary::setTetrahedronVoidId(int tetrahedronId, int voidId){
	tetrahedraVoidId[tetrahedronId] = voidId;
}



