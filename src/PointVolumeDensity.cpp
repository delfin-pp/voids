/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/PointVolumeDensity.cpp
 * 
 */
#include "PointVolumeDensity.hpp"
#include <algorithm>
#include <cmath>
#include <stdlib.h>

void PointVolumeDensity::apply(TetrahedronDictionary *tetrahedraDict){ // 1/vol
    int n_points = tetrahedraDict->getPointDictionary()->getDataLength();
    int n_tetrahedra = tetrahedraDict->getDataLength();
    pointDensity.resize(n_points, make_pair(0, 0.));
    pointTetrahedra.resize(n_points);
    //suma de los volumenes
    for(int i=0; i<n_tetrahedra; i++){
        Tetrahedron tetrahedron = tetrahedraDict->getById(i);
        int *points= tetrahedron.getPointsId();
        for( int it=0; it < 4 ; it++){
            pointDensity[points[it]].second += tetrahedron.getVolume();
            pointTetrahedra[points[it]].push_back(i);
        }
    }

    float sum_density = 0.;

    for(int i=0; i<n_points; i++){
        pointDensity[i].first = i;
        pointDensity[i].second = 1 / pointDensity[i].second;
        sum_density += pointDensity[i].second;
    }

    mean = sum_density / n_points;
}