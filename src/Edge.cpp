/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/Edge.cpp
 * 
 */
#include "Edge.hpp"
#include <math.h>

Edge::Edge(){
	length = 0.0;
}
Edge::Edge(int x, int y){
	pointsId[0] = x;
	pointsId[1] = y;
	length = 0.0;
	inBorder = 0;
}
Edge::~Edge(){}


void Edge::setId(int _id){
	id=_id;
}
int Edge::getId(){
	return this->id;
}
void Edge::setPointsId(int _pointsId[2]){
	pointsId[0] = _pointsId[0];
	pointsId[1] = _pointsId[1];
}
int* Edge::getPointsId(){
	return this->pointsId;
}
void Edge::setTetrahedraId(vector<int> _tetrahedraId){
	tetrahedraId = _tetrahedraId;
}
void Edge::addTetrahedronId(int _tetrahedronId){
	tetrahedraId.push_back(_tetrahedronId);
}
vector<int> Edge::getTetrahedraId(){
	return this->tetrahedraId;
}
void Edge::setPointDictionary(PointDictionary* _pointsDict){
	pointsDict = _pointsDict;
}
void Edge::setLength(float _length){
	length=_length; 
}
float Edge::getLength(){
	if(this->length <= 0.0){
		float *point_j = pointsDict->getById(pointsId[0]);
		float *point_k = pointsDict->getById(pointsId[1]);
		float dx = point_j[0] - point_k[0];
		float dy = point_j[1] - point_k[1];
		float dz = point_j[2] - point_k[2];
                length = sqrt(dx*dx + dy*dy + dz*dz);
	}
	return this->length;
}

int Edge::isInBorder(){
	return this->inBorder;
}
	
void Edge::setInBorder(int _inBorder){
	this->inBorder = _inBorder;
}
