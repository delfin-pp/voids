/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/PointLengthDensity.cpp
 * 
 */
#include "PointLengthDensity.hpp"
#include <algorithm>
#include <cmath>
#include <stdlib.h>
#include <set>


void PointLengthDensity::apply(TetrahedronDictionary *tetrahedraDict){ // 1/vol
    int n_points = tetrahedraDict->getPointDictionary()->getDataLength();
    pointDensity.resize(n_points, make_pair(0, 0.));
    pointTetrahedra.resize(n_points);

    vector<set<int>> aux(n_points);

    EdgeDictionary* edges = tetrahedraDict->getEdgeDictionary();
    int n_edges = edges->getDataLength();

    int* pointsId;
    for(int i = 0; i < n_edges; i++){
        Edge edge = edges->getById(i);
        pointsId = edge.getPointsId();
        pointDensity[pointsId[0]].second += edge.getLength();
        pointDensity[pointsId[1]].second += edge.getLength();

        vector<int> tetrahedra = edge.getTetrahedraId();
        for(vector<int>::iterator it = tetrahedra.begin(); it != tetrahedra.end(); ++it) {
            aux[pointsId[0]].insert(*it);
            aux[pointsId[1]].insert(*it);
        }

    }

    float sum_density = 0.;

    for(int i=0; i<n_points; i++){
        pointDensity[i].first = i;
        pointDensity[i].second = 1 / pointDensity[i].second;
        sum_density += pointDensity[i].second;

        pointTetrahedra[i].assign(aux[i].begin(), aux[i].end());
    }

    mean = sum_density / n_points;
}