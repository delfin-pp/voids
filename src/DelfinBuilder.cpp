/**
 * DELFIN++ -- Delaunay Edge Void Finder (++) -- ./src/DelfinBuilder.cpp
 * 
 * DelfinBuilder is a class used in a previous version of the DELFIN algorithm
 * and is not used in DELFIN++.
 * Maintained in the files to preserve the completeness of the previous version.
 * 
 */
#include "DelfinBuilder.hpp"
#include <algorithm>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include <iomanip>
#include <stack>

using namespace std;

DelfinBuilder::~DelfinBuilder() {}

DelfinBuilder::DelfinBuilder() : VoidBuilder()
{
    minVolume = 0.0;
    minPercentVolume = 0.0;
    minEdgeLength = 0.0;
}

DelfinBuilder::DelfinBuilder(float _minVolume, float _minPercentVolume, float _minEdgeLength)
{
    minVolume = _minVolume;
    minPercentVolume = _minPercentVolume;
    minEdgeLength = _minEdgeLength;
}

bool DelfinBuilder::isBiggerThan(Edge a, Edge b)
{
    return (a.getLength() > b.getLength());
}

void DelfinBuilder::analyze(TetrahedronDictionary *tetrahedraDict)
{
    cout << "Analyzing ...\n";
    vector<vector<int>> voidsResult; // Tetrahedra id in voids
    EdgeDictionary *edgesDict = tetrahedraDict->getEdgeDictionary();

    vector<Edge> edgeList(edgesDict->getDataLength()); // Edge list used by sort
    for (int k = 0; k < edgesDict->getDataLength(); k++)
    {
        edgeList[k] = edgesDict->getById(k);
    }
    sort(edgeList.begin(), edgeList.end(), &DelfinBuilder::isBiggerThan);

    int *tetrahedraInVoid = new int[tetrahedraDict->getDataLength()]; // Array contains tetrahedra marked visited by analyze
    for (int i = 0; i < tetrahedraDict->getDataLength(); ++i)
    {
        tetrahedraInVoid[i] = 0;
    }

    float *volume = new float();
    int voidId = 0;

    for (vector<Edge>::iterator it = edgeList.begin(); it != edgeList.end(); ++it)
    {
        Edge edge = *it; // Actual edge
        bool isStart = true;

        // Get tetrahedra that share this edge
        vector<int> tetrahedraId = edge.getTetrahedraId();

        for (auto f_it = tetrahedraId.begin(); f_it != tetrahedraId.end(); ++f_it)
        {
            Tetrahedron tetrahedron = tetrahedraDict->getById(*f_it);
            if (tetrahedron.getLongestEdge() != edge.getLength())
            {
                isStart = false;
                break;
            }
        }
        if (!isStart)
        {
            continue; // Actual edge is not a longest local edge
        }

        if (edge.getLength() < minEdgeLength)
        {
            break; // All edges longer than minEdgeLength have been processed
        }

        vector<int> *singleVoid = new vector<int>(); // Tetrahedra' id of a single void
        *volume = 0.0;
        checkNeighbours(edge, tetrahedraDict, singleVoid, tetrahedraInVoid, volume); // Iterative algorithm to find subvoids
        if (singleVoid->size() > 0)
        {
            if ((*volume) > minVolume)
            {
                vector<int> singleVoidAux;
                singleVoidAux = *singleVoid;
                voidsResult.push_back(singleVoidAux);
                for (vector<int>::iterator singleVoid_it = singleVoid->begin(); singleVoid_it != singleVoid->end(); ++singleVoid_it)
                {
                    tetrahedraDict->setTetrahedronVoidId(*singleVoid_it, voidId);
                }
                voidId++;
            }
            else
            {
                for (vector<int>::iterator singleVoid_it = singleVoid->begin(); singleVoid_it != singleVoid->end(); ++singleVoid_it)
                {
                    tetrahedraInVoid[*singleVoid_it] = 0;
                }
            }
        }
        delete singleVoid;
    }
    delete volume;
    delete[] tetrahedraInVoid;
    result = voidsResult;
}

void DelfinBuilder::checkNeighbours(Edge edge, TetrahedronDictionary *tetrahedraDict, vector<int> *voidResult, int *tetrahedraInVoid, float *volume)
{
    stack<Edge> edgeStack;
    edgeStack.push(edge);
    EdgeDictionary *edgesDict = tetrahedraDict->getEdgeDictionary();
    while (!edgeStack.empty())
    {
        edge = edgeStack.top();
        edgeStack.pop();
        vector<int> tetrahedraId = edge.getTetrahedraId();
        // Get tetrahedra who share the same edge
        for (vector<int>::iterator it = tetrahedraId.begin(); it != tetrahedraId.end(); ++it)
        {
            int tetrahedronId = *it;
            Tetrahedron tetrahedron = tetrahedraDict->getById(tetrahedronId);
            // Algorithm's conditions for add tetrahedra in void result
            if ((tetrahedraInVoid[tetrahedronId] == 0) &&             // Tetrahedron not in a void
                (tetrahedron.getLongestEdge() == edge.getLength()) && // Actual edge is one of the longest edges of the tetrahedron
                (tetrahedron.getVolume() / (*volume) * 100.0 >= minPercentVolume))
            {
                tetrahedraInVoid[tetrahedronId] = 1;  // Set tetrahedron in void
                voidResult->push_back(tetrahedronId); // Add tetrahedron in result
                (*volume) = (*volume) + tetrahedron.getVolume();
                int *edgesId = tetrahedron.getEdgesId();
                for (int k = 0; k < 6; ++k)
                {
                    if (edgesId[k] != edge.getId())
                    {
                        edgeStack.push(edgesDict->getById(edgesId[k]));
                    }
                }
            }
        }
    }
}
