/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/VoidShape.cpp
 * 
 */
#include "VoidShape.hpp"
#include "jacobi_pd.hpp"
#include <cmath>
VoidShape::VoidShape(){
    inertiaTensor = new float *[3];
    eigenVectors = new float *[3];
    eigenValues = new float[3];
    massCenter = new float[3];
    for (int k = 0; k < 3; ++k)
    {
        inertiaTensor[k] = new float[3];
        eigenVectors[k] = new float[3];
    }
}
VoidShape::~VoidShape(){
    for (int k = 0; k < 3; ++k)
    {
        delete[] inertiaTensor[k];
        delete[] eigenVectors[k];
    }
    delete[] inertiaTensor;
    delete[] eigenVectors;
    delete[] eigenValues;
    delete[] massCenter;
}
float VoidShape::getEllipticity(){
    return this->ellipticity;
}
float *VoidShape::getMassCenter(){
    return this->massCenter;
}
float *VoidShape::getEigenValues(){
    return this->eigenValues;
}
float **VoidShape::getEigenVectors(){
    return this->eigenVectors;
}
float **VoidShape::getInertiaTensor(){
    return this->inertiaTensor;
}

void VoidShape::analyze(vector<int> pointsId, PointDictionary* pointsDict){
    vector<float*> points;
    float mean[3] = {0., 0., 0.};
    for (auto p_it = pointsId.begin(); p_it != pointsId.end(); ++p_it)
    {
        float *data = pointsDict->getById(*p_it);
        float *point= new float[3];
        for(int i = 0; i < 3; i++){
            point[i] = data[i];
            mean[i] += point[i];
        }
        points.push_back(point);
    }

    int size = points.size();
    for (int m = 0; m < 3; m++)
    {
        mean[m] = mean[m] / size;
        massCenter[m] = mean[m];
    }

    // Substract the mean
    for (int i = 0; i < size; i++)
    {
        points[i][0] -= mean[0]; 
        points[i][1] -= mean[1];
        points[i][2] -= mean[2];
    }

    // Calculate the C
    float **C;
    C = new float *[3];
    C[0] = new float[3];
    C[1] = new float[3];
    C[2] = new float[3];

    C[0][0] = 0.0; C[0][1] = 0.0; C[0][2] = 0.0;
    C[1][0] = 0.0; C[1][1] = 0.0; C[1][2] = 0.0;
    C[2][0] = 0.0; C[2][1] = 0.0; C[2][2] = 0.0;

    for (int i = 0; i < 3; i++)
    {
        for (int j = i; j < 3; j++)
        {
            for(int p = 0; p < size ; p++)
            {
                C[i][j] += points[p][i] * points[p][j];
            }
            //C[i][j] = C[i][j] / (size - 1);
            inertiaTensor[i][j] = -C[i][j];
            if (j > i)
            {
                C[j][i] = C[i][j];
                inertiaTensor[j][i] = -C[j][i];
            } 
        }
    }

    float tr_C = C[0][0] + C[1][1] + C[2][2];
    inertiaTensor[0][0] += tr_C;
    inertiaTensor[1][1] += tr_C; 
    inertiaTensor[2][2] += tr_C;

    // Calculate the eigenvectors and eigenvalues of the inertia tensor
    jacobi_pd::Jacobi<float, float *, float **> eigen_calc(3);
    eigen_calc.Diagonalize(inertiaTensor, eigenValues, eigenVectors, eigen_calc.SORT_DECREASING_EVALS);

    //The ellipticity (oblateness) is e = 1 − (c / a)**(1/4), where a and c are the semimajor and semiminor axes, respectively.
    ellipticity = 1. - pow((eigenValues[2] / eigenValues[0]), 0.25);

    for (int i = 0; i < size; i++){
        delete[] points[i];
    }
    for(int i = 0; i<3; i++){
        delete[] C[i];
    }
    delete[] C;
}
