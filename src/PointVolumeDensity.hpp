/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/PointVolumeDensity.hpp
 * 
 */
#ifndef __POINTVOLUMEDENSITY__H
#define __POINTVOLUMEDENSITY__H
#include "PointDensity.hpp"
using namespace std;

/**
 * @brief class for the density of the points in a 3D mesh
 * by the volume of its sorrounding tetrahedra
 * 
 */
class PointVolumeDensity : public PointDensity {
    public:
        /**
         * @brief Applies the density calculation on the tetrahedral mesh
         * 
         * Gets the number of points in the mesh and resizes the pointDensity
         * and pointTetrahedra vectors.
         * From the Tetrahedron Dictionary, computes the volume of every 
         * tetrahedron and add its value to its vertices.
         * Based on the sum of the volumes, the density of each point is 
         * calculated, and from this the average density is calculated.
         * 
         * @param tetrahedraDict The dictionary that contains the mesh
         */
        void apply(TetrahedronDictionary*);
};


#endif