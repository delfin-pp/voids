/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/PointDensity.hpp
 * 
 */
#ifndef __POINTDENSITY__H
#define __POINTDENSITY__H
#include "ElementDensity.hpp"
#include <vector>
using namespace std;

/**
 * @brief class for the density of the points in a 3D mesh
 * 
 */
class PointDensity : public ElementDensity{
    private:
        /**
         * @brief Compares two pairs by the second element
         * 
         * A pair X is smaller than a pair Y if the second element of
         * X is smaller than the second element of Y.
         * 
         * @return true If the first pair is smaller than the second
         * @return false If the second pair is smaller than the first
         */
        static bool isSmallerThan(pair<int, float>, pair<int, float>);
    protected:
        vector<pair<int,float>> pointDensity; /* Pairs with the points' id and density */
        vector<vector<int>> pointTetrahedra; /* Tetrahedra of the points */
    public:
        /**
         * @brief Construct a new Point Density object
         * 
         */
        PointDensity();
        /**
         * @brief Destroy the Point Density object
         * 
         */
        ~PointDensity();
        /**
         * @brief Get the vector with the Point Density sorted by ascending density
         * 
         * @return vector<pair<int,float>> Vector with pairs of points' id and density
         * sorted by density
         */
        vector<pair<int,float>> getSortedPoints();
        /**
         * @brief Get the vector with the Point Density
         * 
         * @return vector<pair<int,float>> Vector with pairs of points' id and density
         */
        vector<pair<int,float>> getPointDensity();
        /**
         * @brief Get the density of the point
         * 
         * @param pointId Id of the point
         * @return float Density of the point
         */
        float getPointIdDensity(int);
        /**
         * @brief Get the tetrahedra that have the point as a vertex
         * 
         * @param pointId Id of the point
         * @return vector<int> Vector of tetrahedra that share the point
         */
        vector<int> getPointIdTetrahedra(int);
        /**
         * @brief Writes an OFF file of the mesh
         * 
         * Writes an OFF file of the mesh color coding the faces of the
         * tetraedra according to the density values of its points.
         * 
         * @param filename String with the name of the output OFF file
         * @param tetrahedronDictionary The dictionary that contains the mesh
         */
        void printResultOFF(string, TetrahedronDictionary*);
};

#endif