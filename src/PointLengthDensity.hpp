/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/PointLengthDensity.hpp
 * 
 */
#ifndef __POINTLENGTHDENSITY__H
#define __POINTLENGTHDENSITY__H
#include "PointDensity.hpp"
using namespace std;

/**
 * @brief class for the density of the points in a 3D mesh
 * by the length of the edges it defines.
 * 
 */
class PointLengthDensity : public PointDensity {
    public:
        /**
         * @brief Applies the density calculation on the tetrahedral mesh
         * 
         * Gets the number of points in the mesh and resizes the pointDensity
         * and pointTetrahedra vectors.
         * From the Edge Dictionary, computes the length of every edge and
         * add its value to its delimiting vertices.
         * Based on the sum of the lengths, the density of each point is 
         * calculated, and from this the average density is calculated.
         * 
         * @param tetrahedraDict The dictionary that contains the mesh
         */
        void apply(TetrahedronDictionary*);
};


#endif