/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/DensityBuilder.hpp
 * 
 */
#ifndef __DENSITYBUILDER__H
#define __DENSITYBUILDER__H
#include <iostream>
#include <vector>
#include "VoidBuilder.hpp"
#include "ElementDensity.hpp"
#include "VoidSpace.hpp"
using namespace std;

/**
 * @brief class for Void Finder via density of geometric elements in a
 * 3D tetrahedral mesh.
 * 
 */
class DensityBuilder : public VoidBuilder{
    protected:
        float minVolume; /* Minimum volume of a void */
        float maxVolume; /* Maximum volume of a void  */
        float meanDensity; /* Mean densitiy of the elements of the mesh */
        float thresholdPercent; /* Percentage value to stop including 
        elements to voids */
        bool wallDensity; /* Bool to define if the threshold value used 
        for the wall of voids is a percentage of the mean density of the
        mesh (= true) or is a percentage of the number of elements of the
        mesh (= false) */
        /**
         * @brief Add tetrahedra to a void
         * 
         * Given a vector of tetrahedra, these are added to the indicated void.
         * If any tetrahedron previously belonged to a different void, the voids merge 
         * into the void with id equal to voidId.
         * 
         * @param tetrahedraDict Reference to the Dictionary of tetrahedra
         * @param tetrahedraId Vector of tetrahedra ids to add to the void
         * @param voidId Id of the void to which the tetrahedra will be added
         */
        void addTetrahedraToVoid(TetrahedronDictionary*, vector<int>, int);
        /**
         * @brief Join two voids
         * 
         * Given two voids, the tetrahedra that are part of the second void become 
         * part of the first, and the second void is removed from the void list.
         * 
         * @param firstVoid Id of the first void
         * @param secondVoid Id of the first void
         */
        void joinVoids(int, int);
        /**
         * @brief Get the Bigger Neighbour Void Id object
         * 
         * Given a tetrahedron, find the voids to which its neighbors belong, and 
         * return the id of the largest void within those found. 
         * If no neighbor belongs to a void, -1 is returned.
         * 
         * @param tetrahedraDict Reference to the Dictionary of tetrahedra
         * @param tetrahedron Tetrahedron object where the search is carried out
         * 
         * @return int Id of the biggest void that neighbors the tetrahedron
         */
        int getBiggerNeighbourVoidId(TetrahedronDictionary*, Tetrahedron);
        /**
         * @brief Discards Voids from the list
         * 
         * Applies a filter to the list of voids found. Discards void without a valid 
         * volume (too small or too big). Also discards with id equal to -1.
         * 
         * @param discardBorder Discard voids with a tetrahedron in the border of the 
         * 3D mesh.
         * @param deletePoorQuality Delete tetraedron from a void if a big percentage of
         * its surface area is covered by poor quality tetrhaedron.
         */
        void discardVoids(bool, bool);
    public:
        /**
         * @brief Construct a new Density Builder object
         * 
         */
        DensityBuilder();
        /**
         * @brief Construct a new Density Builder object
         * 
         * @param _thresholdPercent
         * @param _wallDensity
         * @param _minVolume
         * @param _maxVolume
         */
        DensityBuilder(float, bool, float, float);
        /**
         * @brief Destroy the Density Builder object
         * 
         */
        ~DensityBuilder();
        /**
         * @brief Get the length of the list of Voids
         * 
         * @return int Number of voids
         */
        int getVoidsLength();
        /**
         * @brief Add a Void to the list of voids of the builder
         * 
         * @param voidSpace Reference to the Void to add
         * 
         * @return int Id of the void
         */
        int addVoid(VoidSpace*);
        /**
         * @brief Get a specific Void object
         * 
         * @param voidId Id of the void to be retrieved
         * 
         * @return VoidSpace* Reference to the Void
         */
        VoidSpace* getVoid(int);

};




#endif