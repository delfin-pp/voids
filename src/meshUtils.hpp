/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/meshUtils.hpp
 * 
 * Fuctions for properties for mesh elements
 */
#include <iostream>
#include <vector>
#include "Dictionary.hpp"
#include "PointDictionary.hpp"
#include "EdgeDictionary.hpp"
#include "TetrahedronDictionary.hpp"

using namespace std;

/**
 * @brief Computes the parameter R3 from the average distance to the
 * third nearest galaxy (D3) as well as the standard deviation (σ3)
 * of that value. 
 * R3 is defined as R3 = D3 + λσ3
 * 
 * @param lambda Lambda value to compute R3
 * @param tetrahedraDict The dictionary that contains the mesh
 * @return float Parameter R3 of the mesh
 */
float getR3(float, TetrahedronDictionary*);