/**
 * DELFIN++ -- Delaunay Edge Void Finder (++) -- ./src/VoidJoiner.cpp
 * 
 * VoidJoiner is a class used in a previous version of the DELFIN algorithm
 * and is not used in DELFIN++.
 * Maintained in the files to preserve the completeness of the previous version.
 * 
 */
#include <iomanip>
#include "VoidJoiner.hpp"
#include <iostream>
#include "geom3d.hpp"

using namespace std;

vector< vector< int > > VoidJoiner::getResult(){
	return this->result;
}

vector<VoidSpace *> VoidJoiner::getVoids(TetrahedronDictionary *tetrahedraDict){
	int n_voids = result.size();
	int voids_size =  voids.size();
	if (n_voids != voids_size)
	{
		voids.clear();
		for (int vid = 0; vid < n_voids; vid++)
		{
			VoidSpace *voidSpace = new VoidSpace();
			voidSpace->setTetrahedronDictionary(tetrahedraDict);
			voidSpace->setId(vid);
			voidSpace->setTetrahedraId(result[vid]);
			voids.push_back(voidSpace);
		}
	}
	return voids;
}

void VoidJoiner::printResultOFF(string filename, TetrahedronDictionary* tetrahedraDict, int argc, char *argv[]){
	string outputFile_geom = filename+string("_output.off");
	ofstream ofs_geom(outputFile_geom.c_str());
	srand(time(NULL));
	PointDictionary* p = tetrahedraDict->getPointDictionary();
	if(ofs_geom.good()){
		ofs_geom << "OFF\n";
		ofs_geom << "#";
		for (int i = 0; i < argc-1; i++) {
			 ofs_geom << argv[i] << " ";
		}
		ofs_geom << argv[argc-1] << "\n";
		int points_n = p->getDataLength();
		int faces_n = 0;
		for(vector< vector< int > >::iterator it = this->result.begin() ; it != this->result.end() ; ++it){
			faces_n+=(*it).size();
		}
		faces_n *= 4;
		ofs_geom << points_n << " " << faces_n << " 0\n";
		ofs_geom << setprecision(12);
		for(int k = 0 ; k < p->getDataLength() ; k++){
			float* point = p->getById(k);
			ofs_geom << point[0] << "\t" << point[1] << "\t" << point[2] << "\n";
		}
		for(vector< vector< int > >::iterator it = this->result.begin() ; it != this->result.end() ; ++it){
			int rgb[3];
			for (int i = 0; i < 3; i++) {
				rgb[i] = rand() & 255;
			}
			for(vector<int>::iterator void_it = (*it).begin(); void_it != (*it).end() ; ++void_it) {
				Tetrahedron tetrahedron = tetrahedraDict->getById(*void_it);
				int *points_aux = tetrahedron.getPointsId();
				if (halfp(p->getById(points_aux[0]), p->getById(points_aux[1]), p->getById(points_aux[2]), p->getById(points_aux[3])) < 0) {
					ofs_geom << "3 " << points_aux[0] << " " << points_aux[1] << " " << points_aux[2] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
					ofs_geom << "3 " << points_aux[2] << " " << points_aux[3] << " " << points_aux[0] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
					ofs_geom << "3 " << points_aux[3] << " " << points_aux[2] << " " << points_aux[1] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
					ofs_geom << "3 " << points_aux[1] << " " << points_aux[0] << " " << points_aux[3] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
				} else {
					ofs_geom << "3 " << points_aux[0] << " " << points_aux[1] << " " << points_aux[3] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
					ofs_geom << "3 " << points_aux[3] << " " << points_aux[2] << " " << points_aux[0] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
					ofs_geom << "3 " << points_aux[2] << " " << points_aux[3] << " " << points_aux[1] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
					ofs_geom << "3 " << points_aux[1] << " " << points_aux[0] << " " << points_aux[2] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
				}
			}
		}
	}
	ofs_geom.close();
}

void VoidJoiner::printFeatures(string filename, TetrahedronDictionary *tetrahedraDict){
	string outputFile_feat = filename+string("_features.out");
	ofstream ofs_feat(outputFile_feat.c_str());
	if(ofs_feat.good()){
		for(vector< vector< int > >::iterator it = this->result.begin() ; it != this->result.end() ; ++it){
			double cx = 0.0;
			double cy = 0.0;
			double cz = 0.0;
			double vol = 0.0;
			double r;
			for(vector<int>::iterator void_it = (*it).begin(); void_it != (*it).end() ; ++void_it){
				Tetrahedron tetrahedron = tetrahedraDict->getById(*void_it);
				double u_vol = tetrahedron.getVolume();
				float *centroid = tetrahedron.getCentroid();
				double dx = centroid[0]*u_vol;
				double dy = centroid[1]*u_vol;
				double dz = centroid[2]*u_vol;
				vol += u_vol;
				cx += dx;
				cy += dy;
				cz += dz;
			}
			cx /= vol;
			cy /= vol;
			cz /= vol;
			r = cbrt(.75/M_PI*vol);
			ofs_feat << cx << " " << cy << " " << cz << " " << r << endl;
		}
	}
	ofs_feat.close();
}