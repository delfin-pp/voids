/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/Tetrahedron.hpp
 * 
 */
#ifndef __TETRAHEDRON__H
#define __TETRAHEDRON__H
#include <iostream>
#include "PointDictionary.hpp"
#include "EdgeDictionary.hpp"
using namespace std;

/**
 * @brief class for Tetrahedron in a 3D mesh 
 * 
 */
class Tetrahedron{
	private:
		int id; /* Tetrahedron id */
		int pointsId[4]; /* Tetrahedron's vertex id */
		int neighboursId[4]; /* Ids of neighboring tetrahedra */
		int edgesId[6]; /* Tetrahedron's edges id */
		PointDictionary* pointsDict;
		EdgeDictionary* edgesDict;
		float longestEdge; /* Length of longests edge */
		float volume; /* Volume of the tetrahedron */
		float centroid[3]; /* Coordinates of tetrahedron's centroid  */
		int voidId; /* Id of the void to which it belongs */
		float aspectRatioGamma; /* Tetrahedron's aspect ratio gamma ARG */
	public:
		/**
		 * @brief Construct a new Tetrahedron object
		 * 
		 */
		Tetrahedron();
		/**
		 * @brief Construct a new Tetrahedron object
		 * 
		 * @param id Tetrahedron id
		 * @param points Tetrahedron's vertex id
		 * @param neighbours Ids of neighboring tetrahedra
		 */
		Tetrahedron(int,int[4],int[4]);
		/**
		 * @brief Get the Id of the Tetrahedron
		 * 
		 * @return int The id
		 */
		int getId();
		/**
		 * @brief Set the Id of the Tetrahedron
		 * 
		 * @param id The id of the tetrahedron
		 */
		void setId(int);
		/**
		 * @brief Get the Points Id
		 * 
		 * Get the 4 points (vertex) id of the tetrahedron.
		 * 
		 * @return int* Array of ids
		 */
		int* getPointsId();
		/**
		 * @brief Set the Points Id
		 * 
		 * Set the 4 points (vertex) id of the tetrahedron.
		 * Each vertex must be opposite to the neighbour in the
		 * same possition of the array. The points must be in ccw
		 * order.
		 * 
		 * @param points Array with vertex id
		 * 
		 */
		void setPointsId(int[4]);
		/**
		 * @brief Get the Neighbours Id
		 * 
		 * Get the id of the 4 neighbours of the tetrahedron.
		 * 
		 * @return int* Array of ids
		 */
		int* getNeighboursId();
		/**
		 * @brief Set the Neighbours Id
		 * 
		 * Set the id of the 4 neighboring tetrahedra.
		 * 
		 * @param neighbours Array with neighbours id
		 */
		void setNeighboursId(int[4]);
		/**
		 * @brief Get the Edges Id
		 * 
		 * Get the 6 edges id of the tetrahedron.
		 * 
		 * @return int* Array of ids
		 */
		int* getEdgesId();
		/**
		 * @brief Set the Edges Id
		 * 
		 * Set the id of the 6 edges of the tetrahedron.
		 * 
		 * @param edgesId Array with edges id
		 * 
		 */
		void setEdgesId(int[6]);
		/**
		 * @brief Get the Void Id
		 * 
		 * Get the id of the void to which the tetrahedron belongs.
		 * If the tetrahedron isn't part of a void, returns -1.
		 * 
		 * @return int The void id
		 */
		int getVoidId();
		/**
		 * @brief Set the Void Id
		 * 
		 * Set the id of the void of the tetrahedron.
		 * 
		 * @param voidId The void id
		 * 
		 */
		void setVoidId(int);
		/**
		 * @brief Get the Longest Edge
		 * 
		 * Get the lenght of the longest edge of the tetrahedron.
		 * 
		 * @return float The length of the longest edge
		 */
		float getLongestEdge();
		/**
		 * @brief Set the Point Dictionary
		 * 
		 * @param pointsDict Reference to the Dictionary of points
		 * 
		 */
		void setPointDictionary(PointDictionary*);
		/**
		 * @brief Set the Edge Dictionary
		 * 
		 * @param edgesDict Reference to the Dictionary of edges
		 * 
		 */
		void setEdgeDictionary(EdgeDictionary*);
		/**
		 * @brief Get the Volume of the tetrahedron.
		 * 
		 * @return float The volume
		 */
		float getVolume();
		/**
		 * @brief Get the Centroid
		 * 
		 * Get an array with 3 elements that are the coordinates of
		 * the centroid of the tetrahedron.
		 * 
		 * @return float* Centroid array
		 */
		float* getCentroid();
		/**
		 * @brief Returns if the tetrahedron its in border
		 *
		 * Returns 1 if the tetrahedron is in border of the 3d mesh,
		 * 0 otherwise.
		 *  
		 * @return int If its in border
		 */
		int isInBorder();
		/**
		 * @brief Returns if the tetrahedron is a sliver
		 * 
		 * Returns if the tetrahedron is a sliver according to the rule
		 * that a sliver has solid angles less than 20° or greater than
		 * 160°
		 * 
		 * @return true If its a sliver
		 * @return false Otherwise
		 */
		bool isSliver();
		/**
		 * @brief Get the Aspect Ratio Gamma of the tetrahedron
		 * 
		 * Get or compute the Aspect Ratio Gamma (ARG). 
		 * The arg, when computed with signed volume, detects the most 
		 * common cases of invalid and poor quality tetrahedra: flat, 
		 * needle and inversion.
		 * ARG = R^3 * sqrt(2) / (12 * V)
		 * R = (6^-1 * sum(li^2))^(1/2)
		 * li: length of edge i
		 * 
		 * @return float The ARG
		 */
		float getARG();
		/**
		 * @brief Resets values of variables
		 * 
		 * Set the variables longestEdge and volume to 0.
		 * 
		 */
		void reset();
};

#endif