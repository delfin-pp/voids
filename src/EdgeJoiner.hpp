/**
 * DELFIN++ -- Delaunay Edge Void Finder (++) -- ./src/EdgeJoiner.hpp
 * 
 * EdgeJoiner is a class used in a previous version of the DELFIN algorithm
 * and is not used in DELFIN++.
 * Maintained in the files to preserve the completeness of the previous version.
 * 
 */
#ifndef __EDGEJOINER__H
#define __EDGEJOINER__H
#include <vector>
#include "VoidJoiner.hpp"
using namespace std;

/**
 * @brief class to join voids previously found in a 3D mesh using
 * a length of edge criteria
 * 
 */
class EdgeJoiner : public VoidJoiner{
private:
	vector<int> parent; /* Parent of every subset for Union-Find algorithm*/
	vector<int> rank; /* Rank of every subset for Union-Find algorithm*/
	float minEdgeLength; /* Minimum edge length that must be shared between voids to be joined*/
	bool discardBorder; /* True if voids with edge in the mesh boundary must be discarded*/
	/**
	 * @brief Union operation for the Union-Find algorithm
	 * 
	 * A function to do union by rank of two subsets i, j
	 * 
	 * @param i Subset i
	 * @param j Subset j
	 */
	void djUnion(int,int);
	/**
	 * @brief Find operation for the Union-Find algorithm
	 * 
	 * A function to find the subset of an element i
	 * 
	 * @param i Element i 
	 * @return int Subset of the element i
	 */
	int djFind(int);
public:
	/**
	 * @brief Construct a new Edge Joiner object
	 * 
	 * @param _minEdgeLength Value for minEdgeLength
	 * @param _discardBorder Value for discardBorder
	 */
	EdgeJoiner(float,bool);
	/**
	 * @brief Destroy the Edge Joiner object
	 * 
	 */
	~EdgeJoiner();
	/**
	 * @brief Join voids
	 * 
	 * Joins subvoids according to the length of the edge shared.
	 * If the edge shared is longer than minEdgeLength, then the
	 * subvoids must be joined, otherwise they remain as separated
	 * voids. 
	 * The joining process is done with the Union-Find algorithm.
	 * 
	 * @param subvoids Set of voids to join
	 * @param tetrahedraDict The dictionary that contains the mesh
	 * 
	 */
	void join(vector< vector< int > >,TetrahedronDictionary*);
};

#endif
