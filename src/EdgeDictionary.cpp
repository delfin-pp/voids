/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/EdgeDictionary.cpp
 * 
 */
#include "EdgeDictionary.hpp"

EdgeDictionary::EdgeDictionary(): Dictionary<Edge>(){
	data_n = 0;
}
EdgeDictionary::~EdgeDictionary(){}

int EdgeDictionary::add(Edge* _edge){
	return 0;
}

int EdgeDictionary::add(int x,int y, int tetrahedronId){ // Edge without ID
	int pointsId[2];
	pointsId[0] = x;
	pointsId[1] = y; 
	// Always keep lower point id as first value inside key map in edgesDict1
	if(pointsId[0] > pointsId[1]) {
		// Swap
		pointsId[0] ^= pointsId[1];
		pointsId[1] ^= pointsId[0];
		pointsId[0] ^= pointsId[1];
	}
	// Search if _edge exists in edgesDict
	map< pair<int,int> , int >::iterator it = edgesDict1.find(make_pair(pointsId[0],pointsId[1]));
	// If exist return edge->id
	if(it != edgesDict1.end()){
		edgesTetrahedraId[it->second].push_back(tetrahedronId);
		return it->second;
	}else{
	// Else data_n++ and return id;
		int returnId = data_n;
		inBorder.push_back(0);
		edgesDict1.insert(make_pair(make_pair(pointsId[0],pointsId[1]),returnId));
		edgesDict2.push_back(make_pair(pointsId[0],pointsId[1]));
		vector<int> aux;
		aux.push_back(tetrahedronId);
		edgesTetrahedraId.push_back(aux);
		++data_n;
		return returnId;
	}
}

Edge EdgeDictionary::getById(int _id){
	Edge edge;
	edge.setId(_id);
	int pointsId[2] = {edgesDict2[_id].first,edgesDict2[_id].second};
	edge.setPointsId(pointsId);
	edge.setTetrahedraId(edgesTetrahedraId[_id]);
	edge.setPointDictionary(this->pointsDict);
	edge.setInBorder(inBorder[_id]);
	return edge;
}

void EdgeDictionary::setPointDictionary(PointDictionary* _pointsDict){
	pointsDict = _pointsDict;
}

void EdgeDictionary::setInBorder(int _id, int _inBorder){
	inBorder[_id] = _inBorder;
	return;
}