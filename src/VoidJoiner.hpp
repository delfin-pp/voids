/**
 * DELFIN++ -- Delaunay Edge Void Finder (++) -- ./src/VoidJoiner.hpp
 * 
 * VoidJoiner is a class used in a previous version of the DELFIN algorithm
 * and is not used in DELFIN++.
 * Maintained in the files to preserve the completeness of the previous version.
 * 
 */
#ifndef __VOIDJOINER__H
#define __VOIDJOINER__H
#include <iostream>
#include <vector>
#include "TetrahedronDictionary.hpp"
#include "VoidSpace.hpp"
#include <stdio.h>
#include <float.h>
#include <math.h> 
using namespace std;

/**
 * @brief abstract class to join voids previously found in a 3D mesh
 * 
 */
class VoidJoiner{
protected:
	vector<VoidSpace *> voids; /* Set of identified voids as VoidSpace objects */
	vector< vector<int> > result; /* Set of identified voids as vectors of tetrahedra id */
	/**
	 * @brief Construct a new Void Joiner object
	 * 
	 */
	VoidJoiner(){};
public:
	/**
	 * @brief Destroy the Void Joiner object
	 * virtual method
	 * 
	 */
	virtual ~VoidJoiner(){};
	/**
	 * @brief Join voids
	 * virtual method
	 * 
	 * @param subvoids Set of voids to join
	 * @param tetrahedraDict The dictionary that contains the mesh
	 */
	virtual void join(vector< vector< int > >,TetrahedronDictionary*) = 0;
	/**
	 * @brief Get the Result vector
	 * 
	 * Gets the result vector, that contains vectors that represents
	 * voids. Each contained vectors has the ids of the tetrahedra that
	 * are part of the void.
	 * 
	 * @return vector< vector< int > > Result vector
	 */
	vector< vector< int > > getResult();
	/**
	 * @brief Writes an OFF file of the voids
	 * virtual method
	 * Writes and OFF file of the joined polyhedra/voids, with a random
	 * color for each void.
	 * 
	 * @param filename String with the prefix of the name of the output OFF file
	 * @param tetrahedraDict The dictionary that contains the mesh
	 * @param argc Number of arguments (main)
	 * @param argv Array with arguments (main)
	 */
	virtual void printResultOFF(string,TetrahedronDictionary*,int, char *argv[]);
	/**
	 * @brief Writes an OUT file with geometric and positional 
	 * information of each void
	 * 
	 * Write an OUT file with information of each void.
	 * For each void computes and writes the mass center, and the
	 * effective radius.
	 * 
	 * @param filename String with the prefix of the name of the output OUT file
	 * @param tetrahedraDict The dictionary that contains the mesh
	 */
	void printFeatures(string,TetrahedronDictionary*);
	/**
	 * @brief Get the Voids vector
	 * 
	 * Get the joined voids vector, with each void as an VoidSpace
	 * object.
	 * 
	 * @param tetrahedraDict The dictionary that contains the mesh
	 * @return vector<VoidSpace *> 
	 */
	vector<VoidSpace *> getVoids(TetrahedronDictionary*);
};


#endif