/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/EdgeVolumeDensity.cpp
 * 
 */
#include "EdgeVolumeDensity.hpp"
#include <algorithm>
#include <cmath>
#include <stdlib.h>


void EdgeVolumeDensity::apply(TetrahedronDictionary *tetrahedraDict){
    EdgeDictionary* edges = tetrahedraDict->getEdgeDictionary();
    int n_edges = edges->getDataLength();
    float sum_density = 0.;
    float density;
    float volume;
    edgeDensity.resize(n_edges);
    for(int i = 0; i < n_edges; i++){
        density = 0.;
        volume = 0.;
        Edge edge = edges->getById(i);

        vector<int> tetrahedraId = edge.getTetrahedraId();
        for(auto f_it = tetrahedraId.begin(); f_it != tetrahedraId.end(); ++f_it){
            Tetrahedron tetrahedron = tetrahedraDict->getById(*f_it);
            volume += tetrahedron.getVolume();
        }

        density = 1. / volume;
        sum_density += density;
        edgeDensity[i] = make_pair(i, density);
        
    }

    mean = sum_density / n_edges;
}