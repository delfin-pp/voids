/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/EdgeDensity.cpp
 * 
 */
#include "EdgeDensity.hpp"
#include "geom3d.hpp"
#include <algorithm>
#include <cmath>
#include <stdlib.h>
#include <iostream>
#include <iomanip> 

using namespace std;

EdgeDensity::~EdgeDensity(){}

EdgeDensity::EdgeDensity():ElementDensity(){
    mean = 0.0;
}

bool EdgeDensity::isSmallerThan(pair<int, float> a, pair<int, float> b){
    return (a.second < b.second);
}

vector<pair<int, float>> EdgeDensity::getSortedEdges(){ // ascending order
    int size = int(edgeDensity.size());
    vector<pair<int, float>> sortedEdges(size);
    partial_sort_copy(edgeDensity.begin(), edgeDensity.end(), sortedEdges.begin(), sortedEdges.end(), &EdgeDensity::isSmallerThan);
    return sortedEdges;
}

vector<pair<int, float>> EdgeDensity::getEdgeDensity(){
    return edgeDensity;
}

float EdgeDensity::getEdgeIdDensity(int edgeId){
    return edgeDensity[edgeId].second;
}

void EdgeDensity::printResultOFF(string filename, TetrahedronDictionary *tetrahedraDict)
{
    string outputFile_geom = filename+string("_edgedensity_output.off");
	ofstream ofs_geom(outputFile_geom.c_str());
	srand(time(NULL));
	PointDictionary* p = tetrahedraDict->getPointDictionary();
    if(ofs_geom.good()){
		ofs_geom << "OFF\n";
		int points_n = p->getDataLength();
        int edges_n = edgeDensity.size();
		int faces_n = (tetrahedraDict->getDataLength()) * 4;
		ofs_geom << points_n << " " << faces_n << " 0\n";
		ofs_geom << setprecision(12);
		for(int k = 0 ; k < points_n ; k++){
			float* point = p->getById(k);
			ofs_geom << point[0] << "\t" << point[1] << "\t" << point[2] << "\n";
		}
        // de aqui en adelante cambia
        float a, b, c, d, e, f;
        vector<pair<int, float>> sortedEdges = this->getSortedEdges();    
        a = sortedEdges[0].second;
        b = sortedEdges[int(edges_n * 0.001)].second;
        c = sortedEdges[int(edges_n * 0.01)].second;
        d = sortedEdges[int(edges_n * 0.1)].second;
        e = sortedEdges[int(edges_n * 0.5)].second;
        f = sortedEdges[edges_n-1].second;

        for(int tetraId = 0; tetraId < faces_n/4 ; tetraId++){
            Tetrahedron tetrahedron = tetrahedraDict->getById(tetraId);
			int *points_aux = tetrahedron.getPointsId();
            int *edges_aux = tetrahedron.getEdgesId();

            float d_e0 = getEdgeIdDensity(edges_aux[0]);
            float d_e1 = getEdgeIdDensity(edges_aux[1]);
            float d_e2 = getEdgeIdDensity(edges_aux[2]);
            float d_e3 = getEdgeIdDensity(edges_aux[3]);
            float d_e4 = getEdgeIdDensity(edges_aux[4]);
            float d_e5 = getEdgeIdDensity(edges_aux[5]);


            float w = min(min(d_e0, d_e1), d_e3); // 0 1 2
            float x = min(min(d_e1, d_e2), d_e5); // 2 3 0
            float y = min(min(d_e3, d_e4), d_e5); // 1 2 3
            float z = min(min(d_e0, d_e2), d_e4); // 0 1 3

            vector<int> rgb_w = getRGB(a,b,c,d,e,f,w); // 0 1 2
            vector<int> rgb_x = getRGB(a,b,c,d,e,f,x); // 2 3 0
            vector<int> rgb_y = getRGB(a,b,c,d,e,f,y); // 1 2 3
            vector<int> rgb_z = getRGB(a,b,c,d,e,f,z); // 0 1 3
            
			if (halfp(p->getById(points_aux[0]), p->getById(points_aux[1]), p->getById(points_aux[2]), p->getById(points_aux[3])) < 0) {
				ofs_geom << "3 " << points_aux[0] << " " << points_aux[1] << " " << points_aux[2] 
							<< " " << rgb_w[0] << " " << rgb_w[1] << " " << rgb_w[2] << "\n";
				ofs_geom << "3 " << points_aux[2] << " " << points_aux[3] << " " << points_aux[0] 
							<< " " << rgb_x[0] << " " << rgb_x[1] << " " << rgb_x[2] << "\n";
				ofs_geom << "3 " << points_aux[3] << " " << points_aux[2] << " " << points_aux[1] 
							<< " " << rgb_y[0] << " " << rgb_y[1] << " " << rgb_y[2] << "\n";
				ofs_geom << "3 " << points_aux[1] << " " << points_aux[0] << " " << points_aux[3] 
							<< " " << rgb_z[0] << " " << rgb_z[1] << " " << rgb_z[2] << "\n";
			} else {
				ofs_geom << "3 " << points_aux[0] << " " << points_aux[1] << " " << points_aux[3] 
							<< " " << rgb_z[0] << " " << rgb_z[1] << " " << rgb_z[2] << "\n";
				ofs_geom << "3 " << points_aux[3] << " " << points_aux[2] << " " << points_aux[0] 
							<< " " << rgb_x[0] << " " << rgb_x[1] << " " << rgb_x[2] << "\n";
				ofs_geom << "3 " << points_aux[2] << " " << points_aux[3] << " " << points_aux[1] 
							<< " " << rgb_y[0] << " " << rgb_y[1] << " " << rgb_y[2] << "\n";
				ofs_geom << "3 " << points_aux[1] << " " << points_aux[0] << " " << points_aux[2] 
							<< " " << rgb_w[0] << " " << rgb_w[1] << " " << rgb_w[2] << "\n";
			}

        }

	}
	ofs_geom.close();
}