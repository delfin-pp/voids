/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/PointDictionary.hpp
 * 
 */
#ifndef __POINTDICTIONARY__H
#define __POINTDICTIONARY__H
#include <iostream>
#include <algorithm>
#include "Dictionary.hpp"
using namespace std;

/**
 * @brief class for a dictionary of points
 * 
 */
class PointDictionary : public Dictionary<float*>{
	private:
		float** dict; /* Matrix with coordinates of each point (nx3)*/
		int capacity; /* Capacity of the dictionary (dynamic) */
	public:
		/**
		 * @brief Construct a new Point Dictionary object
		 * 
		 */
		PointDictionary();
		/**
		 * @brief Destroy the Point Dictionary object
		 * 
		 */
		~PointDictionary();
		/**
		 * @brief Reads local file and loads its content into the dictionary
		 * 
		 * Reads the input file, resizes the matrix containing the points and
		 * fills it with the corresponding data.
		 * 
		 * @param points_file Prefix of the name of the file to load the dictionary
		 */
		void load(string);
		/**
		 * @brief Get a point by its id in the dictionary
		 * 
		 * @return float* Array with the coordinates of the point
		 */
		float* getById(int);
		/**
		 * @brief Adds a point to the dictionary
		 * 
		 * @note Makes a call to add(float*)
		 * 
		 * @param _point Point to add to the dictionary
		 * @return int Id assigned to the point
		 */
		int add(float**);
		/**
		 * @brief Adds a point to the dictionary
		 * 
		 * Adds a point to the dictionary. If prior to adding the point, the 
		 * dictionary capacity is not enough, it is doubled to contain more 
		 * points. 
		 * 
		 * @param _point Point to add to the dictionary
		 * @return int Id assigned to the point
		 */
		int add(float*);
};

#endif