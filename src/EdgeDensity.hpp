/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/EdgeDensity.hpp
 * 
 */
#ifndef __EDGEDENSITY__H
#define __EDGEDENSITY__H
#include "ElementDensity.hpp"
#include <vector>
using namespace std;

/**
 * @brief class for the density of the edges in a 3D mesh
 * 
 */
class EdgeDensity : public ElementDensity{
    private:
        /**
         * @brief Compares two pairs by the second element
         * 
         * A pair X is smaller than a pair Y if the second element of
         * X is smaller than the second element of Y.
         * 
         * @return true If the first pair is smaller than the second
         * @return false If the second pair is smaller than the first
         */
        static bool isSmallerThan(pair<int, float>, pair<int, float>);
    protected:
        vector<pair<int, float>> edgeDensity; /* Pairs with the edges' id and density */
    public:
        /**
         * @brief Construct a new Edge Density object
         * 
         */
        EdgeDensity();
        /**
         * @brief Destroy the Edge Density object
         * 
         */
        ~EdgeDensity();
        /**
         * @brief Get the vector with the Edge Density sorted by ascending density
         * 
         * @return vector<pair<int, float>> Vector with pairs of edges' id and density
         * sorted by density
         */
        vector<pair<int, float>> getSortedEdges();
        /**
         * @brief Get the vector with the Edge Density
         * 
         * @return vector<pair<int, float>> Vector with pairs of edges' id and density
         */
        vector<pair<int, float>> getEdgeDensity();
        /**
         * @brief Get the density of the edge
         * 
         * @param edgeId Id of the edge
         * @return float Density of the edge
         */
        float getEdgeIdDensity(int);
        /**
         * @brief Writes an OFF file of the mesh
         * 
         * Writes an OFF file of the mesh color coding the faces of the
         * tetraedra according to the density values of its edges.
         * 
         * @param filename String with the name of the output OFF file
         * @param tetrahedronDictionary The dictionary that contains the mesh
         */
        void printResultOFF(string, TetrahedronDictionary*);
};

#endif