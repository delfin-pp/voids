/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/VoidShape.hpp
 * 
 */
#ifndef __VOIDSHAPE_H
#define __VOIDSHAPE_H
#include "PointDictionary.hpp"
#include <vector>
using namespace std;

/**
 * @brief class to obtain geometric measurements on polyhedra composed
 *  of a set of tetrahedra, which represent cosmic voids
 * 
 */
class VoidShape{
    private:
        float ellipticity; /* Ellipticity of the void */
        float* massCenter; /* Mass center of the void */
        float* eigenValues; /* Eigen values of the inertia tensor of the void */
        float** eigenVectors; /* Eigen vectors of the inertia tensor of the void */
        float** inertiaTensor; /* Inertia tensor of the void */
    public: 
        /**
         * @brief Construct a new Void Shape object
         * 
         */
        VoidShape();
        /**
         * @brief Destroy the Void Shape object
         * 
         */
        ~VoidShape();
        /**
         * @brief Get the Ellipticity of the Void
         * 
         * @return float The ellipticity of the void
         */
        float getEllipticity();
        /**
         * @brief Get the Mass Center of the Void
         * 
         * @return float* The mass center of the void
         */
        float *getMassCenter();
        /**
         * @brief Get the Eigen Values of the Inertia Tensor of the Void
         * 
         * Gets the eigen values of the inertia tensor of the void.
         * These values are sorted in decreasing order of magnitude.
         * 
         * @return float* The Eigen Values of the Inertia Tensor
         */
        float *getEigenValues();
        /**
         * @brief Get the Eigen Vectors of the Inertia Tensor of the Void
         *
         * Gets the eigen vectors of the inertia tensor of the void.
         * These values are sorted in decreasing order of magnitude of its
         * eigen values. 
         * 
         * @return float** The Eigen Vectors of the Inertia Tensor
         */
        float **getEigenVectors();
        /**
         * @brief Get the Inertia Tensor of the Void
         * 
         * @return float** The Inertia Tensor of the Void
         */
        float **getInertiaTensor();
        /**
         * @brief Computes geometric measures of the Void
         * 
         * Given a set of points that are part of the void, computes its
         * mass center and then the inertia tensor.
         * Then, calculates the eigenvectors and eigenvalues of the inertia
         * tensor using the Jacobi method.
         * The ellipticity (oblateness) is calculated as 
         * e = 1 − (c / a)**(1/4), where a and c are the semimajor and 
         * semiminor axes, respectively.
         * 
         * @param pointsId Ids of the points in the Void
         * @param pointsDict Reference to the Dictionary of points
         */
        void analyze(vector<int>, PointDictionary*);
};


#endif
