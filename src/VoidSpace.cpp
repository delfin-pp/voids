/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/VoidSpace.cpp
 * 
 */
#include "VoidSpace.hpp"
#include "geom3d.hpp"
#include <unordered_set>


VoidSpace::~VoidSpace(){
}

VoidSpace::VoidSpace(){
	volume = 0.0;
	density = 0.0;
	id = 0;
	surfaceArea = 0.0;
	borderSurfaceArea = 0.0;
	poorQualitySurfaceArea = 0.0;
}

int VoidSpace::getId(){
	return this->id;
}
void VoidSpace::setId(int _id){
	id=_id;
}

void VoidSpace::setTetrahedronDictionary(TetrahedronDictionary *_tetrahedraDict)
{
	tetrahedraDict = _tetrahedraDict;
}
TetrahedronDictionary* VoidSpace::getTetrahedronDictionary()
{
	return this->tetrahedraDict;
}

vector<int> VoidSpace::getTetrahedraId(){
	return this->tetrahedraId;
}
void VoidSpace::setTetrahedraId(vector<int> _tetrahedraId){
	tetrahedraId = _tetrahedraId;
	for(auto t_it = tetrahedraId.begin(); t_it != tetrahedraId.end(); ++t_it ){
		tetrahedraDict->setTetrahedronVoidId(*t_it, this->id);
	}
}

void VoidSpace::addTetrahedronId(int _id){
	tetrahedraId.push_back(_id);
	tetrahedraDict->setTetrahedronVoidId(_id,this->id);
	if (pointsId.size() > 0) pointsId.clear();
	if (volume > 0) volume = 0.0;
	if (density > 0) density = 0.0;
}

int VoidSpace::getTetrahedraLength(){
	return tetrahedraId.size();
}


int VoidSpace::isInBorder(){
	for(vector<int>::iterator it = tetrahedraId.begin() ; it != tetrahedraId.end() ; ++it){
		if(tetrahedraDict->getById(*it).isInBorder() == 1){
			return 1;
		}
	}
	return 0;
}

void VoidSpace::clearTetrahedra(){
	tetrahedraId.clear();
	volume = 0.0;
	density = 0.0;
	surfaceArea = 0.0;
	borderSurfaceArea = 0.0;
	poorQualitySurfaceArea = 0.0;
}

vector<int> VoidSpace::getPointsId(){
	if (pointsId.size() <= 0)
	{
		unordered_set<int> points;
		for (vector<int>::iterator it = tetrahedraId.begin(); it != tetrahedraId.end(); ++it)
		{
			int *aux = (tetrahedraDict->getById(*it)).getPointsId();
			for (int j = 0; j < 4; j++)
			{
				points.insert(aux[j]);
			}
		}

		pointsId.insert(pointsId.end(), points.begin(), points.end());
	}
	return pointsId;
}

float VoidSpace::getVolume()
{
	if (volume <= 0.0)
	{
		for (vector<int>::iterator it = tetrahedraId.begin(); it != tetrahedraId.end(); ++it)
		{
			volume += (tetrahedraDict->getById(*it)).getVolume();
		}
	}
	return this->volume;
}

float VoidSpace::getDensity()
{
	if (density <= 0.0)
	{
		unordered_set<int> externalPoints;
		unordered_set<int> points;
		for (vector<int>::iterator it = tetrahedraId.begin(); it != tetrahedraId.end(); ++it)
		{
			Tetrahedron tetrahedron = tetrahedraDict->getById(*it);
			int *neighbours = tetrahedron.getNeighboursId();
			int *pointsTetrahedron = tetrahedron.getPointsId();
			for (int i = 0; i < 4; i++) // for every neighbour, check if it is in the void
			{
				points.insert(pointsTetrahedron[i]);
				if (!(std::find(tetrahedraId.begin(), tetrahedraId.end(), neighbours[i]) != tetrahedraId.end()))
				{
					for (int j = 0; j < 4; j++) // shared points with the tetrahedron are part of the surface
					{
						if (j != i)
						{
							externalPoints.insert(pointsTetrahedron[j]);
						}
					}
				}
			}
		}
		int external = externalPoints.size();
		int internal = points.size() - external;
		density = internal / this->getVolume();

		if (pointsId.size() <= 0)
		{
			pointsId.insert(pointsId.end(), points.begin(), points.end());
		}
	}
	return this->density;
}

void VoidSpace::deleteExternalTetrahedra()
{
	pointsId.clear();
	density = 0.0;
	float deletedVolume = 0.0;
	bool goNext = true;
	vector<int>::iterator t_it;
	for (t_it = tetrahedraId.begin(); t_it != tetrahedraId.end();)
	{
		Tetrahedron tetrahedron = tetrahedraDict->getById(*t_it);
		int *neighbours = tetrahedron.getNeighboursId();
		for (int i = 0; i < 4; i++) // for every neighbour, check if it is in the void
		{
			if (!(std::find(tetrahedraId.begin(), tetrahedraId.end(), neighbours[i]) != tetrahedraId.end())) 
			{
				// neighbor i is not in the void, this tetrahedra is in the surface
				// so, delete the tetraedron if it has to be deleted
				if (tetrahedron.isInBorder() || (tetrahedron.getARG() > 3 || tetrahedron.getARG() < 1))
				{
					deletedVolume += tetrahedron.getVolume();
					t_it = tetrahedraId.erase(t_it);
					goNext = false;
				}
				break;
			}
		}

		if (goNext){
			t_it++;
		}
		else{
			goNext = true;
		}
	}
	if (volume > 0.0)
	{
		volume -= deletedVolume; // delete volumes of deleted tetrahedra;
	}
}

float VoidSpace::getPercentageBorderSurfaceArea()
{
	return borderSurfaceArea / surfaceArea;
}

float VoidSpace::getPercentagePoorQualitySurfaceArea(){
	return poorQualitySurfaceArea / surfaceArea;
}

void VoidSpace::analyzeSurfaceArea(){
	surfaceArea = 0.0;
	borderSurfaceArea = 0.0;
	poorQualitySurfaceArea = 0.0;
	bool poorQuality;

	PointDictionary *pointsDict = tetrahedraDict->getPointDictionary();
	for (vector<int>::iterator it = tetrahedraId.begin(); it != tetrahedraId.end(); ++it)
	{
		Tetrahedron tetrahedron = tetrahedraDict->getById(*it);
		int *neighbours = tetrahedron.getNeighboursId();
		int *pointsTetrahedron = tetrahedron.getPointsId();

		if (tetrahedron.getARG() > 3 || tetrahedron.getARG() < 1){
			poorQuality = true;
		}
		else{
			poorQuality = false;
		}

		for (int i = 0; i < 4; i++) // for every neighbour, check if it is in the void
		{
			if (!(std::find(tetrahedraId.begin(), tetrahedraId.end(), neighbours[i]) != tetrahedraId.end()))
			{
				double tArea;
				if (i == 0)
					tArea = triangleArea(pointsDict->getById(pointsTetrahedron[1]), pointsDict->getById(pointsTetrahedron[2]), pointsDict->getById(pointsTetrahedron[3]));
				else if (i == 1)
					tArea = triangleArea(pointsDict->getById(pointsTetrahedron[0]), pointsDict->getById(pointsTetrahedron[2]), pointsDict->getById(pointsTetrahedron[3]));
				else if (i == 2)
					tArea = triangleArea(pointsDict->getById(pointsTetrahedron[0]), pointsDict->getById(pointsTetrahedron[1]), pointsDict->getById(pointsTetrahedron[3]));
				else if (i == 3)
					tArea = triangleArea(pointsDict->getById(pointsTetrahedron[0]), pointsDict->getById(pointsTetrahedron[1]), pointsDict->getById(pointsTetrahedron[2]));

				surfaceArea += tArea;
				//if (neighbours[i] < 0)
				if (tetrahedron.isInBorder())
					borderSurfaceArea += tArea;
				if (poorQuality)
					poorQualitySurfaceArea += tArea;
			}
		}
	}
}