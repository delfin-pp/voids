/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/EdgeVolumeDensity.hpp
 * 
 */
#ifndef __EDGEVOLUMEDENSITY__H
#define __EDGEVOLUMEDENSITY__H
#include "EdgeDensity.hpp"
using namespace std;

/**
 * @brief class for the density of the edges in a 3D mesh
 * by the volume of its sorrounding tetrahedra
 * 
 */
class EdgeVolumeDensity : public EdgeDensity {
    public:
        /**
         * @brief Applies the density calculation on the tetrahedral mesh
         * 
         * Gets the number of edges in the mesh and resizes the edgeDensity
         * vector.
         * For every edge gets the vector with its sorrounding tetrahedra
         * and computes its volume to then compute the density.
         * From the density of every edge, the average density is calculated.
         * 
         * @param tetrahedraDict The dictionary that contains the mesh
         */
        void apply(TetrahedronDictionary*);
};

#endif