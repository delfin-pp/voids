/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/EdgeDensityBuilder.cpp
 * 
 */
#include "EdgeDensityBuilder.hpp"
#include "EdgeVolumeDensity.hpp"
#include "EdgeLengthDensity.hpp"
#include <algorithm>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include <iomanip>
#include <stack>

using namespace std;

void EdgeDensityBuilder::analyze(TetrahedronDictionary* tetrahedraDict){
    EdgeDensity * densityMetric = new EdgeVolumeDensity();
    analyze(tetrahedraDict, densityMetric, false, false);
}

void EdgeDensityBuilder::analyze(TetrahedronDictionary* tetrahedraDict, EdgeDensity* densityMetric, bool discardBorder, bool deletePoorQuality){
    cout << "Analyzing ...\n";
    EdgeDictionary* edgesDict = tetrahedraDict->getEdgeDictionary();
    densityMetric->apply(tetrahedraDict);
    vector<pair<int,float>> edgesDensity = densityMetric->getSortedEdges();
    float thresholdDensity;
    if (wallDensity)
    {
        thresholdDensity = thresholdPercent * densityMetric->getMeanDensity();
    }
    else
    {
        thresholdDensity = edgesDensity[int(thresholdPercent * edgesDensity.size())].second;
    }
    meanDensity = densityMetric->getMeanDensity();
    for(vector<pair<int, float>>::iterator it = edgesDensity.begin(); it != edgesDensity.end(); ++it){
        pair<int, float> id_dens = *it;

        if(id_dens.second > thresholdDensity) break;
    

        bool newVoid = true;
        Edge edge = edgesDict->getById(id_dens.first);
        vector<int> tetrahedraId = edge.getTetrahedraId();

        for(auto f_it = tetrahedraId.begin(); f_it != tetrahedraId.end(); ++f_it){
            Tetrahedron tetrahedron = tetrahedraDict->getById(*f_it);
            if(tetrahedron.getVoidId() != -1) {
                addTetrahedraToVoid(tetrahedraDict, tetrahedraId, tetrahedron.getVoidId());
                newVoid = false;
                break;
            }
        }
        if (newVoid){
            VoidSpace * voidSpace = new VoidSpace();
            voidSpace->setTetrahedronDictionary(tetrahedraDict);
            voidSpace->setId(voids.size());
            voids.push_back(voidSpace);
            for(auto f_it = tetrahedraId.begin(); f_it != tetrahedraId.end(); ++f_it){
                voidSpace->addTetrahedronId(*f_it);
            }
        }

    }

    discardVoids(discardBorder, deletePoorQuality);
}