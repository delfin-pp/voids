/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/meshUtils.cpp
 * 
 */
#include "meshUtils.hpp"
#include <stdlib.h>
#include <vector>
#include <thread>
#include <cmath>

float getR3(float lambda, TetrahedronDictionary*tetrahedraDict){
    PointDictionary* pointsDict = tetrahedraDict->getPointDictionary();
    EdgeDictionary* edgesDict = tetrahedraDict->getEdgeDictionary();

    vector< vector< float > > d(pointsDict->getDataLength(), vector< float >(3,numeric_limits<float>::max()));
	// Iterate over edges to calculate 3rd nearest neighbor
	for (int i = 0; i < edgesDict->getDataLength(); i++) {
		Edge e = edgesDict->getById(i);

		for (int it = 0; it < 2; it++)
		{
			int vertexId = e.getPointsId()[it];
			int j;	// Index where this edge distance should be in d[vertexId]
			
			for (j = 0; j < 3 && e.getLength() >= d[vertexId][j]; j++);

			if (j > 2) continue;
			
			// Shift d[vertexId] to the right from j to the end
			for (int k = 2; k > j; k--) {
				d[vertexId][k] = d[vertexId][k-1];
			}
			
			d[vertexId][j] = e.getLength();
		}
	}
	// Average
	double avg = 0.0;
	for (vector< vector< float > >::iterator it = d.begin(); it != d.end() ; it++) {
		avg += (*it)[2];
	}
	avg /= pointsDict->getDataLength();
	cout << "d3=" << avg << "\n";
	
	//Std. dev.
	double std = 0.0;
	for (vector< vector< float > >::iterator it = d.begin(); it != d.end() ; it++) {
		std += ((*it)[2]-avg)*((*it)[2]-avg);
	}
	std = sqrt(std/(pointsDict->getDataLength()-1));
	cout << "sigma=" << std << "\n";
	
	return (float)(avg + lambda * std);

}