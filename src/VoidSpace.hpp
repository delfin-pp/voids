/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/VoidSpace.hpp
 * 
 */
#ifndef __VOIDSPACE__H
#define __VOIDSPACE__H
#include <iostream>
#include <vector>
#include "TetrahedronDictionary.hpp"
#include "VoidShape.hpp"
using namespace std;

/**
 * @brief class for a polyhedron that represents a cosmic void
 * 
 */
class VoidSpace{
	private:
		int id; /* Void id */
		vector<int> tetrahedraId; /* Ids of the tetrahedra that compose the void */
		vector<int> pointsId; /* Ids of the points/galaxies that are part of the void */
		TetrahedronDictionary *tetrahedraDict;
		float volume; /* Volume of the void */
		float density; /* Density of the void in galaxies per volume */
		float surfaceArea; /* Surface Area of the void */
		float borderSurfaceArea; /* Surface area of the void in the edge or border of the mesh */
		float poorQualitySurfaceArea; /* Area of the void from the faces of poor quality tetrahedra */
	public:
		/**
		 * @brief Construct a new Void Space object
		 * 
		 * Sets all float and int values to zero.
		 * 
		 */
		VoidSpace();
		/**
		 * @brief Destroy the Void Space object
		 * 
		 */
		~VoidSpace();
		
		/**
		 * @brief Get the Id of the Void
		 * 
		 * @return int The id of the void
		 */
		int getId();
		/**
		 * @brief Set the Id of the Void
		 * 
		 * @param _id The id of the void
		 */
		void setId(int);
		
		/**
		 * @brief Get the Tetrahedron Dictionary
		 * 
		 * @return TetrahedronDictionary* Reference to the Dictionary of tetraedra
		 */
		TetrahedronDictionary* getTetrahedronDictionary();
		/**
		 * @brief Set the Tetrahedron Dictionary
		 * 
		 * @param _tetrahedraDict Reference to the Dictionary of tetraedra
		 */
		void setTetrahedronDictionary(TetrahedronDictionary*);
		
		/**
		 * @brief Get the Tetrahedra Id
		 * 
		 * Get the vector containing all the tetrahedra that are part of the void.
		 * 
		 * @return vector<int> Vector with the tetrahedra ids
		 */
		vector<int> getTetrahedraId();
		/**
		 * @brief Set the Tetrahedra Id
		 * 
		 * Set the id of the tetrahedra that are part of the void.
		 * To every tetrahedra, sets the void id to the id of this Void Space object.
		 * 
		 * @param _tetrahedraId Vector with the tetrahedra ids
		 */
		void setTetrahedraId(vector<int>);
		/**
		 * @brief Add the id of a tetrahedron
		 * 
		 * Add the id of a tetrahedron to the vector containing all the tetrahedra
		 * that are part of the void.
		 * Resets the values of volume and density of the void, and clear the
		 * pointsId vector.
		 * 
		 * @param _id Id of the tetrahedron
		 */
		void addTetrahedronId(int);
		/**
		 * @brief Get the number of tetrahedra that are part of the void
		 * 
		 * @return int Number of tetrahedra
		 */
		int getTetrahedraLength();
		
		/**
		 * @brief Get the Points Id
		 * 
		 * Get the vector containing the ids of all the points that are part
		 * of the void.
		 * 
		 * @return vector<int> Vector with the points ids
		 */
		vector<int> getPointsId();
		/**
		 * @brief Get the Volume of the Void
		 * 
		 * @return float Volume of the void
		 */
		float getVolume();
		/**
		 * @brief Get the Density of the Void
		 * 
		 * It computes and returns the density of the void, which is equal to
		 * the number of points in the interior (or not part of the surface)
		 * divided by the volume.
		 * 
		 * @return float Density of the void
		 */
		float getDensity();
		
		/**
		 * @brief Get the inBorder value
		 * 
		 * Computes and returs the inBorder value that indicates if the void
		 * is part of the surface of the mesh.
		 * To compute the value, the inBorder value of each tetrahedron that
		 * is part of the void is checked.
		 * 1 indicates that the void is part of the border.
		 * 0 indicates that the void is not part of the border.
		 * 
		 * @return int The inBorder value
		 */
		int isInBorder();
		/**
		 * @brief Resets the tetrahedra that are part of the void to none
		 * 
		 * Clears the tetrahedraId vector, and sets the volume, density, 
		 * surfaceArean and other float instance variable to zero.
		 * 
		 */
		void clearTetrahedra();

		/**
		 * @brief Computes meaningful surface area values.
		 * 
		 * Computes the values surfaceArea, borderSurfaceArea and 
		 * poorQualitySurfaceArea.
		 * For each void tetrahedron its ARG is computed to determine its 
		 * quality. It is checked if the tetrahedron is part of the edge 
		 * of the void and if it is, it is also checked if it is part of 
		 * the border of the 3D mesh. 
		 * 
		 */
		void analyzeSurfaceArea();
		/**
		 * @brief Get the Percentage of Surface Area that is in the border
		 * 
		 * @return float The Percentage of Surface Area in the border
		 */
		float getPercentageBorderSurfaceArea();
		/**
		 * @brief Get the Percentage of Surface Area that are part of poor 
		 * quality tetrahedra
		 * 
		 * @return float The Percentage of Surface Area of poor quality
		 */
		float getPercentagePoorQualitySurfaceArea();
		/**
		 * @brief Deletes Poor Quality tetrahedron from the edge of the Void
		 * 
		 * Poor quality tetrahedra or tetrahedra at the border of the 3D mesh
		 * are removed from the surface of the void.
		 * The void volume is updated and the pointsId vector is cleared.
		 * 
		 */
		void deleteExternalTetrahedra();
		
		
};


#endif