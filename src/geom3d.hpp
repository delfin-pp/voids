/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/geom3d.hpp
 * 
 * Geometric functions for points, vectors and planes in a 3D space
 * 
 */
#include <vector>

using namespace std;

/**
 * @brief Indicates orientation of point p respect to the plane 
 * defined by points a, b and c.
 * 
 * The orientation value is defined as follows: A negative value 
 * means that the point is below the plane. A positive value 
 * means the point is above the plane. In case that the point is
 * on the plane, the orientation value is equal to zero.
 * 
 * @param a 3D Point that defines the plane
 * @param b 3D Point that defines the plane
 * @param c 3D Point that defines the plane
 * @param p 3D Point to compute its orientation
 * @return double Orientation value
 */
double halfp(vector<double> a, vector<double> b, vector<double> c, vector<double> p);
double halfp(float *, float *, float *, float *);

/**
 * @brief Cross product between vectors u and v
 * 
 * @param u First 3D vector
 * @param v Second 3D vector
 * @return vector<double> Cross product
 */
vector<double> cross(vector<double> u, vector<double> v);

/**
 * @brief Dot product between vectors u and v
 * 
 * @param u First 3D vector
 * @param v Second 3D vector
 * @return double Dot product
 */
double dotpr(vector<double> u, vector<double> v);

/**
 * @brief Computes the area of a triangle defined by its
 * three 3D points a, b and c
 * 
 * @param a Array of coordinates of a 3D point
 * @param b Array of coordinates of a 3D point
 * @param c Array of coordinates of a 3D point
 * @return double Area of the triangle
 */
double triangleArea(float *, float *, float *);

/**
 * @brief Computes the angle between two 3D vectors
 * 
 * @param u First 3D vector
 * @param v Second 3D vector
 * @return double Angle (in degrees) between the two vectors
 */
double angle2vec(vector<double>, vector<double>);


