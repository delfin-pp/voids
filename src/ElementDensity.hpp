/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/ElementDensity.hpp
 * 
 */
#ifndef __ELEMENTDENSITY__H
#define __ELEMENTDENSITY__H
#include "TetrahedronDictionary.hpp"
#include <cmath>
using namespace std;

/**
 * @brief abstract class for the density of the geometric elements
 * in a 3D mesh
 * 
 */
class ElementDensity{
    public:
        /**
         * @brief Construct a new Element Density object
         * 
         */
        ElementDensity(){};
        /**
         * @brief Destroy the Element Density object
         * 
         */
        virtual ~ElementDensity(){};
        /**
         * @brief Applies the density calculation on a tetrahedral mesh
         * virtual method
         * 
         * @param tetrahedraDict The dictionary that contains the mesh
         */
        virtual void apply(TetrahedronDictionary*) = 0;
        /**
         * @brief Get the Mean Density value
         * 
         * @return float The mean density of the elements
         */
        float getMeanDensity(){return mean;}
        /**
         * @brief Writes an OFF file of the mesh
         * virtual method
         * Writes an OFF file of the mesh color coding the elements according 
         * to its density value.
         * 
         * @param filename String with the name of the output OFF file
         * @param tetrahedronDictionary The dictionary that contains the mesh
         */
        virtual void printResultOFF(string, TetrahedronDictionary*) = 0;
    protected:
        float mean; /* Mean density value */
        /**
         * @brief Computes the RGB values to a certain density
         * 
         * From a density value, its RGB value is calculated in a color palette
         * divided into 5 ranges (using 6 density limit values). The darker the
         * color generated, the darker the value is in a low density range (low 
         * presence of elements).
         * 
         * @param a Initial value of the first density range
         * @param b Initial value of the second density range
         * @param c Initial value of the third density range
         * @param d Initial value of the fourth density range
         * @param e Initial value of the fifth density range
         * @param f Final value of the fifth density range
         * @param p Density for which the RGB value is calculated
         * @return vector<int> Vector with the RGB value of p
         */
        vector<int> getRGB(float a, float b, float c, float d, float e, float f, float p)
        {
            int R[6] = {189, 240, 253, 254, 254, 255};
            int G[6] = {0, 59, 141, 178, 217, 255};
            int B[6] = {38, 32, 60, 76, 118, 178};
            int i = 0;
            if (p >= a && p < b)
            {
                i = 0;
                p = (p - a) / (b - a);
            }
            else if (p >= b && p < c)
            {
                i = 1;
                p = (p - b) / (c - b);
            }
            else if (p >= c && p < d)
            {
                i = 2;
                p = (p - c) / (d - c);
            }
            else if (p >= d && p < e)
            {
                i = 3;
                p = (p - d) / (e - d);
            }
            else
            {
                i = 4;
                p = (p - e) / (f - e);
            }

            vector<int> res(3);

            res[0] = int(R[i + 1] * p + R[i] * (1. - p));
            res[1] = int(G[i + 1] * p + G[i] * (1. - p));
            res[2] = int(B[i + 1] * p + B[i] * (1. - p));

            return res;
        }
};

#endif