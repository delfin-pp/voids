/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/PointDensityBuilder.hpp
 * 
 */
#ifndef __POINTDENSITYBUILDER__H
#define __POINTDENSITYBUILDER__H
#include <iostream>
#include <vector>
#include "PointDensity.hpp"
#include "DensityBuilder.hpp"
#include "TetrahedronDictionary.hpp"
using namespace std;

/**
 * @brief class for Void Finder via density of points in a 3D
 * tetrahedral mesh.
 * 
 */
class PointDensityBuilder : public DensityBuilder{
    using DensityBuilder::DensityBuilder;
    public:
        /**
         * @brief Identifies the voids in a set of tetrahedra using a
         * point density metric
         * 
         * Identifies the voids in a set of tetrahedra by calling the 
         * specialization that uses a point density metric. The density
         * metric used is PointVolumeDensity, and both booleans parameters
         * are set as false.
         * 
         * @param tetrahedraDict The dictionary that contains the mesh
         */
        void analyze(TetrahedronDictionary*);
        /**
         * @brief Identifies the voids in a set of tetrahedra using a
         * point density metric
         * 
         * Identifies the voids in a set of tetrahedra using a point
         * density metric. Applies the point density metric to the mesh, 
         * so that it computes the density of every point. After that, 
         * sorts the points by ascending density and evaluates every point
         * until a defined density threshold (this threshold can be
         * dependent on the mean density of the mesh).
         * For each point, check if its tetrahedra are part of a void. 
         * If they are, they will all be part of the same void (voids 
         * are joined if necessary), if they are not, a new void is 
         * created with the tetrahedra sharing the point.
         * Finally, the post-process is called to discard voids (if 
         * necessary).
         * 
         * @param tetrahedraDict The dictionary that contains the mesh
         * @param densityMetric Density metric to compute the point density
         * @param discardBorder Bool to discard voids that have and edge in
         * the boundary of the mesh
         * @param deletePoorQuality Bool to delete poor quality tetrahedra
         * of the surface of a void
         */
        void analyze(TetrahedronDictionary*, PointDensity*, bool, bool);
        /**
         * @brief Write an OUT file with the points that are part of every
         * void
         * 
         * Writes an OUT file of the points of each void, writing the id,
         * the position and the density of the point, as calculated by
         * the density metric.
         * This file is formatted as:
         * void ID, particle ID, x, y, z, density
         * The first line of the file contains the following string:
         * "# void ID, particle ID, x, y, z, density"
         * 
         * @param filename String with the prefix of the output file
         * @param tetrahedraDict The dictionary that contains the mesh
         * @param densityMetric Density metric with the computed point density
         */
        void printVoidsPoints(string, TetrahedronDictionary*, PointDensity*);
    
};

#endif
