/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/geom3d.cpp
 * 
 */
#include "geom3d.hpp"
#include <cmath>

double halfp(vector<double> a, vector<double> b, vector<double> c, vector<double> p) {
	vector<double> ab(3);
	vector<double> ac(3);
	vector<double> ap(3);
	for (int i = 0; i < 3; i++) {
		ab[i] = b[i] - a[i];
		ac[i] = c[i] - a[i];
		ap[i] = p[i] - a[i];
	}
	return dotpr(cross(ab, ac), ap);
}

double halfp(float *a, float *b, float *c, float *p) {
	vector<double> ab(3);
	vector<double> ac(3);
	vector<double> ap(3);
	for (int i = 0; i < 3; i++) {
		ab[i] = b[i] - a[i];
		ac[i] = c[i] - a[i];
		ap[i] = p[i] - a[i];
	}
	return dotpr(cross(ab, ac), ap);
}

vector<double> cross(vector<double> u, vector<double> v) {
	vector<double> cp(3);
	for (int i = 0; i < 3; i++) {
		cp[i] = u[(i+1)%3] * v[(i+2)%3] - u[(i+2)%3] * v[(i+1)%3];
	}
	return cp;
}

double dotpr(vector<double> u, vector<double> v) {
	return u[0]*v[0]+u[1]*v[1]+u[2]*v[2];
} 

double triangleArea(float *a, float *b, float *c){
	vector<double> e1(3);
	vector<double> e2(3);
	vector<double> e3(3);

	for (int i = 0; i < 3; i++)
	{
		e1[i] = b[i] - a[i];
		e2[i] = c[i] - a[i];
	}

	e3 = cross(e1, e2);
	return 0.5 * sqrt(dotpr(e3, e3));
}

double angle2vec(vector<double> u, vector<double> v)
{
	double angle;
	double angle_cos;
	double u_norm;
	double uv_dot;
	double v_norm;

	uv_dot = dotpr(u, v);
	u_norm = sqrt(dotpr(u, u));
	v_norm = sqrt(dotpr(v, v));

	angle_cos = uv_dot / (u_norm * v_norm);
	angle = acos(angle_cos) * 180 / M_PI;

	return angle;
}