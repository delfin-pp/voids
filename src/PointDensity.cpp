/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/PointDensity.cpp
 * 
 */
#include "PointDensity.hpp"
#include "geom3d.hpp"
#include <algorithm>
#include <cmath>
#include <stdlib.h>
#include <iostream>
#include <iomanip> 

using namespace std;


PointDensity::~PointDensity(){}

PointDensity::PointDensity():ElementDensity(){
    mean = 0.0;
}

bool PointDensity::isSmallerThan(pair<int, float> a, pair<int, float> b){
    return (a.second < b.second);
}

vector<pair<int,float>> PointDensity::getSortedPoints(){
    int size = int(pointDensity.size());
    vector<pair<int, float>> sortedPoints(size);
    partial_sort_copy(pointDensity.begin(), pointDensity.end(), sortedPoints.begin(), sortedPoints.end(), &PointDensity::isSmallerThan);
    return sortedPoints;
}


vector<pair<int,float>> PointDensity::getPointDensity(){
    return pointDensity;
}

float PointDensity::getPointIdDensity(int pointId){
    return pointDensity[pointId].second;
}

vector<int> PointDensity::getPointIdTetrahedra(int pointId){
    return pointTetrahedra[pointId];
}

void PointDensity::printResultOFF(string filename, TetrahedronDictionary* tetrahedraDict){
    string outputFile_geom = filename+string("_pointdensity_output.off");
	ofstream ofs_geom(outputFile_geom.c_str());
	srand(time(NULL));
	PointDictionary* p = tetrahedraDict->getPointDictionary();
    if(ofs_geom.good()){
		ofs_geom << "OFF\n";
		int points_n = p->getDataLength();
		int faces_n = (tetrahedraDict->getDataLength()) * 4;
		ofs_geom << points_n << " " << faces_n << " 0\n";
		ofs_geom << setprecision(12);
		for(int k = 0 ; k < points_n ; k++){
			float* point = p->getById(k);
			ofs_geom << point[0] << "\t" << point[1] << "\t" << point[2] << "\n";
		}
        // de aqui en adelante cambia
        float a, b, c, d, e, f;
        vector<pair<int, float>> sortedPoints = this->getSortedPoints();
        a = sortedPoints[0].second;
        b = sortedPoints[int(points_n * 0.001)].second;
        c = sortedPoints[int(points_n * 0.01)].second;
        d = sortedPoints[int(points_n * 0.1)].second;
        e = sortedPoints[int(points_n * 0.5)].second;
        f = sortedPoints[points_n-1].second;

        for(int tetraId = 0; tetraId < faces_n/4 ; tetraId++){
            Tetrahedron tetrahedron = tetrahedraDict->getById(tetraId);
			int *points_aux = tetrahedron.getPointsId();

            float d_p0 = getPointIdDensity(points_aux[0]);
            float d_p1 = getPointIdDensity(points_aux[1]);
            float d_p2 = getPointIdDensity(points_aux[2]);
            float d_p3 = getPointIdDensity(points_aux[3]);

            float w = min(min(d_p0, d_p1), d_p2); /// 3;
            float x = min(min(d_p2, d_p3), d_p0); /// 3;
            float y = min(min(d_p1, d_p2), d_p3); /// 3;
            float z = min(min(d_p0, d_p1), d_p3); /// 3;

            vector<int> rgb_w = getRGB(a,b,c,d,e,f,w); // 0 1 2
            vector<int> rgb_x = getRGB(a,b,c,d,e,f,x); // 2 3 0
            vector<int> rgb_y = getRGB(a,b,c,d,e,f,y); // 1 2 3
            vector<int> rgb_z = getRGB(a,b,c,d,e,f,z); // 0 1 3
            
			if (halfp(p->getById(points_aux[0]), p->getById(points_aux[1]), p->getById(points_aux[2]), p->getById(points_aux[3])) < 0) {
				ofs_geom << "3 " << points_aux[0] << " " << points_aux[1] << " " << points_aux[2] 
							<< " " << rgb_w[0] << " " << rgb_w[1] << " " << rgb_w[2] << "\n";
				ofs_geom << "3 " << points_aux[2] << " " << points_aux[3] << " " << points_aux[0] 
							<< " " << rgb_x[0] << " " << rgb_x[1] << " " << rgb_x[2] << "\n";
				ofs_geom << "3 " << points_aux[3] << " " << points_aux[2] << " " << points_aux[1] 
							<< " " << rgb_y[0] << " " << rgb_y[1] << " " << rgb_y[2] << "\n";
				ofs_geom << "3 " << points_aux[1] << " " << points_aux[0] << " " << points_aux[3] 
							<< " " << rgb_z[0] << " " << rgb_z[1] << " " << rgb_z[2] << "\n";
			} else {
				ofs_geom << "3 " << points_aux[0] << " " << points_aux[1] << " " << points_aux[3] 
							<< " " << rgb_z[0] << " " << rgb_z[1] << " " << rgb_z[2] << "\n";
				ofs_geom << "3 " << points_aux[3] << " " << points_aux[2] << " " << points_aux[0] 
							<< " " << rgb_x[0] << " " << rgb_x[1] << " " << rgb_x[2] << "\n";
				ofs_geom << "3 " << points_aux[2] << " " << points_aux[3] << " " << points_aux[1] 
							<< " " << rgb_y[0] << " " << rgb_y[1] << " " << rgb_y[2] << "\n";
				ofs_geom << "3 " << points_aux[1] << " " << points_aux[0] << " " << points_aux[2] 
							<< " " << rgb_w[0] << " " << rgb_w[1] << " " << rgb_w[2] << "\n";
			}

        }

	}
	ofs_geom.close();
}