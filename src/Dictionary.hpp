/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/Dictionary.hpp
 * 
 */
#ifndef __DICTIONARY__H
#define __DICTIONARY__H
#include <iostream>
#include <vector>
using namespace std;

/**
 * @brief class template for a dictionary or container of elements,
 * which get assigned an id.
 * 
 * @tparam T Type to work with/is contained in the dictionary
 */
template<class T>
class Dictionary{
	protected:
		int data_n; /* Number of elements in the dictionary */
		/**
		 * @brief Splits a string into a vector
		 * 
		 * 
		 * @param str String to split
		 * @param token Character dividing the string into split groups
		 * @return vector<string> Vector of the words in the string, separated
		 * by the delimiter token
		 */
		vector<string> split(string str, char token);
		/**
		 * @brief Construct a new Dictionary object
		 * 
		 */
		Dictionary(){};
	public:
		/**
		 * @brief Destroy the Dictionary object
		 * virtual method
		 */
		virtual ~Dictionary(){};
		/**
		 * @brief Reads a local file and loads its content into a
		 * dictionary
		 * virtual method
		 * 
		 * @param filename Prefix of the name of the file to load
		 */
		virtual void load(string) = 0;
		/**
		 * @brief Get an element by its id
		 * virtual template method
		 * 
		 * @param id Id of the expected element
		 * @return T Element searched for
		 */
		virtual T getById(int) = 0;
		/**
		 * @brief Adds an element to the dictionary
		 * virtual themplate method
		 * 
		 * @param elem Element to add to the dictionary
		 * @return int Id of the added element
		 */
		virtual int add(T*) = 0;
		/**
		 * @brief Get the length of the dictionary or number of
		 * elements in it
		 * 
		 * @return int Length of the dictionary
		 */
		int getDataLength(){return data_n;}
};


template<class T> vector<string> Dictionary<T>::split(string str, char token){
	vector<string> result;
	size_t begin = 0 ,found = 0;
	found = str.find(token,begin);
	while(found != string::npos){
		result.push_back(str.substr(begin,found-begin+1)); 
		begin = found+1;
		found = str.find(token,begin);
	}
	if(begin != str.length()){
		result.push_back(str.substr(begin,str.length()-begin+1));
	}
	return result;
}


#endif