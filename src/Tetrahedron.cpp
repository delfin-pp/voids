/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/Tetrahedron.cpp
 * 
 */
#include "Tetrahedron.hpp"
#include "geom3d.hpp"
#include <algorithm>
#include <math.h>
#include <limits>

Tetrahedron::Tetrahedron(){
	longestEdge = 0.0;
	volume = 0.0;
	aspectRatioGamma = 0.0;
	voidId = -1;
}
Tetrahedron::Tetrahedron(int _id, int _points[4], int _neighbours[4]){
	this->setId(_id);
	this->setPointsId(_points);
	this->setNeighboursId(_neighbours);
	longestEdge = 0.0;
	volume = 0.0;
	aspectRatioGamma = 0.0;
	voidId = -1;
}
int Tetrahedron::getId(){
	return this->id;
}
void Tetrahedron::setId(int _id){
	id=_id;
}
int* Tetrahedron::getPointsId(){
	return this->pointsId;
}
void Tetrahedron::setPointsId(int _pointsId[4]){
	pointsId[0]=_pointsId[0];
	pointsId[1]=_pointsId[1];
	pointsId[2]=_pointsId[2];
	pointsId[3]=_pointsId[3];
}
int* Tetrahedron::getNeighboursId(){
	return this->neighboursId;
}
void Tetrahedron::setNeighboursId(int _neighboursId[4]){
	neighboursId[0]=_neighboursId[0];
	neighboursId[1]=_neighboursId[1];
	neighboursId[2]=_neighboursId[2];
	neighboursId[3]=_neighboursId[3];
}
int* Tetrahedron::getEdgesId(){
	return this->edgesId;
}
void Tetrahedron::setEdgesId(int _edgesId[6]){
	edgesId[0]=_edgesId[0];
	edgesId[1]=_edgesId[1];
	edgesId[2]=_edgesId[2];
	edgesId[3]=_edgesId[3];
	edgesId[4]=_edgesId[4];
	edgesId[5]=_edgesId[5];
}
void Tetrahedron::setEdgeDictionary(EdgeDictionary* _edgesDict){
	edgesDict = _edgesDict;
}
void Tetrahedron::setPointDictionary(PointDictionary* _pointsDict){
	pointsDict = _pointsDict;
}
float Tetrahedron::getLongestEdge(){
	if(this->longestEdge <= 0.0){
		float max_length = 0.0;
		for(int k= 0 ; k < 6 ; k++){
			float length = edgesDict->getById(edgesId[k]).getLength();
			if(length > max_length) max_length = length;
		}
		longestEdge = max_length;
	}
	return this->longestEdge;
}
float Tetrahedron::getVolume(){
	// Matrix result from vertex a,b,c and d
	// |(a-d).((b-d)x(c-d))|/6
	if(this->volume <= 0.0){
		float* a = pointsDict->getById(pointsId[0]);
		float* b = pointsDict->getById(pointsId[1]);
		float* c = pointsDict->getById(pointsId[2]);
		float* d = pointsDict->getById(pointsId[3]);
		float a_d[3];
		float b_d[3];
		float c_d[3];
		for(int k = 0 ; k < 3 ; k++){
			a_d[k] = a[k] - d[k];
			b_d[k] = b[k] - d[k];
			c_d[k] = c[k] - d[k];
		}
		// (b-d) x (c-d)
		float b_dXc_d[3];
		b_dXc_d[0] = b_d[1]*c_d[2] - c_d[1]*b_d[2];
		b_dXc_d[1] = c_d[0]*b_d[2] - b_d[0]*c_d[2];
		b_dXc_d[2] = b_d[0]*c_d[1] - c_d[0]*b_d[1];
		float result = float(a_d[0]*b_dXc_d[0]) + float(a_d[1]*b_dXc_d[1]) + float(a_d[2]*b_dXc_d[2]);
		result = float(result/6.0);
		if(result <= 0.0 ) result= float(result*-1.0);
		volume = result;
	}
	return this->volume;
}
float* Tetrahedron::getCentroid(){
	float* v1 = pointsDict->getById(pointsId[0]);
	float* v2 = pointsDict->getById(pointsId[1]);
	float* v3 = pointsDict->getById(pointsId[2]);
	float* v4 = pointsDict->getById(pointsId[3]);

	float result[3];
	result[0] = v1[0]+v2[0]+v3[0]+v4[0];
	result[1] = v1[1]+v2[1]+v3[1]+v4[1];
	result[2] = v1[2]+v2[2]+v3[2]+v4[2];

	centroid[0] = result[0]/4.0;
	centroid[1] = result[1]/4.0;
	centroid[2] = result[2]/4.0;

	return this->centroid;
}
int Tetrahedron::isInBorder(){
	for(int k = 0 ; k < 4 ; ++k){
		if(neighboursId[k] < 0 ) return 1;
	}
	return 0;
}
void Tetrahedron::reset(){
	longestEdge = 0.0;
	volume = 0.0;
}
int Tetrahedron::getVoidId(){
	return this->voidId;
}
void Tetrahedron::setVoidId(int _voidId){
	voidId = _voidId;
}
bool Tetrahedron::isSliver(){
	//https://people.sc.fsu.edu/~jburkardt/cpp_src/tetrahedron_properties/tetrahedron_properties.cpp
	float *a = pointsDict->getById(pointsId[0]);
	float *b = pointsDict->getById(pointsId[1]);
	float *c = pointsDict->getById(pointsId[2]);
	float *d = pointsDict->getById(pointsId[3]);
	vector<double> a_b(3);
	vector<double> a_c(3);
	vector<double> a_d(3);
	vector<double> b_c(3);
	vector<double> b_d(3);
	vector<double> c_d(3);
	for (int k = 0; k < 3; k++)
	{
		a_b[k] = a[k] - b[k];
		a_c[k] = a[k] - c[k];
		a_d[k] = a[k] - d[k];
		b_c[k] = b[k] - c[k];
		b_d[k] = b[k] - d[k];
		c_d[k] = c[k] - d[k];
	}
	vector<double> abc_normal = cross(a_c, a_b);
	vector<double> abd_normal = cross(a_b, a_d);
	vector<double> acd_normal = cross(a_d, a_c);
	vector<double> bcd_normal = cross(b_c, b_d);
	vector<double> angle(6);

	angle[0] = angle2vec(abc_normal, abd_normal);
	angle[1] = angle2vec(abc_normal, acd_normal);
	angle[2] = angle2vec(abd_normal, acd_normal);
	angle[3] = angle2vec(abc_normal, bcd_normal);
	angle[4] = angle2vec(abd_normal, bcd_normal);
	angle[5] = angle2vec(acd_normal, bcd_normal);

	int i;
	for (i = 0; i < 6; i++){
		angle[i] = 180 - angle[i];
	}
	sort(angle.begin(), angle.end());

	bool res = true;
	for (int i = 0; i < 4; i++){
		res = res && (angle[i] < 20);
	}
	for (int i = 4; i < 6; i++){
		res = res && (angle[i] > 160);
	}
	return res;
}

float Tetrahedron::getARG(){
	if(aspectRatioGamma==0.0){
		float sum_l2 = 0.0;
		for (int k = 0; k < 6; k++)
		{
			float length = edgesDict->getById(edgesId[k]).getLength();
			sum_l2 += length * length;
		}
		float R = sqrt(sum_l2 / 6.0);
		float V = this->getVolume();
		if(V == 0.0){
			aspectRatioGamma = -1.0;
		}
		else {
			aspectRatioGamma = (pow(R, 3.0) * sqrt(2.0)) / (12 * V);
		}
	}
	return this->aspectRatioGamma;
}
