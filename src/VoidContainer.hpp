/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/VoidContainer.hpp
 * 
 */
#ifndef __VOIDCONTAINER__H
#define __VOIDCONTAINER__H
#include <iostream>
#include "VoidSpace.hpp"
#include "TetrahedronDictionary.hpp"
#include <map>
using namespace std;

/**
 * @brief class for a container or set of cosmic voids
 * 
 * @note currently unused
 */
class VoidContainer{
	private:
		map< int, vector<int> > voidsMap; /* Map with the voids ids and its tetrahedra */
		int data_n; /* Next id to assign to a void in the container */
		TetrahedronDictionary* tetrahedraDict;
	public:
		/**
		 * @brief Construct a new Void Container object
		 * 
		 */
		VoidContainer();
		/**
		 * @brief Destroy the Void Container object
		 * 
		 */
		~VoidContainer();
		/**
		 * @brief Add a new void to the container
		 * 
		 * Given a vector with the id of tetrahedra, adds them as a new void
		 * to the container, and assigns the id of this new void to the tetrahedra.
		 * Retursn the id of the void.
		 * 
		 * @param newVoid Vector with the id of the tetrahedra that are part of the void
		 * @return int Id assigned to the void 
		 */
		int addVoid(vector<int>);
		/**
		 * @brief Deletes void by id
		 * 
		 * Erases from voidsMap the cointainer with "id" as key
		 * 
		 * @param id Id of the void to be deleted
		 */
		void deleteVoid(int);
		/**
		 * @brief Gets a Void by its id
		 * 
		 * Given a void id, returns a Void Space with that id and
		 * the tetrahedra in the mapped value of "id" in voidsMap.
		 * 
		 * @param id Id of the void to get
		 * @return VoidSpace Void with the id
		 */
		VoidSpace getById(int);
		/**
		 * @brief Set the Tetrahedron Dictionary
		 * 
		 * @param _tetrahedraDict Reference to the Dictionary of tetrahedra
		 */
		void setTetrahedronDictionary(TetrahedronDictionary*);
		/**
		 * @brief Get the Tetrahedron Dictionary
		 * 
		 * @return TetrahedronDictionary* Reference to the Dictionary of tetrahedra
		 */
		TetrahedronDictionary* getTetrahedronDictionary();
		/**
		 * @brief Get the Next Id to assign to a new void in the container
		 * 
		 * @return int Next id of a void
		 */
		int getNextId(){ return this->data_n; }
		/**
		 * @brief Get the number of voids in the container
		 * 
		 * @return int Number of voids in the container
		 */
		int getDataLength(){ return this->voidsMap.size(); }
};

#endif
