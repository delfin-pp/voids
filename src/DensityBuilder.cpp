/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/DensityBuilder.cpp
 * 
 */
#include "DensityBuilder.hpp"
#include <unordered_set>
#include <limits>
using namespace std;

DensityBuilder::~DensityBuilder(){
    for (auto p : voids)
    {
        delete p;
    }
    voids.clear();
    voids.shrink_to_fit();
}

DensityBuilder::DensityBuilder():VoidBuilder(){
    thresholdPercent = 0.02;
    wallDensity = false;
    minVolume = 0.0;
    maxVolume = numeric_limits<float>::infinity();
}

DensityBuilder::DensityBuilder(float _thresholdPercent, bool _wallDensity, float _minVolume, float _maxVolume)
{
    thresholdPercent = _thresholdPercent;
    wallDensity = _wallDensity;
    minVolume = _minVolume;
    maxVolume = _maxVolume;
}

void DensityBuilder::addTetrahedraToVoid(TetrahedronDictionary* tetrahedraDict, vector<int> tetrahedraId, int voidId){
    for(auto f_it = tetrahedraId.begin(); f_it != tetrahedraId.end(); ++f_it){
        Tetrahedron tetrahedron = tetrahedraDict->getById(*f_it);
        if(tetrahedron.getVoidId() == -1) {
            voids[voidId]->addTetrahedronId(*f_it);
        }
        else if (voidId != tetrahedron.getVoidId()){
            joinVoids(voidId, tetrahedron.getVoidId());
        }
    }
}

void DensityBuilder::joinVoids(int firstVoid, int secondVoid){
    if (firstVoid != secondVoid && voids[firstVoid]->getId() != -1 && voids[secondVoid]->getId() != -1)
    {
        vector<int> tetrahedraInVoid = voids[secondVoid]->getTetrahedraId();
        for (int j = 0; j < voids[secondVoid]->getTetrahedraLength(); j++)
        {
            voids[firstVoid]->addTetrahedronId(tetrahedraInVoid[j]);
        }
        // delete neighbourVoid from voids
        voids[secondVoid]->setId(-1);
        voids[secondVoid]->clearTetrahedra();
    }
}


int DensityBuilder::getBiggerNeighbourVoidId(TetrahedronDictionary* tetrahedraDict, Tetrahedron tetrahedron){
    float volume = 0;
    int voidId = -1;
    int* neighbours = tetrahedron.getNeighboursId();
    for(int i = 0; i < 4; i++){
        if(neighbours[i] >= 0.0){
            int neighbourVoid = (tetrahedraDict->getById(neighbours[i])).getVoidId();
            if (neighbourVoid != voidId && neighbourVoid != -1){
                float neighVolume = voids[neighbourVoid]->getVolume();
                if(neighVolume > volume){
                    volume = neighVolume;
                    voidId = neighbourVoid;
                }
            }
        }
    }
    return voidId;
}

void DensityBuilder::discardVoids(bool discardBorder, bool deletePoorQuality){
    //float ratioBorderArea;
    float badQualityArea;
    vector<VoidSpace *>::iterator v_it;
    cout << "discardBorder " << discardBorder << endl;
    cout << "deletePoorQuality " << deletePoorQuality << endl;

    for (v_it = voids.begin(); v_it != voids.end();){
        if (((*v_it)->getId() == -1) || ((*v_it)->getVolume() < minVolume) || 
        ((*v_it)->getVolume() > maxVolume) || (discardBorder && ((*v_it)->isInBorder() == 1))){
            delete (*v_it);
            v_it = voids.erase(v_it);
        }
        else{
            (*v_it)->analyzeSurfaceArea();
            badQualityArea = (*v_it)->getPercentagePoorQualitySurfaceArea();
            if (badQualityArea > 0.90){
                delete (*v_it);
                v_it = voids.erase(v_it);
            }
            else if (deletePoorQuality && (badQualityArea > 0.55)){
                (*v_it)->deleteExternalTetrahedra();
                v_it++;
            }
            else {
                v_it++;
            }
        }
    }
    cout << voids.size() << " voids found\n";
}

int DensityBuilder::getVoidsLength(){
    return voids.size();
}

int DensityBuilder::addVoid(VoidSpace * voidSpace){
    int id = voids.size();
    voidSpace->setId(id);
    voids.push_back(voidSpace);
    return id;
}

VoidSpace* DensityBuilder::getVoid(int voidId){
    return voids[voidId];
}