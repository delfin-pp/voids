/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/VoidBuilder.cpp
 * 
 */
#include "VoidBuilder.hpp"
#include "VoidShape.hpp"
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <iomanip>
#include "geom3d.hpp"

vector< vector< int > > VoidBuilder::getResult(){
	if(result.size() <= 0)
	{
		for (int vid = 0; vid < int(voids.size()); vid++)
		{
			result.push_back(voids[vid]->getTetrahedraId());	
		}
	}
	return result;
}
void VoidBuilder::printResultOFF(string filename, TetrahedronDictionary* tetrahedraDict){
	string outputFile_geom = filename+string("_output.off");
	ofstream ofs_geom(outputFile_geom.c_str());
	srand(time(NULL));
	PointDictionary* p = tetrahedraDict->getPointDictionary();
	if(ofs_geom.good()){
		ofs_geom << "OFF\n";
		int points_n = p->getDataLength();
		int faces_n = 0;
		for(vector< VoidSpace * >::iterator it = this->voids.begin() ; it != this->voids.end() ; ++it){
			faces_n+=(*it)->getTetrahedraLength();
		}
		faces_n *= 4;
		ofs_geom << points_n << " " << faces_n << " 0\n";
		for(int k = 0 ; k < p->getDataLength() ; k++){
			float* point = p->getById(k);
			ofs_geom << setprecision(12) << point[0] << "\t" << point[1] << "\t" << point[2] << "\n";
		}
		for (vector<VoidSpace *>::iterator it = this->voids.begin(); it != this->voids.end(); ++it)
		{
			int rgb[3];
			for (int i = 0; i < 3; i++) {
				rgb[i] = rand() & 255;
			}
			vector<int> tetrahedraId = (*it)->getTetrahedraId();
			for (vector<int>::iterator void_it = tetrahedraId.begin(); void_it != tetrahedraId.end(); ++void_it)
			{
				Tetrahedron tetrahedron = tetrahedraDict->getById(*void_it);
				int *points_aux = tetrahedron.getPointsId();
				if (halfp(p->getById(points_aux[0]), p->getById(points_aux[1]), p->getById(points_aux[2]), p->getById(points_aux[3])) < 0) {
					ofs_geom << "3 " << points_aux[0] << " " << points_aux[1] << " " << points_aux[2] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
					ofs_geom << "3 " << points_aux[2] << " " << points_aux[3] << " " << points_aux[0] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
					ofs_geom << "3 " << points_aux[3] << " " << points_aux[2] << " " << points_aux[1] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
					ofs_geom << "3 " << points_aux[1] << " " << points_aux[0] << " " << points_aux[3] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
				} else {
					ofs_geom << "3 " << points_aux[0] << " " << points_aux[1] << " " << points_aux[3] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
					ofs_geom << "3 " << points_aux[3] << " " << points_aux[2] << " " << points_aux[0] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
					ofs_geom << "3 " << points_aux[2] << " " << points_aux[3] << " " << points_aux[1] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
					ofs_geom << "3 " << points_aux[1] << " " << points_aux[0] << " " << points_aux[2] 
								<< " " << rgb[0] << " " << rgb[1] << " " << rgb[2] << "\n";
				}
			}
		}
	}
	ofs_geom.close();
}


void VoidBuilder::printVoidInformation(string filename, TetrahedronDictionary*tetrahedraDict){
	string outputFileC = filename +string("_output_centers.dat.out");
	string outputFileS = filename + string("_output_shapes.dat.out");
	ofstream ofsC(outputFileC.c_str());
	ofstream ofsS(outputFileS.c_str());
	if (ofsC.good() && ofsS.good())
	{
		ofsC << "# center x,y,z, volume, radius, void ID, density" << "\n";
		ofsS << "# void ID, ellip, eig(1), eig(2), eig(3), " 
			<< "eigv(1)-x, eigv(1)-y, eigv(1)-z, "
			<< "eigv(2)-x, eigv(2)-y, eigv(2)-z, "
			<< "eigv(3)-x, eigv(3)-y, eigv(3)-z" << "\n";
		for (vector<VoidSpace *>::iterator it = this->voids.begin(); it != this->voids.end(); ++it)
		{
			float centroid[3];
			centroid[0] = 0.0;
			centroid[1] = 0.0;
			centroid[2] = 0.0;
			float volume = 0.0;
			vector<int> tetrahedraId = (*it)->getTetrahedraId();
			for (vector<int>::iterator void_it = tetrahedraId.begin(); void_it != tetrahedraId.end(); ++void_it)
			{
				Tetrahedron tetrahedron = tetrahedraDict->getById(*void_it);
				volume = volume + tetrahedron.getVolume();
				float *aux_centroid = tetrahedron.getCentroid();
				centroid[0] = centroid[0] + aux_centroid[0];
				centroid[1] = centroid[1] + aux_centroid[1];
				centroid[2] = centroid[2] + aux_centroid[2];
			}
			centroid[0] = centroid[0] / float(tetrahedraId.size());
			centroid[1] = centroid[1] / float(tetrahedraId.size());
			centroid[2] = centroid[2] / float(tetrahedraId.size());
			float radio = float(pow((float(3.0 / float(4.0 * PI)) * volume), float(1.0 / 3.0)));
			ofsC << setprecision(12) << centroid[0] << " "
				<< centroid[1] << " "
				<< centroid[2] << " "
				<< volume << " "
				<< radio << " "
				<< (*it)->getId() << " "
				<< (*it)->getDensity() << "\n";

			VoidShape *voidShape = new VoidShape();
			voidShape->analyze((*it)->getPointsId(), ((*it)->getTetrahedronDictionary())->getPointDictionary());
			float *eig = voidShape->getEigenValues();
			float **eigv = voidShape->getEigenVectors();
			ofsS << (*it)->getId() << " "
				 << voidShape->getEllipticity() << " "
				 << eig[0] << " "
				 << eig[1] << " "
				 << eig[2] << " "
				 << eigv[0][0] << " "
				 << eigv[0][1] << " "
				 << eigv[0][2] << " "
				 << eigv[1][0] << " "
				 << eigv[1][1] << " "
				 << eigv[1][2] << " " 
				 << eigv[2][0] << " "
				 << eigv[2][1] << " "
				 << eigv[2][2] << "\n";
			delete voidShape;
		}
	}
	ofsC.close();
	ofsS.close();
}

/* void VoidBuilder::printVoidInformation(string filename, TetrahedronDictionary *tetrahedraDict)
{
	string outputFileC = filename + string("_void_volume.dat.out");
	ofstream ofsC(outputFileC.c_str());
	if (ofsC.good())
	{
		for (vector<VoidSpace *>::iterator it = this->voids.begin(); it != this->voids.end(); ++it)
		{
			float volume = 0.0;
			vector<int> tetrahedraId = (*it)->getTetrahedraId();
			for (vector<int>::iterator void_it = tetrahedraId.begin(); void_it != tetrahedraId.end(); ++void_it)
			{
				Tetrahedron tetrahedron = tetrahedraDict->getById(*void_it);
				volume = volume + tetrahedron.getVolume();
			}
			ofsC << setprecision(12) << volume << "\n";
		}
	}
	ofsC.close();
} */

void VoidBuilder::printVoidsPoints(string filename, TetrahedronDictionary *tetrahedraDict){
	string outputFile = filename+string("_void_points.dat.out");
	ofstream ofs(outputFile.c_str());
	PointDictionary *p = tetrahedraDict->getPointDictionary();
	if(ofs.good()){
		int n_voids = voids.size();
		ofs << "# void ID, particle ID, x, y, z" << "\n";
		/* 		ofs << n_voids << "\n";
		for (int i = 0; i < n_voids; i++)
		{
			ofs << i << " " << (voids[i]->getPointsId()).size() << "\n";
		} */
		for (int i = 0; i < n_voids; i++)
		{
			vector<int> points = voids[i]->getPointsId();
			int vid = voids[i]->getId();
			for(vector<int>::iterator it = points.begin(); it !=points.end(); ++it){
				float * point_aux = p->getById(*it);
				ofs << vid << " " << (*it) << " " << point_aux[0] << " " << point_aux[1] << " " << point_aux[2] << "\n";
			}
		}
	}
}
