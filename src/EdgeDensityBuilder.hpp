/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./src/EdgeDensityBuilder.hpp
 * 
 */
#ifndef __EDGEDENSITYBUILDER__H
#define __EDGEDENSITYBUILDER__H
#include <iostream>
#include <vector>
#include "EdgeDensity.hpp"
#include "DensityBuilder.hpp"
#include "TetrahedronDictionary.hpp"
using namespace std;

/**
 * @brief class for Void Finder via density of edges in a 3D
 * tetrahedral mesh.
 * 
 */
class EdgeDensityBuilder : public DensityBuilder{
    using DensityBuilder::DensityBuilder;
    public:
        /**
         * @brief Identifies the voids in a set of tetrahedra using an
         * edge density metric
         * 
         * Identifies the voids in a set of tetrahedra by calling the 
         * specialization that uses an edge density metric. The density
         * metric used is EdgeVolumeDensity, and both booleans parameters
         * are set as false.
         * 
         * @param tetrahedraDict The dictionary that contains the mesh
         * 
         */
        void analyze(TetrahedronDictionary*);
        /**
         * @brief Identifies the voids in a set of tetrahedra using an
         * edge density metric
         * 
         * Identifies the voids in a set of tetrahedra using an edge
         * density metric. Applies the edge density metric to the mesh, 
         * so that it computes the density of every edge. After that, 
         * sorts the edges by ascending density and evaluates every edge
         * until a defined density threshold (this threshold can be
         * dependent on the mean density of the mesh).
         * For each edge, check if its tetrahedra are part of a void. 
         * If they are, they will all be part of the same void (voids 
         * are joined if necessary), if they are not, a new void is 
         * created with the tetrahedra sharing the edge.
         * Finally, the post-process is called to discard voids (if 
         * necessary).
         * 
         * @param tetrahedraDict The dictionary that contains the mesh
         * @param densityMetric Density metric to compute the edge density
         * @param discardBorder Bool to discard voids that have and edge in
         * the boundary of the mesh
         * @param deletePoorQuality Bool to delete poor quality tetrahedra
         * of the surface of a void
         */
        void analyze(TetrahedronDictionary*, EdgeDensity*, bool, bool);
        
    
        
};




#endif
