# contrast always equal to 10
N=( 16000)

for i in ${!N[*]}
do
    FILENAME=g_points_n${N[i]}
    valgrind --log-file="../data/scalability/leaks/m0_${FILENAME}_leak.txt" --leak-check=yes -s -q ../build/src/delfin ../data/scalability/$FILENAME -m0  -w -t0.185 -v10000

done

for i in ${!N[*]}
do
    FILENAME=g_points_n${N[i]}
    valgrind --log-file="../data/scalability/leaks/m2_${FILENAME}_leak.txt" --leak-check=yes -s -q ../build/src/delfin ../data/scalability/$FILENAME -m2  -w -t0.14 -v10000

done

for i in ${!N[*]}
do
    FILENAME=g_points_n${N[i]}
    valgrind --log-file="../data/scalability/leaks/m1_${FILENAME}_leak.txt" --leak-check=yes -s -q ../build/src/delfin ../data/scalability/$FILENAME -m1 -w -t0.38 -v10000

done

for i in ${!N[*]}
do
    FILENAME=g_points_n${N[i]}
    valgrind --log-file="../data/scalability/leaks/m3_${FILENAME}_leak.txt" --leak-check=yes -s -q ../build/src/delfin ../data/scalability/$FILENAME -m3  -w -t0.12 -v10000

done

for i in ${!N[*]}
do
    FILENAME=g_points_n${N[i]}
    valgrind --log-file="../data/scalability/leaks/m4_${FILENAME}_leak.txt" --leak-check=yes -s -q ../build/src/delfin ../data/scalability/$FILENAME -m4 -v10000

done