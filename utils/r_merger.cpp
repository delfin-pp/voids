#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>
#include <cstdlib>

using namespace std;

int main(int argc, char *argv[])
{
	vector<vector<double> > mat;
	char line[256];
	while (true) {
		cin.getline(line, 256);
		if (cin.eof())
			break;
		char *tok;
		mat.push_back(vector<double>(1, atof(tok = strtok(line, " \t"))));
		while ((tok = strtok(NULL, " \t")) != NULL) {
			mat.back().push_back(atof(tok));
		}
	}
	double sum = 0;
	for (auto it = mat.begin(); it != mat.end(); it++) {
		for (auto it2 = it->begin(); it2 != it->end(); it2++) {
			sum += *it2;
		}
	}
	cout << "("<< argv[1] << ", " << sum/mat.size() << ")\n";
	return 0;
}
