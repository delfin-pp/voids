#!/bin/bash

# Generate 3D spheres
CONTRAST=( 80    40   26_6    20     16         15  13_3         12     10     8   6_6     5     4     2   1_5)
OUTER=(.00400 .0040	.00400 .0040 .00400 .004000000 .0040 .004000000  .0040 .0040 .0040 .0040 .0040 .0040 .0040)
INNER=(.00005 .0001 .00015 .0002 .00025 .000266666 .0003 .000333333  .0004 .0005 .0006 .0008 .0010 .0020 .0027)
WALLM0=(0.07 0.06 0.07 0.078 0.078 0.072 0.082 0.082 0.08 0.085 0.085 0.095 0.1 0.2 0.18)
WALLM1=(0.288 0.288 0.29 0.29 0.275 0.285 0.315 0.288 0.29 0.285 0.29 0.37 0.37 0.42 0.43)
WALLM2=(0.07 0.06 0.07 0.078 0.078 0.072 0.082 0.082 0.08 0.085 0.085 0.095 0.1 0.135 0.155)
WALLM3=(0.08 0.06 0.1 0.09 0.1 0.098 0.115 0.1 0.11 0.11 0.097 0.12 0.16 0.12 0.155)


rm -f spheres_r.dat
rm -f spheres_e.dat
touch spheres_r.dat
touch spheres_e.dat

# Generate all points
#for i in ${!CONTRAST[*]}
#do
#	FILEROOT=20spheres_g_c${CONTRAST[i]}
	
	# Generate points and triangulations
#	./generator/spheres/3dSpheresGenerator -f -g -n 20 -s 20.0 -x 20.0 -m 100.0 -d ${OUTER[i]} -v ${INNER[i]} -o $FILEROOT.dat
#	qdelaunay TI $FILEROOT.dat Fv Qt TO ${FILEROOT}_vertex.dat
#	qdelaunay TI $FILEROOT.dat Fn Qt TO ${FILEROOT}_neighbours.dat
	
	# Move to data folder
#	mv $FILEROOT.dat ../data/spheres/
#	mv ${FILEROOT}_vertex.dat ../data/spheres/
#	mv ${FILEROOT}_neighbours.dat ../data/spheres/
	
#done

# Generate values for all methods
echo "m0" >> spheres_r.dat
echo "m0" >> spheres_e.dat

for i in ${!CONTRAST[*]}
do
	FILEROOT=20spheres_g_c${CONTRAST[i]}
	# Run algorithm
	../build/src/delfin ../data/spheres/${FILEROOT} -m0 -w -t${WALLM0[i]} -v10000
	
	# Compare and save results
	./comparator/compare ${FILEROOT}.dat_voids.dat ../data/spheres/${FILEROOT}_output.off -f >${FILEROOT}_short.dat
	
	mv r.dat ${FILEROOT}_r.dat
	mv e.dat ${FILEROOT}_e.dat

    ./r_merger ${CONTRAST[i]} <${FILEROOT}_r.dat >>spheres_r.dat
	./e_merger ${CONTRAST[i]} <${FILEROOT}_e.dat >>spheres_e.dat
done


echo "m1" >> spheres_r.dat
echo "m1" >> spheres_e.dat

for i in ${!CONTRAST[*]}
do
	FILEROOT=20spheres_g_c${CONTRAST[i]}
	# Run algorithm
	../build/src/delfin ../data/spheres/${FILEROOT} -m1 -w -t${WALLM1[i]} -v10000
	
	# Compare and save results
	./comparator/compare ${FILEROOT}.dat_voids.dat ../data/spheres/${FILEROOT}_output.off -f >${FILEROOT}_short.dat
	
	mv r.dat ${FILEROOT}_r.dat
	mv e.dat ${FILEROOT}_e.dat

    ./r_merger ${CONTRAST[i]} <${FILEROOT}_r.dat >>spheres_r.dat
	./e_merger ${CONTRAST[i]} <${FILEROOT}_e.dat >>spheres_e.dat
done

echo "m2" >> spheres_r.dat
echo "m2" >> spheres_e.dat

for i in ${!CONTRAST[*]}
do
	FILEROOT=20spheres_g_c${CONTRAST[i]}
	# Run algorithm
	../build/src/delfin ../data/spheres/${FILEROOT} -m2 -w -t${WALLM2[i]} -v10000
	
	# Compare and save results
	./comparator/compare ${FILEROOT}.dat_voids.dat ../data/spheres/${FILEROOT}_output.off -f >${FILEROOT}_short.dat
	
	mv r.dat ${FILEROOT}_r.dat
	mv e.dat ${FILEROOT}_e.dat

    ./r_merger ${CONTRAST[i]} <${FILEROOT}_r.dat >>spheres_r.dat
	./e_merger ${CONTRAST[i]} <${FILEROOT}_e.dat >>spheres_e.dat
done

echo "m3" >> spheres_r.dat
echo "m3" >> spheres_e.dat

for i in ${!CONTRAST[*]}
do
	FILEROOT=20spheres_g_c${CONTRAST[i]}
	# Run algorithm
	../build/src/delfin ../data/spheres/${FILEROOT} -m3 -w -t${WALLM3[i]} -v10000
	
	# Compare and save results
	./comparator/compare ${FILEROOT}.dat_voids.dat ../data/spheres/${FILEROOT}_output.off -f >${FILEROOT}_short.dat
	
	mv r.dat ${FILEROOT}_r.dat
	mv e.dat ${FILEROOT}_e.dat

    ./r_merger ${CONTRAST[i]} <${FILEROOT}_r.dat >>spheres_r.dat
	./e_merger ${CONTRAST[i]} <${FILEROOT}_e.dat >>spheres_e.dat
done

echo "m4" >> spheres_r.dat
echo "m4" >> spheres_e.dat

for i in ${!CONTRAST[*]}
do
	FILEROOT=20spheres_g_c${CONTRAST[i]}
	# Run algorithm
	../build/src/delfin ../data/spheres/${FILEROOT} -m4 -v10000
	
	# Compare and save results
	./comparator/compare ${FILEROOT}.dat_voids.dat ../data/spheres/${FILEROOT}_output.off -f >${FILEROOT}_short.dat
	
	mv r.dat ${FILEROOT}_r.dat
	mv e.dat ${FILEROOT}_e.dat

    ./r_merger ${CONTRAST[i]} <${FILEROOT}_r.dat >>spheres_r.dat
	./e_merger ${CONTRAST[i]} <${FILEROOT}_e.dat >>spheres_e.dat
done

