#!/bin/bash

# contrast always equal to 10
N=( 1000 2000 4000 8000 16000 32000 64000 128000 256000 512000 1024000 2048000)

for i in ${!N[*]}
do
    FILENAME=g_points_n${N[i]}
    ../build/src/delfin ../data/scalability/$FILENAME -m0 -w -t0.185 -v10000 -c > ../data/scalability/time/v3_m0_${FILENAME}_a_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m1 -w -t0.38 -v10000 -c > ../data/scalability/time/v3_m1_${FILENAME}_a_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m2 -w -t0.14 -v10000 -c > ../data/scalability/time/v3_m2_${FILENAME}_a_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m3 -w -t0.12 -v10000 -c > ../data/scalability/time/v3_m3_${FILENAME}_a_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m4 -v15000 -c > ../data/scalability/time/m4_${FILENAME}_a_time.txt

done

for i in ${!N[*]}
do
    FILENAME=g_points_n${N[i]}
    ../build/src/delfin ../data/scalability/$FILENAME -m0 -w -t0.185 -v10000 -c > ../data/scalability/time/v3_m0_${FILENAME}_b_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m1 -w -t0.38 -v10000 -c > ../data/scalability/time/v3_m1_${FILENAME}_b_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m2 -w -t0.14 -v10000 -c > ../data/scalability/time/v3_m2_${FILENAME}_b_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m3 -w -t0.12 -v10000 -c > ../data/scalability/time/v3_m3_${FILENAME}_b_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m4 -v15000 -c > ../data/scalability/time/m4_${FILENAME}_b_time.txt

done

for i in ${!N[*]}
do
    FILENAME=g_points_n${N[i]}
    ../build/src/delfin ../data/scalability/$FILENAME -m0 -w -t0.185 -v10000 -c > ../data/scalability/time/v3_m0_${FILENAME}_c_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m1 -w -t0.38 -v10000 -c > ../data/scalability/time/v3_m1_${FILENAME}_c_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m2 -w -t0.14 -v10000 -c > ../data/scalability/time/v3_m2_${FILENAME}_c_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m3 -w -t0.12 -v10000 -c > ../data/scalability/time/v3_m3_${FILENAME}_c_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m4 -v15000 -c > ../data/scalability/time/m4_${FILENAME}_c_time.txt

done

for i in ${!N[*]}
do
    FILENAME=g_points_n${N[i]}
    ../build/src/delfin ../data/scalability/$FILENAME -m0 -w -t0.185 -v10000 -c > ../data/scalability/time/v3_m0_${FILENAME}_d_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m1 -w -t0.38 -v10000 -c > ../data/scalability/time/v3_m1_${FILENAME}_d_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m2 -w -t0.14 -v10000 -c > ../data/scalability/time/v3_m2_${FILENAME}_d_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m3 -w -t0.12 -v10000 -c > ../data/scalability/time/v3_m3_${FILENAME}_d_time.txt
    ../build/src/delfin ../data/scalability/$FILENAME -m4 -v15000 -c > ../data/scalability/time/m4_${FILENAME}_d_time.txt


done