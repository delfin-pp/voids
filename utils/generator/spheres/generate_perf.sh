#!/bin/bash

D=0.001016588
V=0.0001
P=1000

for ((x=0; x < 11; x++))
  do
    ./3dSpheresGenerator -n 20 -m 50 -d $D -v $V -s 6 -x 6 -o 20sphere3d${P}.dat
    D=$(echo "scale=9; $D*2" | bc)
    V=$(echo "scale=9; $V*2" | bc)
    P=$(($P*2))
    
  done
