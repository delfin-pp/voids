#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <random>
#include <chrono>
#include <cmath>

using namespace std;

#define RED "\033[0;31m"
#define NORM "\033[0m"
#define BOLD "\033[1m"
#define UNDR "\033[4m"
#define bold(x) BOLD << x << NORM
#define undr(x) UNDR << x << NORM

double dist2(double, double, double);
void usage(char *);

int n_points, e_points, i_points, n_voids = 1;
double de = 2.0, di = 0.0;
double min_size = 7.5, max_size = 100.0, dim = 200.0;
char *outfile = (char *)0;
bool foster_format = false;
bool gaussian_distribution = false;

int main(int argc, char *argv[]) {
	
	typedef chrono::high_resolution_clock myclock;
	myclock::time_point beginning = myclock::now();
	
	
	if (argc < 2) {
		usage(argv[0]);
		return 0;
	}
	
	for(int k = 1 ; k < argc ; ++k ){
		if(argv[k][0] == '-'){
			switch(argv[k][1]){
				case 'n':
					if (k+1 < argc) {
						n_voids = atoi(argv[k+1]);
					} else {
						cerr << RED << BOLD << "ERROR: Option -n requires an integer." << NORM << "\n";
						usage(argv[0]);
						return -1;
					}
					break;
				case 'd':
					if (k+1 < argc) {
						de = atof(argv[k+1]);
					} else {
						cerr << RED << BOLD << "ERROR: Option -d requires a double." << NORM << "\n";
						usage(argv[0]);
						return -2;
					}
					break;
				case 'v':
					if (k+1 < argc) {
						di = atof(argv[k+1]);
					} else {
						cerr << RED << BOLD << "ERROR: Option -v requires a double." << NORM << "\n";
						usage(argv[0]);
						return -3;
					}
					break;
				case 'm':
					if (k+1 < argc) {
						dim = atof(argv[k+1]);
					} else {
						cerr << RED << BOLD << "ERROR: Option -m requires a double." << NORM << "\n";
						usage(argv[0]);
						return -4;
					}
					break;
				case 's':
					if (k+1 < argc) {
						min_size = atof(argv[k+1]);
					} else {
						cerr << RED << BOLD << "ERROR: Option -s requires a double." << NORM << "\n";
						usage(argv[0]);
						return -5;
					}
					break;
				case 'x':
					if (k+1 < argc) {
						max_size = atof(argv[k+1]);
					} else {
						cerr << RED << BOLD << "ERROR: Option -x requires a double." << NORM << "\n";
						usage(argv[0]);
						return -6;
					}
					break;
				case 'o':
					if (k+1 < argc) {
						outfile = argv[k+1];
					}
					break;
				case 'f':
					foster_format = true;
					break;
				case 'h':
					usage(argv[0]);
					return 0;
				case 'g':
					gaussian_distribution = true;
					break;
				default:
					cerr << RED << BOLD << "ERROR: Unknown parameter " << argv[k] << "." << NORM << "\n";
					usage(argv[0]);
					return -11;
			}
		}
	}
	
	default_random_engine generator;
	
	myclock::duration d = myclock::now() - beginning;
	unsigned seed2 = d.count();
	generator.seed(seed2);
	
	uniform_real_distribution<double> rand_size_dist(min_size, max_size);
	uniform_real_distribution<double> rand_coord_dist(-dim, dim);
	
	// Generate spheres
	int i, j, k;
	double void_vol = 0.0;
	vector< vector< double > > spheres(n_voids, vector< double > (4, 0.0));
	for (i = 0; i < n_voids; i++) {
		for (k = 0; k < 100; k++) {
			// Randomly generate radius between min_size and max_size
			double r = rand_size_dist(generator);
			
			// Generate center coordinates between -dim and dim
			double x, y, z;
			do {
				x = rand_coord_dist(generator);
			} while (x < -dim+r || x > dim-r);
			do {
				y = rand_coord_dist(generator);
			} while (y < -dim+r || y > dim-r);
			do {
				z = rand_coord_dist(generator);
			} while (z < -dim+r || z > dim-r);
			
			// Make sure it doesn't intersect with other voids
			for (j = 0; j < i; j++) {
				if (dist2(spheres[j][0]-x, spheres[j][1]-y, spheres[j][2]-z)
						< (spheres[j][3]+r) * (spheres[j][3]+r))
					break;
			}
			if (j == i) {
				// If successful, save
				spheres[i][0] = x;
				spheres[i][1] = y;
				spheres[i][2] = z;
				spheres[i][3] = r;
				void_vol += 4.0/3.0*M_PI*r*r*r;
				break;
			}
		}
		// If repeatedly unsuccessful, abort
		if (k == 100) {
			cerr << RED << BOLD << "ERROR: Could not generate voids." << NORM << "\n";
			return -20;
		}
	}
	e_points = (int) (de * (8 * dim * dim * dim - void_vol));
	i_points = (int) (di * void_vol);
	n_points = e_points + i_points;
	cerr << "Generating " << e_points << " background points\n";
	cerr << "Generating " << i_points << " inner points\n";

	
	// Generate outer points
	vector< vector< double > > points(n_points, vector< double > (3, 0.0));
	for (i = 0; i < e_points; i++) {
		while (true) {
			// Generate point coordinates between -dim and dim
			double x = rand_coord_dist(generator);
			double y = rand_coord_dist(generator);
			double z = rand_coord_dist(generator);
				
			// Check it doesn't fall inside a void
			for (j = 0; j < n_voids; j++) {
				if (dist2(spheres[j][0]-x, spheres[j][1]-y, spheres[j][2]-z)
						< spheres[j][3]*spheres[j][3])
					break;
			}
			if (j == n_voids) {
				// If successful, save
				points[i][0] = x;
				points[i][1] = y;
				points[i][2] = z;
				break;
			}
		}
	}
	// Generate inner points
	vector<vector<int>> points_in_void(n_voids);
	if(gaussian_distribution){
		double r = rand_size_dist(generator);
		normal_distribution<double> distribution(r, 3.0);
		for (int i = e_points; i < n_points; i++)
		{
			while (true)
			{
				// Generate point coordinates between -dim and dim
				double x = rand_coord_dist(generator);
				double y = rand_coord_dist(generator);
				double z = rand_coord_dist(generator);

				double d;

				while (true)
				{
					d = distribution(generator);
					if ((d >= 0.0) && (d < r))
						break;
				}

				// Check it actually does fall inside a void
				for (j = 0; j < n_voids; j++)
				{
					if (dist2(spheres[j][0] - x, spheres[j][1] - y, spheres[j][2] - z) < spheres[j][3] * spheres[j][3]){
						if (dist2(spheres[j][0] - x, spheres[j][1] - y, spheres[j][2] - z) >= d * d) {
							break;
						}
					}
				}
				if (j != n_voids)
				{
					// If successful, save
					points[i][0] = x;
					points[i][1] = y;
					points[i][2] = z;
					points_in_void[j].push_back(i);
					break;
				}
			}
		}
	}
	else {
		for (int i = e_points; i < n_points; i++) {
			while (true) {
				// Generate point coordinates between -dim and dim
				double x = rand_coord_dist(generator);
				double y = rand_coord_dist(generator);
				double z = rand_coord_dist(generator);
					
				// Check it actually does fall inside a void
				for (j = 0; j < n_voids; j++) {
					if (dist2(spheres[j][0]-x, spheres[j][1]-y, spheres[j][2]-z)
							< spheres[j][3]*spheres[j][3])
						break;
				}
				if (j != n_voids) {
					// If successful, save
					points[i][0] = x;
					points[i][1] = y;
					points[i][2] = z;
					points_in_void[j].push_back(i);
					break;
				}
			}
		}
	}
	
	// Output points
	ofstream voids;
	ofstream list;
	ofstream voids_points;
	ofstream voids_data;
	ostream *out = &cout;
	ofstream file;
	if (outfile == (char *)0) {
		voids.open("voids.dat");
		list.open("voids.list");
		voids_points.open("voids.ply");
	}
	else {
		char voidfile[256];
		char listfile[256];
		char voidpointfile[256];
		char voiddatafile[256];
		strcpy(voidfile, outfile);
		strcat(voidfile, "_voids.dat");
		strcpy(listfile, outfile);
		strcat(listfile, "_voids.list");
		strcpy(voidpointfile, outfile);
		strcat(voidpointfile, "_voids.ply");
		strcpy(voiddatafile, outfile);
		strcat(voiddatafile, "_voids.points");
		voids.open(voidfile);
		list.open(listfile);
		voids_points.open(voidpointfile);
		voids_data.open(voiddatafile);
		file.open(outfile);
		out = &file;
	}
	*out << "3\n";
	*out << n_points << "\n";
	for (i = 0; i < n_points; i++) {
		*out << points[i][0] << " " << points[i][1] << " " << points[i][2] << "\n";
	}
	list << "{ LIST\n";
	if (foster_format) {
		for (i = 0; i < n_voids; i++) {
			list << "\t{ SPHERE " << spheres[i][3] << " " << spheres[i][0] << " " << spheres[i][1] << " " << spheres[i][2] << " }\n";
			// Lines in foster's void format
			voids << "1\n";
			voids << spheres[i][0] << " " << spheres[i][1] << " " << spheres[i][2] << endl;
			voids << 0 << " " << 0 << " " << 0 << endl;
			voids << 0 << " " << 4.0/3.0*M_PI*spheres[i][3]*spheres[i][3]*spheres[i][3] << " " << spheres[i][3] << endl;     
			voids << 0 << endl;
			voids << "2\n";
			voids << spheres[i][0] << "       " << spheres[i][1] << "        " << spheres[i][2] << endl;
			voids << spheres[i][3] << endl;
		}
	} else {
		for (i = 0; i < n_voids; i++) {
			list << "\t{ SPHERE " << spheres[i][3] << " " << spheres[i][0] << " " << spheres[i][1] << " " << spheres[i][2] << " }\n";
			// Lines in the form x y z r
			voids << spheres[i][0] << " " << spheres[i][1] << " " << spheres[i][2] << " " << spheres[i][3] << "\n";
		}
	}
	list << "}\n";

	voids_points << "ply\n";
	voids_points << "format ascii 1.0\n";
	voids_points << "element vertex " << i_points << "\n";
	voids_points << "property float x\n";
	voids_points << "property float y\n";
	voids_points << "property float z\n";
	voids_points << "property uchar red\n";
	voids_points << "property uchar green\n";
	voids_points << "property uchar blue\n";
	voids_points << "end_header\n";

	voids_data << n_voids << "\n";
	for (int i = 0; i < n_voids; i++)
	{
		voids_data << i << " " << points_in_void[i].size() << "\n";
	}

	for (int i = 0; i < n_voids; i++)
	{
		int r = rand() & 255;
		int g = rand() & 255;
		int b = rand() & 255;
		int n_piv = points_in_void[i].size();
		for(int j=0; j<n_piv; j++){
			voids_points << points[points_in_void[i][j]][0] << " " << points[points_in_void[i][j]][1] << " " << points[points_in_void[i][j]][2] << " " << r << " " << g << " " << b << "\n";
			voids_data << i << " " << points[points_in_void[i][j]][0] << " " << points[points_in_void[i][j]][1] << " " << points[points_in_void[i][j]][2] << "\n";
		}
	}

	return 0;
}

double dist2(double dx, double dy, double dz) {
	return dx*dx+dy*dy+dz*dz;
}

void usage(char *fname) {
	cout << "Usage: " << bold(fname) << " "
		<< "[" << bold("-n") << " " << undr("nSphrs") << "]"
		<< "[" << bold("-d") << " " << undr("sDens") << "]"
		<< "[" << bold("-v") << " " << undr("vDens") << "]"
		<< "[" << bold("-m") << " " << undr("sSize") << "]"
		<< "[" << bold("-s") << " " << undr("minRad") << "]"
		<< "[" << bold("-x") << " " << undr("maxRad") << "]"
		<< "[" << bold("-o") << " " << undr("file") << "]"
		<< "[" << bold("-h") << "]\n";
	cout << "  " << bold("-n") << " " << undr("nSphrs") << " Sets the number of spheres generated (default " << n_voids << ").\n";
	cout << "  " << bold("-d") << " " << undr("sDens") << "  Sets global point density (default " << de << ").\n";
	cout << "  " << bold("-v") << " " << undr("vDens") << "  Sets void point density (default " << di << ").\n";
	cout << "  " << bold("-m") << " " << undr("sSize") << "  Sets sample volume (default " << dim << "). Coordinates will be in range [-" << undr("sSize") << "," << undr("sSize") << ").\n";
	cout << "  " << bold("-s") << " " << undr("minRad") << " Minimum sphere radius (default " << min_size << ").\n";
	cout << "  " << bold("-x") << " " << undr("maxRad") << " Maximum sphere radius (default " << max_size << ").\n";
	cout << "  " << bold("-o") << " " << undr("file") << "   Output file (outputs to " << bold("stdout") << " by default).\n";
	cout << "  " << bold("-h") << "        Show this help information and exit.\n";
}
