#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <random>
#include <chrono>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <functional>

#define SQRT3 1.7320508075688772

/* CLI definitions */

#define RED "\033[0;31m"
#define NORM "\033[0m"
#define BOLD "\033[1m"
#define UNDR "\033[4m"
#define bold(x) BOLD << x << NORM
#define undr(x) UNDR << x << NORM

template <typename T>
std::vector<T> operator-(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());

    std::vector<T> result;
    result.reserve(a.size());

    std::transform(a.begin(), a.end(), b.begin(), 
                   std::back_inserter(result), std::minus<T>());
    return result;
}

using namespace std;

/**
Euclidean distance between two points.

Calculates euclidean distance between the 3D point (x, y, z) and the origin.

@param  x the x coordinate of the point
@param  y the y coordinate of the point
@param  z the z coordinate of the point
@return the distance between (x, y, z) and the origin (0, 0, 0).
*/
double dist2(double, double, double);

vector<double> cross(vector<double> u, vector<double> v);

double dotpr(vector<double> u, vector<double> v);

void usage(char *);

int n_points, e_points, i_points, n_voids = 1;
double de = 2.0, di = 0.0;
double min_size = 7.5, max_size = 100.0, dim = 200.0;
char *outfile = (char *)0;

int main(int argc, char *argv[]) {
	
	typedef chrono::high_resolution_clock myclock;
	myclock::time_point beginning = myclock::now();
	
	
	if (argc < 2) {
		usage(argv[0]);
		return 0;
	}
	
	for(int k = 1 ; k < argc ; ++k ){
		if(argv[k][0] == '-'){
			switch(argv[k][1]){
				case 'n':
					if (k+1 < argc) {
						n_voids = atoi(argv[k+1]);
					} else {
						cerr << RED << BOLD << "ERROR: Option -n requires an integer." << NORM << "\n";
						usage(argv[0]);
						return -1;
					}
					break;
				case 'd':
					if (k+1 < argc) {
						de = atof(argv[k+1]);
					} else {
						cerr << RED << BOLD << "ERROR: Option -d requires a double." << NORM << "\n";
						usage(argv[0]);
						return -2;
					}
					break;
				case 'v':
					if (k+1 < argc) {
						di = atof(argv[k+1]);
					} else {
						cerr << RED << BOLD << "ERROR: Option -v requires a double." << NORM << "\n";
						usage(argv[0]);
						return -3;
					}
					break;
				case 'm':
					if (k+1 < argc) {
						dim = atof(argv[k+1]);
					} else {
						cerr << RED << BOLD << "ERROR: Option -m requires a double." << NORM << "\n";
						usage(argv[0]);
						return -4;
					}
					break;
				case 's':
					if (k+1 < argc) {
						min_size = atof(argv[k+1]);
					} else {
						cerr << RED << BOLD << "ERROR: Option -s requires a double." << NORM << "\n";
						usage(argv[0]);
						return -5;
					}
					break;
				case 'x':
					if (k+1 < argc) {
						max_size = atof(argv[k+1]);
					} else {
						cerr << RED << BOLD << "ERROR: Option -x requires a double." << NORM << "\n";
						usage(argv[0]);
						return -6;
					}
					break;
				case 'o':
					if (k+1 < argc) {
						outfile = argv[k+1];
					}
					break;
				case 'h':
					usage(argv[0]);
					return 0;
				default:
					cerr << RED << BOLD << "ERROR: Unknown parameter " << argv[k] << "." << NORM << "\n";
					usage(argv[0]);
					return -11;
			}
		}
	}
	
	int fac[6][3] = {{0,2,1},{0,4,2},{0,1,4},{7,6,5},{7,5,3},{7,3,6}};
	
	default_random_engine generator;
	
	myclock::duration d = myclock::now() - beginning;
	unsigned seed2 = d.count();
	generator.seed(seed2);
	
	uniform_real_distribution<double> rand_size_dist(min_size, max_size);
	uniform_real_distribution<double> rand_ang_dist(0, 1);
	uniform_real_distribution<double> rand_coord_dist(-dim, dim);
	
	// Generate cubes
	int i, j, k;
	double void_vol = 0.0;
	vector< vector< vector<double> > > cubes(n_voids, vector< vector<double> >(8, vector<double>(3)));
	for (i = 0; i < n_voids; i++) {
		for (k = 0; k < 100; k++) {
			// Randomly generate side length between min_size and max_size
			double a = rand_size_dist(generator);
			
			// Generate uniform cube rotation
			double theta, phi, psi;
			theta = 2.0 * M_PI * rand_ang_dist(generator);
			phi = acos(2.0 * rand_ang_dist(generator) - 1.0);
			psi = 2.0 * M_PI * rand_ang_dist(generator);
			
			// Rotate ψ about ẑ, φ about ŷ, and θ about ẑ
			double t1 = cos(psi);
			double t2 = sin(psi);
			double t3 = cos(phi);
			double t4 = sin(phi);
			double t5 = cos(theta);
			double t6 = sin(theta);
			const double rot[3][3] = {
				{	t1*t3*t5-t2*t6,		-t2*t3*t5-t1*t6,	t4*t5},
				{	t1*t3*t6+t2*t5,		-t2*t3*t6+t1*t5,	t4*t6},
				{	-t1*t4,				t2*t4,				t3}
			};
			
			// Generate center coordinates between -dim and dim
			double p[3], o[3];
			for (int ax = 0; ax < 3; ax++) {
				do {
					p[ax] = rand_coord_dist(generator);
				} while (p[ax] < -dim+a/2.0*SQRT3 || p[ax] > dim-a/2.0*SQRT3);
			}
			
			// Calculate vertices
			for (int v = 0; v < 8; v++) {
				for (int ax = 0; ax < 3; ax++) {
					o[ax] = (v&(1<<ax)) == 0 ? -a/2.0 : a/2.0;
				}
				for (int ax = 0; ax < 3; ax++) {
					cubes[i][v][ax] = p[ax];
					for (int it = 0; it < 3; it++) {
						cubes[i][v][ax] += rot[ax][it] * o[it];
					}
				}
			}
			
			// Check it doesn't intersect other cubes (plane separation)
			bool inter;
			for (j = 0; j < i; j++) {
				inter = true;
				for (int f1 = 0; f1 < 6; f1++) {
					bool plane = true;
					for (int v2 = 0; v2 < 8; v2++) {
						if (dotpr(cross(cubes[i][fac[f1][1]]-cubes[i][fac[f1][0]],cubes[i][fac[f1][2]]-cubes[i][fac[f1][0]]), cubes[j][v2]-cubes[i][fac[f1][0]]) <= 0.0) {
							plane = false;
							break;
						}
					}
					if (plane) {
						inter = false;
						break;
					}
				}
				if (inter) {
					break;
				}
				for (int f2 = 0; f2 < 6; f2++) {
					bool plane = true;
					for (int v1 = 0; v1 < 8; v1++) {
						if (dotpr(cross(cubes[j][fac[f2][1]]-cubes[j][fac[f2][0]],cubes[j][fac[f2][2]]-cubes[j][fac[f2][0]]), cubes[i][v1]-cubes[j][fac[f2][0]]) <= 0.0) {
							plane = false;
							break;
						}
					}
					if (plane) {
						inter = false;
						break;
					}
				}
				if (inter) {
					break;
				}
			}
			if (!inter) {
				// If successful, break to next cube
				void_vol += a * a * a;
				break;
			}
		}
		// If repeatedly unsuccessful, abort
		if (k == 100) {
			cerr << RED << BOLD << "ERROR: Could not generate voids." << NORM << "\n";
			return -20;
		}
	}
	e_points = (int) (de * (8 * dim * dim * dim - void_vol));
	i_points = (int) (di * void_vol);
	n_points = e_points + i_points;
	
	cerr << n_voids << " cubes generated.\n";
	cerr << "Generating " << e_points << " outer points.\n";
	
	// Generate outer points
	vector< vector< double > > points(n_points, vector< double > (3, 0.0));
	for (i = 0; i < e_points; i++) {
		while (true) {
			// Generate point coordinates between -dim and dim
			vector<double> p(3);
			for (k = 0; k < 3; k++) {
				p[k] = rand_coord_dist(generator);
			}
			
			// Check it doesn't fall inside a void
			for (j = 0; j < n_voids; j++) {
				// Cross-dot product to check inclusion 
				// NOTE faces are parallel so only 3 cross products (+6 dot products) are necessary
				bool contained = true;
				for (k = 0; k < 6; k++) {
					if (dotpr(cross(cubes[j][fac[k][1]]-cubes[j][fac[k][0]],cubes[j][fac[k][2]]-cubes[j][fac[k][0]]), p-cubes[j][fac[k][0]]) >= 0.0) {
						contained = false;
						break;
					}
				}
				if (contained) {
					break;
				}
			}
			// If successful, save
			if (j == n_voids) {
				for (k = 0; k < 3; k++) {
					points[i][k] = p[k];
				}
				break;
			}
		}
	}
	cerr << "Generating " << i_points << " inner points.\n";
	// Generate inner points
	for (int i = e_points; i < n_points; i++) {
		while (true) {
			// Generate point coordinates between -dim and dim
			vector<double> p(3);
			for (k = 0; k < 3; k++) {
				p[k] = rand_coord_dist(generator);
			}
			
			// Check it actually does fall inside a void
			for (j = 0; j < n_voids; j++) {
				// Cross-dot product to check inclusion 
				// NOTE faces are parallel so only 3 cross products (+6 dot products) are necessary
				bool contained = true;
				for (k = 0; k < 6; k++) {
					if (dotpr(cross(cubes[j][fac[k][1]]-cubes[j][fac[k][0]],cubes[j][fac[k][2]]-cubes[j][fac[k][0]]), p-cubes[j][fac[k][0]]) >= 0.0) {
						contained = false;
						break;
					}
				}
				if (contained)
					break;
			}
			// If successful, save
			if (j != n_voids) {
				for (k = 0; k < 3; k++) {
					points[i][k] = p[k];
				}
				break;
			}
		}
	}
	
	// Output points
	ofstream voids;
	ostream *out = &cout;
	ofstream off;
	ofstream vect;
	ofstream file;
	if (outfile == (char *)0) {
		off.open("voids.off");
		vect.open("points.vect");
		voids.open("voids.dat");
	}
	else {
		char voidfile[256];
		char offfile[256];
		char vectfile[256];
		strcpy(voidfile, outfile);
		strcat(voidfile, "_voids.dat");
		strcpy(offfile, outfile);
		strcat(offfile, "_voids.off");
		strcpy(vectfile, outfile);
		strcat(vectfile, "_points.vect");
		voids.open(voidfile);
		off.open(offfile);
		vect.open(vectfile);
		file.open(outfile);
		out = &file;
	}
	vect << "VECT\n";
	vect << n_points << " " << n_points << " " << 1 << "\n";
	
	for (i = 0; i < n_points; i++) {
		vect << "1 ";
	}
	vect << "\n";
	vect << "1";
	for (i = 1; i < n_points; i++) {
		vect << " 0";
	}
	vect << "\n";
	*out << "3\n";
	*out << n_points << "\n";
	for (i = 0; i < n_points; i++) {
		*out << points[i][0] << " " << points[i][1] << " " << points[i][2] << "\n";
		vect << points[i][0] << " " << points[i][1] << " " << points[i][2] << "\n";
	}
	vect << "1 1 1 1\n";
	
	off << "OFF\n";
	off << (n_voids * 8) + n_points << " " << n_voids * 6 << " 0\n";
	voids << n_voids << "\n";
	for (i = 0; i < n_voids; i++) {
		// Output cubes in a rereadable format
		for (j = 0; j < 8; j++) {
			voids << cubes[i][j][0] << " " << cubes[i][j][1] << " " << cubes[i][j][2] << "\n";
			off << cubes[i][j][0] << " " << cubes[i][j][1] << " " << cubes[i][j][2] << "\n";
		}
	}
	for (i = 0; i < n_points; i++) {
		off << points[i][0] << " " << points[i][1] << " " << points[i][2] << "\n";
	}
	for (i = 0; i < n_voids; i++) {
		off << "4 " << 8*i << " " << 8*i+2 << " " << 8*i+3 << " " << 8*i+1 << "\n";
		off << "4 " << 8*i << " " << 8*i+1 << " " << 8*i+5 << " " << 8*i+4 << "\n";
		off << "4 " << 8*i << " " << 8*i+4 << " " << 8*i+6 << " " << 8*i+2 << "\n";
		off << "4 " << 8*i+7 << " " << 8*i+6 << " " << 8*i+4 << " " << 8*i+5 << "\n";
		off << "4 " << 8*i+7 << " " << 8*i+3 << " " << 8*i+2 << " " << 8*i+6 << "\n";
		off << "4 " << 8*i+7 << " " << 8*i+5 << " " << 8*i+1 << " " << 8*i+3 << "\n";
	}
	return 0;
}

double dist2(double dx, double dy, double dz) {
	return dx*dx+dy*dy+dz*dz;
}

vector<double> cross(vector<double> u, vector<double> v) {
	vector<double> cp(3);
	for (int i = 0; i < 3; i++) {
		cp[i] = u[(i+1)%3] * v[(i+2)%3] - u[(i+2)%3] * v[(i+1)%3];
	}
	return cp;
}

double dotpr(vector<double> u, vector<double> v) {
	return u[0]*v[0]+u[1]*v[1]+u[2]*v[2];
}

void usage(char *fname) {
	cout << "Usage: " << bold(fname) << " "
		<< "[" << bold("-n") << " " << undr("nCubes") << "]"
		<< "[" << bold("-d") << " " << undr("sDens") << "]"
		<< "[" << bold("-v") << " " << undr("vDens") << "]"
		<< "[" << bold("-m") << " " << undr("sSize") << "]"
		<< "[" << bold("-s") << " " << undr("minEdge") << "]"
		<< "[" << bold("-x") << " " << undr("maxEdge") << "]"
		<< "[" << bold("-o") << " " << undr("file") << "]"
		<< "[" << bold("-h") << "]\n";
	cout << "  " << bold("-n") << " " << undr("nCubes") << "  Sets the number of cubes generated (default " << n_voids << ").\n";
	cout << "  " << bold("-d") << " " << undr("sDens") << "   Sets global point density (default " << de << ").\n";
	cout << "  " << bold("-v") << " " << undr("vDens") << "   Sets void point density (default " << di << ").\n";
	cout << "  " << bold("-m") << " " << undr("sSize") << "   Sets sample volume (default " << dim << "). Coordinates will be in range [-" << undr("sSize") << "," << undr("sSize") << ").\n";
	cout << "  " << bold("-s") << " " << undr("minEdge") << " Minimum cube edge length (default " << min_size << ").\n";
	cout << "  " << bold("-x") << " " << undr("maxEdge") << " Maximum cube edge length (default " << max_size << ").\n";
	cout << "  " << bold("-o") << " " << undr("file") << "    Output file (outputs to " << bold("stdout") << " by default).\n";
	cout << "  " << bold("-h") << "         Show this help information and exit.\n";
}
