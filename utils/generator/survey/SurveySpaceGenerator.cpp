#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <random>
#include <iostream>
#include <fstream>
#include <vector>
#include <gsl/gsl_integration.h>

#define RED "\033[0;31m"
#define NORM "\033[0m"
#define BOLD "\033[1m"
#define UNDR "\033[4m"
#define bold(x) BOLD << x << NORM
#define undr(x) UNDR << x << NORM

#define LIGHTSPEED 299792.458 //  in km/s
#define PI 3.14159265

double dist2(double, double, double);
void usage(char* fname);
void make_cosmological_redshift(double OM, double OL, double z0, double z1, int N);
double comoving_distance(double z, double H0, double Om, gsl_integration_workspace *);
double* comoving_distance(double* z, double H0, double Om, int n, gsl_integration_workspace *);
double* convert_coordinates(double ra, double dec, double cd);
double** convert_coordinates(double* ra, double* dec, double* cd, int n);
void write_particles(double* x, double* y, double* z, int n);
void draw_voids(double* x, double* y, double* z, double* radius, int n);
// this defines the expansion function that we will integrate
// Laveaux & Wandelt (2012) Eq. 24
struct expan_params { double Om; double Ol; double w0; double wa;};
double expanFun (double z, void * p) {
  struct expan_params * params = (struct expan_params *)p;
  double Om = (params->Om);
  double Ol = (params->Ol);
  double w0 = (params->w0);
  double wa = (params->wa);

  //const double h0 = 1.0;
  const double h0 = 0.71;
  double ez;

  double wz = w0 + wa*z/(1+z);

  ez = Om*pow(1+z,3) + (1.-Om);
  //ez = Om*pow(1+z,3) + pow(h0,2) * (1.-Om)*pow(1+z,3+3*wz);

  ez = sqrt(ez);
  //ez = sqrt(ez)/h0;

  ez = 1./ez;

 //return 1/sqrt(Om*pow(1+z,3) + Om);

  return  ez;
}

int n_points = 1000;
int n_voids = 10;
char *outfile = (char *) "output.dat";

double contrast = 20.;

double ra_min = 10.0, ra_max = 170.0;
double dec_min = 0.0, dec_max = 60.0;
double red_min = 0.3, red_max = 0.4;

double r_min = 15.0, r_max = 55.0;

double H0 = 71.0; /* Hubble constant */
double h = H0 / 100.;
double Om =  0.3; /* Omega matter */
double Ol =  0.6889; /* Omega lambda */

int main(int argc, char *argv[])
{

	if (argc < 2) {
		usage(argv[0]);
		return 0;
	}

    /* Read command line arguments */
    for(int k = 1 ; k < argc ; ++k ){
		if(argv[k][0] == '-'){
			switch(argv[k][1]){
				case 'h':
					usage(argv[0]);
					return 0;
				case 'n':
					if (k+1 < argc) {
						n_points = atoi(argv[k+1]);
					} else {
						std::cerr << RED << BOLD << "ERROR: Option -n requires an integer." << NORM << "\n";
						usage(argv[0]);
						return -1;
					}
					break;
				case 'H':
					if (k+1 < argc) {
						H0 = atof(argv[k+1]);
						h = H0 / 100.;
					} else {
						std::cerr << RED << BOLD << "ERROR: Option -H requires a double." << NORM << "\n";
						usage(argv[0]);
						return -2;
					}
					break;
				case 'm':
					if (k+1 < argc) {
						Om = atof(argv[k+1]);
					} else {
						std::cerr << RED << BOLD << "ERROR: Option -m requires a double." << NORM << "\n";
						usage(argv[0]);
						return -3;
					}
					break;
				case 'l':
					if (k+1 < argc) {
						Ol = atof(argv[k+1]);
					} else {
						std::cerr << RED << BOLD << "ERROR: Option -l requires a double." << NORM << "\n";
						usage(argv[0]);
						return -4;
					}
					break;
				case 'a':
					if (k+1 < argc) {
						ra_min = atof(argv[k+1]);
					} else {
						std::cerr << RED << BOLD << "ERROR: Option -a requires a double." << NORM << "\n";
						usage(argv[0]);
						return -5;
					}
					break;
				case 'A':
					if (k+1 < argc) {
						ra_max = atof(argv[k+1]);
					} else {
						std::cerr << RED << BOLD << "ERROR: Option -A requires a double." << NORM << "\n";
						usage(argv[0]);
						return -6;
					}
					break;
				case 'd':
					if (k+1 < argc) {
						dec_min = atof(argv[k+1]);
					} else {
						std::cerr << RED << BOLD << "ERROR: Option -d requires a double." << NORM << "\n";
						usage(argv[0]);
						return -7;
					}
					break;
				case 'D':
					if (k+1 < argc) {
						dec_max = atof(argv[k+1]);
					} else {
						std::cerr << RED << BOLD << "ERROR: Option -D requires a double." << NORM << "\n";
						usage(argv[0]);
						return -8;
					}
					break;
				case 'z':
					if (k+1 < argc) {
						red_min = atof(argv[k+1]);
					} else {
						std::cerr << RED << BOLD << "ERROR: Option -z requires a double." << NORM << "\n";
						usage(argv[0]);
						return -9;
					}
					break;
				case 'Z':
					if (k+1 < argc) {
						red_max = atof(argv[k+1]);
					} else {
						std::cerr << RED << BOLD << "ERROR: Option -Z requires a double." << NORM << "\n";
						usage(argv[0]);
						return -10;
					}
					break;
				case 'v':
					if (k+1 < argc) {
						n_voids = atoi(argv[k+1]);
					} else {
						std::cerr << RED << BOLD << "ERROR: Option -v requires an integer." << NORM << "\n";
						usage(argv[0]);
						return -11;
					}
					break;
				case 'r':
					if (k+1 < argc) {
						r_min = atof(argv[k+1]);
					} else {
						std::cerr << RED << BOLD << "ERROR: Option -r requires a double." << NORM << "\n";
						usage(argv[0]);
						return -12;
					}
					break;
				case 'R':
					if (k+1 < argc) {
						r_max = atof(argv[k+1]);
					} else {
						std::cerr << RED << BOLD << "ERROR: Option -R requires a double." << NORM << "\n";
						usage(argv[0]);
						return -13;
					}
					break;
				case 'c':
					if (k+1 < argc) {
						contrast = atof(argv[k+1]);
					} else {
						std::cerr << RED << BOLD << "ERROR: Option -c requires a double." << NORM << "\n";
						usage(argv[0]);
						return -14;
					}
					break;
				case 'o':
					if (k+1 < argc) {
						outfile = argv[k+1];
					}
					break;
				default:
					std::cerr << RED << BOLD << "ERROR: Unknown parameter " << argv[k] << "." << NORM << "\n";
					usage(argv[0]);
					return -16;
			}
		}
	}
 
    //Generate array of right ascension, declination and redshift
    std::default_random_engine gen;
    std::uniform_real_distribution<double> rand_ra(ra_min, ra_max);
    std::uniform_real_distribution<double> rand_dec(dec_min, dec_max);
    std::uniform_real_distribution<double> rand_red(red_min, red_max);
    std::uniform_real_distribution<double> rand_r(r_min, r_max);

	// Voids work
	double x_voids[n_voids];
	double y_voids[n_voids];
	double z_voids[n_voids];
	double radius[n_voids];
	
	gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);
	double aux_a, aux_d, aux_z, aux_cd;
	double* xyz;
	int i, j;

	for (i = 0; i < n_voids; i++)
	{
		radius[i] = rand_r(gen);

		while (true)
		{
			aux_a = rand_ra(gen);
			aux_d = rand_dec(gen);
			aux_z = rand_red(gen);
			aux_cd = comoving_distance(aux_z, H0, Om, w);
			xyz = convert_coordinates(aux_a, aux_d, aux_cd);
			
			for (j = 0; j < i; j++)
			{
				if (dist2(x_voids[j]-xyz[0], y_voids[j]-xyz[1], z_voids[j]-xyz[2])
							< (radius[j]+radius[i])*(radius[j]+radius[i]))
						break;
			}

			if (j == i)
			{
				x_voids[i] = xyz[0];
				y_voids[i] = xyz[1];
				z_voids[i] = xyz[2];
				break;
			}
		}
	}

	// Galaxies work
	int e_points, i_points;
    double x_points[n_points];
    double y_points[n_points];
    double z_points[n_points];

	i_points = int ( n_points / (contrast + 1) );
	e_points = n_points - i_points ;

	// Generate external points
    for (i = 0; i < e_points; i++)
    {
		while(true)
		{
			aux_a = rand_ra(gen);
			aux_d = rand_dec(gen);
			aux_z = rand_red(gen);
			aux_cd = comoving_distance(aux_z, H0, Om, w);
			xyz = convert_coordinates(aux_a, aux_d, aux_cd);	
			// Check it doesn't fall inside a void
			for (j = 0; j < n_voids; j++) 
			{
				if (dist2(x_voids[j]-xyz[0], y_voids[j]-xyz[1], z_voids[j]-xyz[2])
							< radius[j]*radius[j])
					break;
			}

			// If successful, save
			if (j == n_voids)
			{
				x_points[i] = xyz[0];
				y_points[i] = xyz[1];
				z_points[i] = xyz[2];
				break;
			}
		}
    } 

	// Generate internal points
	int idx_void;
	//double x_in, y_in, z_in;
	std::uniform_int_distribution<int> rand_idx(0, n_voids-1);
 	for (int i = e_points; i < n_points; i++)
    {
        idx_void = rand_idx(gen);
		std::uniform_real_distribution<double> rand_coord_dist(-radius[idx_void], radius[idx_void]);
		x_points[i] = x_voids[idx_void] + rand_coord_dist(gen);
		y_points[i] = y_voids[idx_void] + rand_coord_dist(gen);
		z_points[i] = z_voids[idx_void] + rand_coord_dist(gen);
    } 
	
	write_particles(x_points, y_points, z_points, n_points);
	draw_voids(x_voids, y_voids, z_voids, radius, n_voids);

    return 0;
}

double comoving_distance(double z, double H0, double Om, gsl_integration_workspace * w)
{
	double hubbleDistance = (LIGHTSPEED / H0); // in Mpc
	double result, error;
	struct expan_params params = {Om, 0.0, 0.0, 0.0};
	gsl_function F;
	F.function = &expanFun;
	F.params = &params;
	gsl_integration_qags (&F, 0, z, 0, 1e-3, 1000, w, &result, &error);
	return hubbleDistance * result;
}

double* comoving_distance(double* z, double H0, double Om, int n, gsl_integration_workspace * w)
{
	double hubbleDistance = (LIGHTSPEED / H0); // in Mpc
	double result, error;
	double* cd = new double[n];
	struct expan_params params = {Om, 0.0, 0.0, 0.0};
	gsl_function F;
	F.function = &expanFun;
	F.params = &params;
	for(int i=0; i<n; i++)
	{
		gsl_integration_qags (&F, 0, z[i], 0, 1e-3, 1000, w, &result, &error);
		cd[i] = hubbleDistance * result;
	}
	return cd;
}

double** convert_coordinates(double* ra, double* dec, double* cd, int n)
{
	double** cartesian;
	cartesian = new double*[n];
	for(int i=0; i<n; i++)
	{
		cartesian[i] = new double[3];
		cartesian[i][0] = cd[i] * cos(ra[i] * PI / 180) * cos (dec[i] * PI / 180);
		cartesian[i][1] = cd[i] * sin(ra[i] * PI / 180) * cos (dec[i] * PI / 180);
		cartesian[i][2] = cd[i] * sin(dec[i] * PI / 180);
	}
	return cartesian;
}

double* convert_coordinates(double ra, double dec, double cd)
{
	double* cartesian;
	cartesian = new double[3];
	cartesian[0] = cd * cos(ra * PI / 180) * cos (dec * PI / 180);
	cartesian[1] = cd * sin(ra * PI / 180) * cos (dec * PI / 180);
	cartesian[2] = cd * sin(dec * PI / 180);
	return cartesian;
}

void write_particles(double* x, double* y, double* z, int n)
{
	std::ofstream f(outfile);
	if(f.is_open())
	{
		//f << "x[Mpc] y[Mpc] z[Mpc]\n";
		f << "3\n";
		f << n << "\n";
		for (int i = 0; i < n_points; i++)
		{
			f << x[i] << " " << y[i] 
			<< " " << z[i] << "\n";
		}
		f.close();
	}
	else
	{
		std::cerr << RED << BOLD << "ERROR: Error opening output file." << NORM << "\n";
	}
	return;
}

void draw_voids(double* x, double* y, double* z, double* radius, int n)
{
	// Write spheres as icosahedrons
	std::ofstream f("voids.ply");
	if(f.is_open())
	{
		f << "ply\n";
		f << "format ascii 1.0\n";
		f << "element vertex " << n * 12 << " \n";
		f << "property float x\n" 
		  << "property float y\n"
		  << "property float z\n";
		f << "element face " << n * 20 << " \n";
		f << "property list uchar int vertex_index\n";
		f << "end_header\n";
		// Vertex
		double t = (1.0 + std::sqrt(5.0)) / 2.0;
		double r;
		for (int i = 0; i < n; i++)
		{
			// Using the radius as the midradius
			r = radius[i];
			f << r * -1.0 + x[i] << " " << r *     t + y[i] << " " << r *  0.0 + z[i] << "\n";
			f << r *  1.0 + x[i] << " " << r *     t + y[i] << " " << r *  0.0 + z[i] << "\n";
			f << r * -1.0 + x[i] << " " << r *    -t + y[i] << " " << r *  0.0 + z[i] << "\n";
			f << r *  1.0 + x[i] << " " << r *    -t + y[i] << " " << r *  0.0 + z[i] << "\n";
			f << r *  0.0 + x[i] << " " << r *  -1.0 + y[i] << " " << r *    t + z[i] << "\n";
			f << r *  0.0 + x[i] << " " << r *   1.0 + y[i] << " " << r *    t + z[i] << "\n";
			f << r *  0.0 + x[i] << " " << r *  -1.0 + y[i] << " " << r *   -t + z[i] << "\n";
			f << r *  0.0 + x[i] << " " << r *   1.0 + y[i] << " " << r *   -t + z[i] << "\n";
			f << r *    t + x[i] << " " << r *   0.0 + y[i] << " " << r * -1.0 + z[i] << "\n";
			f << r *    t + x[i] << " " << r *   0.0 + y[i] << " " << r *  1.0 + z[i] << "\n";
			f << r *   -t + x[i] << " " << r *   0.0 + y[i] << " " << r * -1.0 + z[i] << "\n";
			f << r *   -t + x[i] << " " << r *   0.0 + y[i] << " " << r *  1.0 + z[i] << "\n";
		}
		// Faces
		for (int i = 0; i < n * 12;)
		{
			f << 3 << " " << 0  + i << " " << 11 + i << " " << 5  + i << "\n";
			f << 3 << " " << 0  + i << " " << 5  + i << " " << 1  + i << "\n";
			f << 3 << " " << 0  + i << " " << 1  + i << " " << 7  + i << "\n";
			f << 3 << " " << 0  + i << " " << 7  + i << " " << 10 + i << "\n";
			f << 3 << " " << 0  + i << " " << 10 + i << " " << 11 + i << "\n";
			f << 3 << " " << 1  + i << " " << 5  + i << " " << 9  + i << "\n";
			f << 3 << " " << 5  + i << " " << 11 + i << " " << 4  + i << "\n";
			f << 3 << " " << 11 + i << " " << 10 + i << " " << 2  + i << "\n";
			f << 3 << " " << 10 + i << " " << 7  + i << " " << 6  + i << "\n";
			f << 3 << " " << 7  + i << " " << 1  + i << " " << 8  + i << "\n";
			f << 3 << " " << 3  + i << " " << 9  + i << " " << 4  + i << "\n";
			f << 3 << " " << 3  + i << " " << 4  + i << " " << 2  + i << "\n";
			f << 3 << " " << 3  + i << " " << 2  + i << " " << 6  + i << "\n";
			f << 3 << " " << 3  + i << " " << 6  + i << " " << 8  + i << "\n";
			f << 3 << " " << 3  + i << " " << 8  + i << " " << 9  + i << "\n";
			f << 3 << " " << 4  + i << " " << 9  + i << " " << 5  + i << "\n";
			f << 3 << " " << 2  + i << " " << 4  + i << " " << 11 + i << "\n";
			f << 3 << " " << 6  + i << " " << 2  + i << " " << 10 + i << "\n";
			f << 3 << " " << 8  + i << " " << 6  + i << " " << 7  + i << "\n";
			f << 3 << " " << 9  + i << " " << 8  + i << " " << 1  + i << "\n";
			i = i + 12;
		}
		f.close();
	}
	else
	{
		std::cerr << RED << BOLD << "ERROR: Error opening output file." << NORM << "\n";
	}
	return;
}

double dist2(double dx, double dy, double dz) {
	return dx*dx+dy*dy+dz*dz;
}

void usage(char* fname)
{
    std::cout << "Usage: " << fname << " "
		<< "[" << "-n" << " " << "nPoints" << "]"
		<< "[" << "-v" << " " << "nVoids" << "]"
		<< "[" << "-H" << " " << "H0" << "]"
		<< "[" << "-m" << " " << "Om" << "]"
		<< "[" << "-l" << " " << "Ol" << "]"
		<< "[" << "-a" << " " << "RAmin" << "]"
		<< "[" << "-A" << " " << "RAmax" << "]"
		<< "[" << "-d" << " " << "DECmin" << "]"	
		<< "[" << "-D" << " " << "DECmax" << "]"
		<< "[" << "-z" << " " << "zmin" << "]"
		<< "[" << "-Z" << " " << "zmax" << "]"
		<< "[" << "-r" << " " << "Rmin" << "]"
		<< "[" << "-R" << " " << "Rmax" << "]"
		<< "[" << "-c" << " " << "c" << "]"
		<< "[" << "-o" << " " << "file" << "]"
		<< "[" << "-h" <<  "]\n";
	std::cout << "  " << "-n" << " " << "nPoints" << "  Sets the number of particles generated (default " << n_points << ").\n";
	std::cout << "  " << "-v" << " " << "nVoids" << "   Sets the number of voids in the space (default " << n_voids << ").\n";
	std::cout << "  " << "-H" << " " << "H0" << "       Sets the Hubble constant (default " << H0 << ").\n";
	std::cout << "  " << "-m" << " " << "Om" << "       Sets the value for Omegga Matter (default " << Om << ").\n";
	std::cout << "  " << "-l" << " " << "Ol" << "       Sets the value for Omegga Lambda (default " << Ol << ").\n";
	std::cout << "  " << "-a" << " " << "RAmin" << "    Sets the value in degrees for the minimun Right Ascencion (default " << ra_min << ").\n";
	std::cout << "  " << "-A" << " " << "RAmax" << "    Sets the value in degrees for the maximum Right Ascencion (default " << ra_max << ").\n";
	std::cout << "  " << "-d" << " " << "DECmin" << "   Sets the value in degrees for the minimun Declination (default " << dec_min << ").\n";
	std::cout << "  " << "-D" << " " << "DECmax" << "   Sets the value in degrees for the maximum Declination (default " << dec_max << ").\n";
	std::cout << "  " << "-z" << " " << "zmin" << "     Sets the value for the minimum redshift (default " << red_min << ").\n";
	std::cout << "  " << "-Z" << " " << "zmax" << "     Sets the value for the maximum redshift (default " << red_max << ").\n";
	std::cout << "  " << "-r" << " " << "Rmin" << "     Sets the value for the minimum void radius (default " << r_min << ").\n";
	std::cout << "  " << "-R" << " " << "Rmax" << "     Sets the value for the maximum void radius (default " << r_max << ").\n";
	std::cout << "  " << "-c" << " " << "c" << "        Sets the value for the contrast between external and internal particles (default " << contrast << ").\n";
	std::cout << "  " << "-o" << " " << "file" << "     Output file (outputs to '" << outfile << "' by default).\n";
	std::cout << "  " << "-h" << "          Show this help information and exit.\n";
}