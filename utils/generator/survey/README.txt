Survey Space Generator
----------------------------------------------------
To compile, run:

g++ SurveySpaceGenerator.cpp -lgsl -lcblas -o survey

After compiling, run:

./survey -h

to get help with the program commands