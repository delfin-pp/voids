#ifndef __GEOM__H
#define __GEOM__H

#include <algorithm>
#include <vector>
#include <cassert>	// assert
#include <utility>
#include <functional>   // std::minus

#define SEED 0

using namespace std;

template <typename T>
vector<T> operator-(const vector<T>& a, const vector<T>& b)
{
    assert(a.size() == b.size());

    vector<T> result;
    result.reserve(a.size());

    transform(a.begin(), a.end(), b.begin(), back_inserter(result), minus<T>());
    return result;
}

const int cubes_fac[6][3] = {{0,2,1},{0,4,2},{0,1,4},{7,6,5},{7,5,3},{7,3,6}};
const int tetra_fac[4][3] = {{0,1,2},{2,3,0},{3,2,1},{1,0,3}};

double sqdist(vector<double> p1, vector<double> p2);

pair<double,double> montecarlo(vector<double> min1, vector<double> max1, vector<double> min2, vector<double> max2, ComparableVoid& fvoid, vector<vector<int> > hvoid, vector<vector<double> > pts, int npoints);

vector<double> cross(vector<double> u, vector<double> v);

double dotpr(vector<double> u, vector<double> v);

double halfp(vector<double> a, vector<double> b, vector<double> c, vector<double> p);

#endif