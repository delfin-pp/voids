#include <cstring>	// strtok, strcmp
#include <cassert>	// assert
#include <cfloat>	// constants
#include <cmath>	// sqrt, pi

#include <iostream>
#include <fstream>	// ifstream
#include <vector>	// vector
#include <queue>	// priority_queue
#include <random>	// default_random_engine, uniform_double_distribution

#define SEED 0

using namespace std;

class FosterVoid {
	vector<double> centroid;
	vector<vector<double> > spheres;
	double distance, redshift, ra, dec, volume, radius;
	int g_in;

public:
	double xl, xu, yl, yu, zl, zu;	// Bounding box
	FosterVoid(double _x, double _y, double _z, double _d, double _redshift,
			double _ra, double _dec, double _v, double _r, int _g_in);
	void addSphere(double x, double y, double z, double r);
	const vector<vector<double> > getSpheres();
	double getVolume();
};

vector<FosterVoid> fvoids;	// List of list of spheres (x,y,z,r)
vector<vector<vector<int> > > hvoids;	// List of list of tetrahedra (p0,p1,p2,p3)
vector<vector<double> > pts;	// List of points (x,y,z)

vector<vector<double> > r;
vector<vector<double> > e;

/**
 * Returns squared distance between two points, i.e., d².
 * */
double sqdist(vector<double> p1, vector<double> p2);

void montecarlo(double mx, double my, double mz, double Mx, double My, double Mz, int i, int j, int npoints);

vector<double> cross(vector<double> u, vector<double> v);

double dotpr(vector<double> u, vector<double> v);

double halfp(vector<double> a, vector<double> b, vector<double> c, vector<double> p);

class comparator {
public:
	bool operator() (const pair<int, int> &lhs, const pair<int, int> &rhs) {
		double ratio1 = r[lhs.first][lhs.second];
		double ratio2 = r[rhs.first][rhs.second];
		if (ratio2 == 0 && ratio1 == 0) {
			return true;
		}
		return ratio2 > ratio1;
	}
};

int fac[4][3] = {{0,1,2},{2,3,0},{3,2,1},{1,0,3}};

int main(int argc, char *argv[]){
	// Read input files
	ifstream f1;
	f1.open(argv[1]);
	
	// Get spheres from each void
	int n1;
	if (f1.is_open()) {
		int t;
		while (f1 >> t) {
			if (t == 1) {
				double cx, cy, cz, dist, redshift, ra, dec, v, r, nin;
				int ngin;
				f1 >> cx >> cy >> cz >> dist >> redshift >> ra >> dec >> v
						>> r >> nin;
						ngin = (int) nin;
				fvoids.push_back(FosterVoid(cx,cy,cz,dist,redshift,ra,dec,v,r,ngin));
			}
			else {	// t == 2
				double x, y, z, r;
				f1 >> x >> y >> z >> r;
				fvoids.back().addSphere(x, y, z, r);
			}
		}
		n1 = fvoids.size();
		
		f1.close();
	}
	
	ifstream f2;
	f2.open(argv[2]);
	
	// Get tetrahedra from each void
	int n2;
	if (f2.is_open()) {
		char line[80];
		char *tok;
		int np = -1, nf = -1;
		bool off = false;
		bool params = false;
		
		// Get header
		while (!params && !f2.eof()) {
			f2.getline(line, 80);
			
			tok = strtok(line, " \n\r\t");
			if (tok[0] == '#') {
				continue;
			}
			
			if (!off && strcmp("OFF", tok) == 0) {
				off = true;
				continue;
			}
			off = true;
			np = atoi(tok);
			tok = strtok(NULL, " \n\r\t");
			nf = atoi(tok);
			tok = strtok(NULL, " \n\r\t");
			break;
		}
		
		// Get points
		pts.resize(np, vector<double>(3,0.0));
		for (int npt = 0; npt < np && !f2.eof(); npt++) {
			while (!f2.eof()) {
				f2.getline(line, 80);
				tok = strtok(line, " \n\r\t");
				if (tok[0] != '#') break;
			}
			pts[npt][0] = atof(tok);
			
			tok = strtok(NULL, " \n\r\t");
			pts[npt][1] = atof(tok);
			
			tok = strtok(NULL, " \n\r\t");
			pts[npt][2] = atof(tok);
		}
		
		// Get tetrahedra
		hvoids.resize(1,vector<vector<int> >(0));
		
		int oc[3], nc[3];
		for (int nt = 0; nt*4 < nf && !f2.eof(); nt++) {
			vector<int> tetrahedron(4, -1);
			for (int i = 0; i < 4; i++) {
				while (!f2.eof()) {
					f2.getline(line, 80);
					tok = strtok(line, " \n\r\t");
					if (tok[0] != '#') break;
				}
				assert (atoi(tok) == 3);
				
				for (int j = 0; j < 3; j++) {
					tok = strtok(NULL, " \n\r\t");
					if (tetrahedron[fac[i][j]] == -1) {
						tetrahedron[fac[i][j]] = atoi(tok);
					} else {
						assert (tetrahedron[fac[i][j]] == atoi(tok));
					}
				}
				for (int j = 0; j < 3; j++) {
					tok = strtok(NULL, " \n\r\t");
					if (i == 0) {
						nc[j] = atoi(tok);
					} else {
						assert (nc[j] == atoi(tok));
					}
				}
			}
			
			// Put tetrahedron in the right void
			if (hvoids.back().size() != 0 && oc[0] != nc[0] && oc[1] != nc[1] && oc[2] != nc[2]) {
				hvoids.push_back(vector<vector<int> >(0));
			}
			hvoids.back().push_back(tetrahedron);
			for (int i = 0; i < 3; i++) {
				oc[i] = nc[i];
			}
		}
		
		n2 = hvoids.size();
		
		f2.close();
	}
	
	r.resize(n1,vector<double>(n2, 0.0));
	e.resize(n1,vector<double>(n2, 0.0));
	
	// Fill r and e
	for (int j = 0; j < n2; j++) {
		// Get bounding box for Hervias void
		double minx2, miny2, minz2, maxx2, maxy2, maxz2;
		minx2 = miny2 = minz2 = DBL_MAX;
		maxx2 = maxy2 = maxz2 = -DBL_MAX;
		for (auto it = hvoids[j].begin(); it != hvoids[j].end(); it++) {
			for (int k = 0; k < 4; k++) {
				if (minx2 > pts[(*it)[k]][0]) {
					minx2 = pts[(*it)[k]][0];
				}
				if (maxx2 < pts[(*it)[k]][0]) {
					maxx2 = pts[(*it)[k]][0];
				}
				if (miny2 > pts[(*it)[k]][1]) {
					miny2 = pts[(*it)[k]][1];
				}
				if (maxy2 < pts[(*it)[k]][1]) {
					maxy2 = pts[(*it)[k]][1];
				}
				if (minz2 > pts[(*it)[k]][2]) {
					minz2 = pts[(*it)[k]][2];
				}
				if (maxz2 < pts[(*it)[k]][2]) {
					maxz2 = pts[(*it)[k]][2];
				}
			}
		}
		for (int i = 0; i < n1; i++) {
			double minx1, miny1, minz1, maxx1, maxy1, maxz1;
			
			// Get bounding box for Foster void
			minx1 = fvoids[i].xl;
			maxx1 = fvoids[i].xu;
			miny1 = fvoids[i].yl;
			maxy1 = fvoids[i].yu;
			minz1 = fvoids[i].zl;
			maxz1 = fvoids[i].zu;
			
			// Check if bounding boxes intersect
			if (minx1 > maxx2 || minx2 > maxx1 || miny1 > maxy2
					|| miny2 > maxy1 || minz1 > maxz2 || minz2 > maxz1) {
				e[i][j] = 1.0;
				continue;
			}
			
			// Get final bounding box
			minx1 = min(minx1, minx2);
			miny1 = min(miny1, miny2);
			minz1 = min(minz1, minz2);
			maxx1 = max(maxx1, maxx2);
			maxy1 = max(maxy1, maxy2);
			maxz1 = max(maxz1, maxz2);
			
			montecarlo(minx1, miny1, minz1, maxx1, maxy1, maxz1, i, j, 500);
		}
	}
	
	// Dump full r/e table
	ofstream fr;
	ofstream fe;
	fr.open("r.dat");
	fe.open("e.dat");
	for (int i = 0; i < n1; i++) {
		for (int j = 0; j < n2; j++) {
			fr << r[i][j] << (j+1 == n2? "\n" : " ");
			fe << e[i][j] << (j+1 == n2? "\n" : " ");
		}
	}
	fr.close();
	fe.close();
	
	// Set a priority queue with all pairs
	priority_queue<pair<int,int>, vector<pair<int,int> >,comparator> priorized_pairs;
	for (int i = 0; i < n1; i++) {
		for (int j = 0; j < n2; j++) {
			priorized_pairs.push(pair<int,int>(i,j));
		}
	}
	
	// Get closest pairs and set as a pair
	vector<bool> freeA(n1, true);
	vector<bool> freeB(n2, true);
	while (!priorized_pairs.empty()) {
		if (freeA[priorized_pairs.top().first]
// 			&& freeB[priorized_pairs.top().second]
		) {
			int i1 = priorized_pairs.top().first;
			int i2 = priorized_pairs.top().second;
			double rec = r[i1][i2];
			double err = e[i1][i2];
			cout << i1 << "\t" << i2 << "\t" << rec << "\t"
				<< err << "\t" << fvoids[i1].getVolume() << "\t" << endl;
			freeA[priorized_pairs.top().first] = false;
			freeB[priorized_pairs.top().second] = false;
		}
		priorized_pairs.pop();
	}
	return 0;
}

void montecarlo(double mx, double my, double mz, double Mx, double My, double Mz, int i, int j, int npoints) {
	int v1 = 0, v2 = 0, inter = 0;
	
	default_random_engine gen;
	gen.seed(SEED);
	uniform_real_distribution<double> unifx(mx, Mx);
	uniform_real_distribution<double> unify(my, My);
	uniform_real_distribution<double> unifz(mz, Mz);
	
	for (int k = 0; k < npoints; k++) {
		// Generate a random point in bounding box
		vector<double> rand(3);
		rand[0] = unifx(gen);
		rand[1] = unify(gen);
		rand[2] = unifz(gen);
		
		// Check if it falls inside a F&N sphere
		bool foster = false;
		vector<vector<double> > spheres = fvoids[i].getSpheres();
		for (auto it = spheres.begin(); it != spheres.end(); it++) {
			if (sqdist(*it, rand) <= (*it)[3]*(*it)[3]) {
				foster = true;
				break;
			}
		}
		
		// Check if it falls inside a Hervias tetrahedron
		bool hervias = false;
		vector<vector<int> > tetrahedra = hvoids[j];
		for (auto it = tetrahedra.begin(); it != tetrahedra.end(); it++) {
			int i;
			for (i = 0; i < 4; i++) {
				if (halfp(pts[(*it)[fac[i][0]]],pts[(*it)[fac[i][1]]],pts[(*it)[fac[i][2]]], rand) > 0.0) {
					break;
				}
			}
			if (i < 4) {
				continue;
			}
			hervias = true;
			break;
		}
		
		if (foster && hervias) {
			inter++;
			v1++;
			v2++;
		}
		else {
			v1 += foster ? 1 : 0;
			v2 += hervias? 1 : 0;
		}
	}
	
	r[i][j] = 1.0 * inter / v1;
	e[i][j] = 1.0 - 1.0 * inter / v2;
}

double halfp(vector<double> a, vector<double> b, vector<double> c, vector<double> p) {
	vector<double> ab(3);
	vector<double> ac(3);
	vector<double> ap(3);
	for (int i = 0; i < 3; i++) {
		ab[i] = b[i] - a[i];
		ac[i] = c[i] - a[i];
		ap[i] = p[i] - a[i];
	}
	return dotpr(cross(ab, ac), ap);
}

vector<double> cross(vector<double> u, vector<double> v) {
	vector<double> cp(3);
	for (int i = 0; i < 3; i++) {
		cp[i] = u[(i+1)%3] * v[(i+2)%3] - u[(i+2)%3] * v[(i+1)%3];
	}
	return cp;
}

double dotpr(vector<double> u, vector<double> v) {
	return u[0]*v[0]+u[1]*v[1]+u[2]*v[2];
}

double sqdist(vector<double> p1, vector<double> p2) {
	return (p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1])+(p1[2]-p2[2])*(p1[2]-p2[2]);
}


FosterVoid::FosterVoid(double _x, double _y, double _z, double _d,
		double _redshift, double _ra, double _dec, double _v, double _r,
		int _g_in){
	centroid.resize(3, 0);
	centroid[0] = xl = xu = _x;
	centroid[1] = yl = yu = _y;
	centroid[2] = zl = zu = _z;
	distance = _d;
	redshift = _redshift;
	ra = _ra;
	dec = _dec;
	volume = _v;
	radius = _r;
	g_in = _g_in;
}

void FosterVoid::addSphere(double x, double y, double z, double r) {
	spheres.push_back(vector<double>(4));
	(spheres.back())[0] = x;
	(spheres.back())[1] = y;
	(spheres.back())[2] = z;
	(spheres.back())[3] = r;
	if (xl > x-r) {
		xl = x-r;
	}
	if (xu < x+r) {
		xu = x+r;
	}
	if (yl > y-r) {
		yl = y-r;
	}
	if (yu < y+r) {
		yu = y+r;
	}
	if (zl > z-r) {
		zl = z-r;
	}
	if (zu < z+r) {
		zu = z+r;
	}
}

const vector<vector<double> > FosterVoid::getSpheres() {
	return spheres;
}

double FosterVoid::getVolume() {
	return volume;
}

