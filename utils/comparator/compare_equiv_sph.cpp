#include <iostream>
#include <fstream>	// ifstream
#include <vector>	// vector
#include <queue>	// priority_queue
#include <cmath>	// sqrt and pi

using namespace std;

vector<vector<double> > a;
vector<vector<double> > b;

/**
 * Returns squared distance between two points, i.e., d².
 * */
double sqdist(vector<double> p1, vector<double> p2) {
	return (p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1])+(p1[2]-p2[2])*(p1[2]-p2[2]);
}

/**
 * Returns intersection/v2 rate between spheres of radii r1 and r2, whose
 * centers are at distance d.
 * */
double recov(double r1, double r2, double d) {
	if (r1+r2 <= d) {
		// No intersection
		return 0.0;
	}
	double v1 = 4.0/3.0*M_PI*r1*r1*r1;
	double v2 = 4.0/3.0*M_PI*r2*r2*r2;
	if (abs(r1-r2) > d) {
		// One is inside the other
		return r1 > r2 ? 1.0 : v1/v2;
	}
	double h1 = (d*d+r1*r1-r2*r2)/2.0/d;
	double h2 = (d*d+r2*r2-r1*r1)/2.0/d;
	double inter = M_PI*(2.0/3.0*r1*r1*r1-h1*(r1*r1-h1*h1/3.0) +
						 2.0/3.0*r2*r2*r2-h2*(r2*r2-h2*h2/3.0));
	return inter/v2;
}

/**
 * Returns 1-intersection/v1 rate between spheres of radii r1 and r2, whose
 * centers are at distance d.
 * */
double error(double r1, double r2, double d) {
	if (r1+r2 <= d) {
		// No intersection
		return 0.0;
	}
	double v1 = 4.0/3.0*M_PI*r1*r1*r1;
	double v2 = 4.0/3.0*M_PI*r2*r2*r2;
	if (abs(r1-r2) > d) {
		// One is inside the other
		return r1 > r2 ? 1.0 - v2/v1 : 0.0;
	}
	double h1 = (d*d+r1*r1-r2*r2)/2.0/d;
	double h2 = (d*d+r2*r2-r1*r1)/2.0/d;
	double inter = M_PI*(2.0/3.0*r1*r1*r1-h1*(r1*r1-h1*h1/3.0) +
						 2.0/3.0*r2*r2*r2-h2*(r2*r2-h2*h2/3.0));
	return 1.0 - inter/v1;
}

class comparator {
public:
	bool operator() (const pair<int, int> &lhs, const pair<int, int> &rhs) {
		double sqd1 = sqdist(a[lhs.first],b[lhs.second]);
		double sqd2 = sqdist(a[rhs.first],b[rhs.second]);
		return sqd1 > sqd2;
	}
};

class comparator2 {
public:
	bool operator() (const pair<int, int> &lhs, const pair<int, int> &rhs) {
		double d1 = sqrt(sqdist(a[lhs.first],b[lhs.second]));
		double d2 = sqrt(sqdist(a[rhs.first],b[rhs.second]));
		double ratio1 = recov(a[lhs.first][3],b[lhs.second][3],d1);
		double ratio2 = recov(a[rhs.first][3],b[rhs.second][3],d2);
		if (ratio2 == 0 && ratio1 == 0) {
			return d1 > d2;
		}
		return ratio2 > ratio1;
	}
};

int main(int argc, char *argv[]){
	// Read input files
	ifstream f1;
	f1.open(argv[1]);
	
	if (f1.is_open()) {
		int sz;
		f1 >> sz;
		a.resize(sz,vector<double>(4));
		for (int i = 0; i < sz; i++) {
			f1 >> a[i][0] >> a[i][1] >> a[i][2] >> a[i][3];
		}
		f1.close();
	}

	ifstream f2;
	f2.open(argv[2]);
	
	if (f2.is_open()) {
		int sz;
		f2 >> sz;
		b.resize(sz,vector<double>(4));
		for (int i = 0; i < sz; i++) {
			f2 >> b[i][0] >> b[i][1] >> b[i][2] >> b[i][3];
		}
		f2.close();
	}
	
	// Set a priority queue with all pairs
	priority_queue<pair<int,int>, vector<pair<int,int> >,comparator2> priorized_pairs;
	for (int i = 0; i < a.size(); i++) {
		for (int j = 0; j < b.size(); j++) {
			priorized_pairs.push(pair<int,int>(i,j));
		}
	}
	
	// Get closest pairs and set as a pair
	vector<bool> freeA(a.size(), true);
	vector<bool> freeB(b.size(), true);
	while (!priorized_pairs.empty()) {
		if (freeA[priorized_pairs.top().first]
// 			&& freeB[priorized_pairs.top().second]
		) {
			int i1 = priorized_pairs.top().first;
			int i2 = priorized_pairs.top().second;
			double dist = sqrt(sqdist(a[i1],b[i2]));
			double rec = recov(a[i1][3],b[i2][3],dist);
			double err = error(a[i1][3],b[i2][3],dist);
			cout << i1 << "\t" << i2 << "\t" << dist << "\t" << rec << "\t"
				<< err << "\t" << a[i1][3] << "\t" << b[i2][3] << endl;
			freeA[priorized_pairs.top().first] = false;
			freeB[priorized_pairs.top().second] = false;
		}
		priorized_pairs.pop();
	}
	return 0;
}
