#include <cstring>	// strtok, strcmp
#include <cfloat>	// constants
#include <cassert>	// assert
#include <cmath>	// sqrt, pi

#include <iostream>
#include <utility>
#include <fstream>	// ifstream
#include <vector>	// vector
#include <queue>	// priority_queue
#include "FosterVoid.hpp"

#define RAS0 (140.0 * M_PI / 180.0)
#define RAS1 (230.0 * M_PI / 180.0)
#define DEC0 (30.0 * M_PI / 180.0)
#define DEC1 (65.0 * M_PI / 180.0)

using namespace std;

int main(int argc, char *argv[]){
	vector<FosterVoid> fvoids;	// List of Foster voids
	
	// Read input file
	ifstream f1;
	f1.open(argv[1]);
	
	// Get spheres from each void
	int n1;
	cout << "Opening " << argv[1] << endl;
	if (f1.is_open()) {
		int t;
		bool found = false;
		while (f1 >> t) {
			if (t == 1) {
				double cx, cy, cz, dist, redshift, ra, dec, v, r, nin;
				int ngin;
				f1 >> cx >> cy >> cz >> dist >> redshift >> ra >> dec >> v
						>> r >> nin;
						ngin = (int) nin;
				fvoids.push_back(FosterVoid(cx,cy,cz,dist,redshift,ra,dec,v,r,ngin));
				found = false;
			}
			else {	// t == 2
				double x, y, z, r;
				f1 >> x >> y >> z >> r;
				fvoids.back().addSphere(x, y, z, r);
				if (!found) {
					if (r > x * sin(RAS1) - y * cos(RAS1) ||
						r > y * cos(RAS0) - x * sin(RAS0)) {
							cout << fvoids.size() - 1 << " out from right ascension range." << endl;
							found = true;
						}
					else if (
						r > sqrt(x*x+y*y) * sin(DEC1) - z * cos(DEC1) ||
						r > z * cos(DEC0) - sqrt(x*x+y*y) * sin(DEC0)) {
							cout << fvoids.size() - 1 << " out from declension range." << endl;
							found = true;
						}
				}
			}
		}
		
		n1 = fvoids.size();
		
		f1.close();
	}
	
	return 0;
} 
