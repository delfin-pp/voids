#include <vector>
#include "FosterVoid.hpp"
#include "geom.hpp"


FosterVoid::FosterVoid() {}

FosterVoid::FosterVoid(double _x, double _y, double _z, double _d,
		double _redshift, double _ra, double _dec, double _v, double _r,
		int _g_in){
	centroid.resize(3, 0);
	bbox.resize(6, 0);
	centroid[0] = bbox[0] = bbox[1] = _x;
	centroid[1] = bbox[2] = bbox[3] = _y;
	centroid[2] = bbox[4] = bbox[5] = _z;
	distance = _d;
	redshift = _redshift;
	ra = _ra;
	dec = _dec;
	volume = _v;
	radius = _r;
	g_in = _g_in;
}

void FosterVoid::addSphere(double x, double y, double z, double r) {
	spheres.push_back(vector<double>(4));
	(spheres.back())[0] = x;
	(spheres.back())[1] = y;
	(spheres.back())[2] = z;
	(spheres.back())[3] = r;
	for (int i = 0; i < 6; i++) {
		if (!(i&1) && bbox[i] > (spheres.back())[i/2]-r) {
			bbox[i] = (spheres.back())[i/2]-r;
		}
		else if ((i&1) && bbox[i] < (spheres.back())[i/2]+r) {
			bbox[i] = (spheres.back())[i/2]+r;
		}
	}
}

double FosterVoid::getVolume() {
	return volume;
}

bool FosterVoid::contains(vector<double> p) {
	for (auto it = spheres.begin(); it != spheres.end(); it++) {
		if (sqdist(*it, p) <= (*it)[3]*(*it)[3]) {
			return true;
		}
	}
	return false;
}

vector<double> FosterVoid::getBoundingBox() {
	return bbox;
}
