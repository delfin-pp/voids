#ifndef __COMPARABLEVOID__H
#define __COMPARABLEVOID__H

#include <vector>

using namespace std;

class ComparableVoid {
public:
	virtual bool contains(vector<double>) = 0;
	virtual vector<double> getBoundingBox() = 0;
	virtual double getVolume() = 0;
};

#endif