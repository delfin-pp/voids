#ifndef __CUBEVOID__H
#define __CUBEVOID__H

#include <vector>
#include "ComparableVoid.hpp"
#include <stdio.h> // ojo, agregado a la mala
#include <float.h> // ojo, agregado a la mala
#include <math.h> // ojo, agregado a la mala

using namespace std;

class CubeVoid : public ComparableVoid {
	vector<double> centroid;
	vector<vector<double> > vertices;
	vector<double> bbox;
	double a;

public:
	CubeVoid(vector<vector<double> >);
	double getVolume();
	bool contains(vector<double>);
	vector<double> getBoundingBox();
};

#endif