#include <random>		// default_random_engine, uniform_double_distribution
#include <algorithm>	// min, max
#include "ComparableVoid.hpp"
#include "geom.hpp"

using namespace std;

pair<double,double> montecarlo(vector<double> min1, vector<double> max1, vector<double> min2, vector<double> max2, ComparableVoid& fvoid, vector<vector<int> > hvoid, vector<vector<double> > pts, int npoints) {
	int v1 = 0, v2 = 0, inter = 0;
	
	// Get final bounding box
	vector<double> mint(3);
	vector<double> maxt(3);
	
	for (int i = 0; i < 3; i++) {
		mint[i] = min(min1[i], min2[i]);
		maxt[i] = max(max1[i], max2[i]);
	}
			
	default_random_engine gen;
	gen.seed(SEED);
	vector<uniform_real_distribution<double>> unif(3);
	for (int i = 0; i < 3; i++) {
		unif[i] = uniform_real_distribution<double> (mint[i], maxt[i]);
	}
	
	for (int k = 0; k < npoints; k++) {
		// Generate a random point in bounding box
		vector<double> rand(3);
		for (int i = 0; i < 3; i++) {
			rand[i] = unif[i](gen);
		}
		
		// Check if it falls inside the bounding box
		bool foster = true;
		for (int i = 0; i < 3; i++) {
			if (rand[i] > max1[i] || rand[i] < min1[i]) {
				foster = false;
			}
		}
		// Check if it falls inside a F&N sphere
		if (foster) {
			foster = fvoid.contains(rand);
		}
		
		// Check if it falls inside the bounding box
		bool hervias = true;
		for (int i = 0; i < 3; i++) {
			if (rand[i] > max2[i] || rand[i] < min2[i]) {
				hervias = false;
			}
		}
		// Check if it falls inside a Hervias tetrahedron
		if (hervias) {
			hervias = false;
			for (auto it = hvoid.begin(); it != hvoid.end(); it++) {
				int i;
				for (i = 0; i < 4; i++) {
					if (halfp(pts[(*it)[tetra_fac[i][0]]],pts[(*it)[tetra_fac[i][1]]],pts[(*it)[tetra_fac[i][2]]], rand) > 0.0) {
						break;
					}
				}
				if (i < 4) {
					continue;
				}
				hervias = true;
				break;
			}
		}
		
		if (foster && hervias) {
			inter++;
			v1++;
			v2++;
		}
		else if (!foster && !hervias) {
			k--;
		}
		else {
			v1 += foster ? 1 : 0;
			v2 += hervias? 1 : 0;
		}
	}
	
	// Return (retrieval, error)
	return make_pair(1.0 * inter / v1, 1.0 - 1.0 * inter / v2);
}

double halfp(vector<double> a, vector<double> b, vector<double> c, vector<double> p) {
	return dotpr(cross(b-a, c-a), p-a);
}

vector<double> cross(vector<double> u, vector<double> v) {
	vector<double> cp(3);
	for (int i = 0; i < 3; i++) {
		cp[i] = u[(i+1)%3] * v[(i+2)%3] - u[(i+2)%3] * v[(i+1)%3];
	}
	return cp;
}

double dotpr(vector<double> u, vector<double> v) {
	return u[0]*v[0]+u[1]*v[1]+u[2]*v[2];
}

double sqdist(vector<double> p1, vector<double> p2) {
	return (p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1])+(p1[2]-p2[2])*(p1[2]-p2[2]);
}
