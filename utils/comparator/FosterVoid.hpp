#ifndef __FOSTERVOID__H
#define __FOSTERVOID__H

#include <vector>
#include "ComparableVoid.hpp"

using namespace std;

class FosterVoid : public ComparableVoid {
	vector<double> centroid;
	vector<vector<double> > spheres;
	double distance, redshift, ra, dec, volume, radius;
	vector<double> bbox;
	int g_in;

public:
	FosterVoid();
	FosterVoid(double _x, double _y, double _z, double _d, double _redshift,
			double _ra, double _dec, double _v, double _r, int _g_in);
	void addSphere(double x, double y, double z, double r);
	double getVolume();
	bool contains(vector<double>);
	vector<double> getBoundingBox();
};

#endif
