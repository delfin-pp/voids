#include <cstring>	// strtok, strcmp
#include <cfloat>	// constants
#include <cassert>	// assert
#include <cmath>	// sqrt, pi

#include <iostream>
#include <utility>
#include <fstream>	// ifstream
#include <vector>	// vector
#include <queue>	// priority_queue
#include "ComparableVoid.hpp"
#include "FosterVoid.hpp"
#include "CubeVoid.hpp"
#include "geom.hpp"

using namespace std;

vector<vector<double> > r;
vector<vector<double> > e;

class comparator {
public:
	bool operator() (const pair<int, int> &lhs, const pair<int, int> &rhs) {
		double ratio1 = r[lhs.first][lhs.second];
		double ratio2 = r[rhs.first][rhs.second];
		if (ratio2 == 0 && ratio1 == 0) {
			return true;
		}
		return ratio2 > ratio1;
	}
};

int main(int argc, char *argv[]){
	vector<FosterVoid> fvoids;	// List of Foster voids
	vector<CubeVoid> cvoids;	// List of Cube voids
	vector<vector<vector<int> > > hvoids;	// List of list of thedra. (p0,p1,p2,p3)
	vector<vector<double> > pts;	// List of points (x,y,z)
	
	// Read input files
	ifstream f1;
	f1.open(argv[1]);
	
	// Get spheres from each void
	int n1;
	if (strcmp(argv[3], "-f") == 0) {
		if (f1.is_open()) {
			int t;
			while (f1 >> t) {
				if (t == 1) {
					double cx, cy, cz, dist, redshift, ra, dec, v, r, nin;
					int ngin;
					f1 >> cx >> cy >> cz >> dist >> redshift >> ra >> dec >> v
							>> r >> nin;
							ngin = (int) nin;
					fvoids.push_back(FosterVoid(cx,cy,cz,dist,redshift,ra,dec,v,r,ngin));
				}
				else {	// t == 2
					double x, y, z, r;
					f1 >> x >> y >> z >> r;
					fvoids.back().addSphere(x, y, z, r);
				}
			}
			
			n1 = fvoids.size();
			
			f1.close();
		}
	}
	else if (strcmp(argv[3], "-c") == 0) {
		if (f1.is_open()) {
			f1 >> n1;
			for (int i = 0; i < n1; i++) {
				vector<vector<double> > cube;
				for (int j = 0; j < 8; j++) {
					vector<double> vertex(3);
					f1 >> vertex[0] >> vertex[1] >> vertex[2];
					cube.push_back(vertex);
				}
				cvoids.push_back(CubeVoid(cube));
			}
			
			f1.close();
		}
	}

	ifstream f2;
	f2.open(argv[2]);
	
	// Get tetrahedra from each void
	int n2;
	if (f2.is_open()) {
		char line[80];
		char *tok;
		int np = -1, nf = -1;
		bool off = false;
		bool params = false;
		
		// Get header
		while (!params && !f2.eof()) {
			f2.getline(line, 80);
			
			tok = strtok(line, " \n\r\t");
			if (tok[0] == '#') {
				continue;
			}
			
			if (!off && strcmp("OFF", tok) == 0) {
				off = true;
				continue;
			}
			off = true;
			np = atoi(tok);
			tok = strtok(NULL, " \n\r\t");
			nf = atoi(tok);
			tok = strtok(NULL, " \n\r\t");
			break;
		}
		
		// Get points
		pts.resize(np, vector<double>(3,0.0));
		for (int npt = 0; npt < np && !f2.eof(); npt++) {
			while (!f2.eof()) {
				f2.getline(line, 80);
				tok = strtok(line, " \n\r\t");
				if (tok[0] != '#') break;
			}
			pts[npt][0] = atof(tok);
			
			tok = strtok(NULL, " \n\r\t");
			pts[npt][1] = atof(tok);
			
			tok = strtok(NULL, " \n\r\t");
			pts[npt][2] = atof(tok);
		}
		
		cerr << "Got " << pts.size() << " points.\n";
		
		// Get tetrahedra
		hvoids.resize(1,vector<vector<int> >(0));
		
		int oc[3], nc[3];
		for (int nt = 0; nt*4 < nf && !f2.eof(); nt++) {
			vector<int> tetrahedron(4, -1);
			for (int i = 0; i < 4; i++) { 
				while (!f2.eof()) {
					f2.getline(line, 80);
					tok = strtok(line, " \n\r\t");
					if (tok[0] != '#') break;
				}
				assert (atoi(tok) == 3);
				
				for (int j = 0; j < 3; j++) {
					tok = strtok(NULL, " \n\r\t");
					if (tetrahedron[tetra_fac[i][j]] == -1) {
						tetrahedron[tetra_fac[i][j]] = atoi(tok);
					} else {
						assert (tetrahedron[tetra_fac[i][j]] == atoi(tok));
					}
				}
				for (int j = 0; j < 3; j++) {
					tok = strtok(NULL, " \n\r\t");
					if (i == 0) {
						nc[j] = atoi(tok);
					} else {
						assert (nc[j] == atoi(tok));
					}
				}
			}
			
			// Put tetrahedron in the right void
			//TODO size != 0 AND (OR(oc[i] != nc[i]))
			if (hvoids.back().size() != 0 && oc[0] != nc[0] && oc[1] != nc[1] && oc[2] != nc[2]) {
				hvoids.push_back(vector<vector<int> >(0));
			}
			hvoids.back().push_back(tetrahedron);
			for (int i = 0; i < 3; i++) {
				oc[i] = nc[i];
			}
		}
		
		n2 = hvoids.size();
		cerr << "Got " << n2 << " voids.\n";
		int printedv = n2;  
		cerr << "Void " << printedv << " has " << hvoids[printedv].size() << " tetrahedra (" << (hvoids[printedv].size()*4) << " lines).\n";

		for(int i = 0; i < hvoids[printedv].size(); i++) {
			cerr << hvoids[printedv][i][0] << ", " << hvoids[printedv][i][1] << ", " << hvoids[printedv][i][2] << ", " << hvoids[printedv][i][3] << "\n";
		}
		f2.close();
	}
	r.resize(n1,vector<double>(n2, 0.0));
	e.resize(n1,vector<double>(n2, 0.0));
	// Fill r and e
	for (int j = 0; j < n2; j++) {
		// Get bounding box for Hervias void
		vector<double> min2(3, DBL_MAX);
		vector<double> max2(3, -DBL_MAX);
		for (auto it = hvoids[j].begin(); it != hvoids[j].end(); it++) {
			for (int k = 0; k < 4; k++) {
				for (int cc = 0; cc < 3; cc++) {
					if (min2[cc] > pts[(*it)[k]][cc]) {
						min2[cc] = pts[(*it)[k]][cc];
					}
					if (max2[cc] < pts[(*it)[k]][cc]) {
						max2[cc] = pts[(*it)[k]][cc];
					}
				}
			}
		}
		
		for (int i = 0; i < n1; i++) {
			double minx1, miny1, minz1, maxx1, maxy1, maxz1;
			
			// Get bounding box for target void
			vector<double> min1(3);
			vector<double> max1(3);
			
			vector<double> bbox;
			if (cvoids.size() > 0) {
				bbox = cvoids[i].getBoundingBox();
			}
			else if (fvoids.size() > 0) {
				bbox = fvoids[i].getBoundingBox();
			}
			min1[0] = bbox[0];
			max1[0] = bbox[1];
			min1[1] = bbox[2];
			max1[1] = bbox[3];
			min1[2] = bbox[4];
			max1[2] = bbox[5];
			
			// Check if bounding boxes intersect
			if (min1[0] > max2[0] || min2[0] > max1[0] || min1[1] > max2[1]
					|| min2[1] > max1[1] || min1[2] > max2[2] || min2[2] > max1[2]) {
				e[i][j] = 1.0;
				continue;
			}
			
			pair<double,double> res;
			if (cvoids.size() > 0) {
				res = montecarlo(min1, max1, min2, max2, cvoids[i], hvoids[j], pts, 10);
			}
			else if (fvoids.size() > 0) {
				res = montecarlo(min1, max1, min2, max2, fvoids[i], hvoids[j], pts, 10);
			}
			r[i][j] = res.first;
			e[i][j] = res.second;
		}
	}
	// Dump full r/e table
	ofstream fr;
	ofstream fe;
	fr.open("r.dat");
	fe.open("e.dat");
	for (int i = 0; i < n1; i++) {
		for (int j = 0; j < n2; j++) {
			fr << r[i][j] << (j+1 == n2? "\n" : " ");
			fe << e[i][j] << (j+1 == n2? "\n" : " ");
		}
	}
	fr.close();
	fe.close();
	
	// Set a priority queue with all pairs
	priority_queue<pair<int,int>, vector<pair<int,int> >,comparator> priorized_pairs;
	for (int i = 0; i < n1; i++) {
		for (int j = 0; j < n2; j++) {
			priorized_pairs.push(pair<int,int>(i,j));
		}
	}
	
	// Get closest pairs and set as a pair
	vector<bool> freeA(n1, true);
	vector<bool> freeB(n2, true);
	while (!priorized_pairs.empty()) {
		if (freeA[priorized_pairs.top().first]
// 			&& freeB[priorized_pairs.top().second]
		) {
			int i1 = priorized_pairs.top().first;
			int i2 = priorized_pairs.top().second;
			double rec = r[i1][i2];
			double err = e[i1][i2];
			if (cvoids.size() > 0) {
				cout << i1 << "\t" << i2 << "\t" << rec << "\t"
					<< err << "\t" << cvoids[i1].getVolume() << "\t" << endl;
			} else {
				cout << i1 << "\t" << i2 << "\t" << rec << "\t"
					<< err << "\t" << fvoids[i1].getVolume() << "\t" << endl;
			}
			freeA[priorized_pairs.top().first] = false;
			freeB[priorized_pairs.top().second] = false;
		}
		priorized_pairs.pop();
	}
	return 0;
}
