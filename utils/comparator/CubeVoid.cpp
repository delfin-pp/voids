#include <vector>
#include <cfloat>
#include "CubeVoid.hpp"
#include "geom.hpp"

using namespace std;

CubeVoid::CubeVoid(vector<vector<double> > _vertices) {
	vertices = _vertices;
	bbox = vector<double>({FLT_MAX, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, -FLT_MAX});
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 6; j++) {
			if (!(j&1) && bbox[j] > vertices[i][j/2]) {
				bbox[j] = vertices[i][j/2];
			}
			else if ((j&1) && bbox[j] < vertices[i][j/2]) {
				bbox[j] = vertices[i][j/2];
			}
		}
	}
	a = sqrt(sqdist(vertices[0],vertices[1]));
}

double CubeVoid::getVolume() {
	return a*a*a;
}

bool CubeVoid::contains(vector<double> pt) {
	for (int i = 0; i < 3; i++) {
		vector<double> crossp = cross(vertices[cubes_fac[i][2]]-vertices[cubes_fac[i][0]],vertices[cubes_fac[i][1]]-vertices[cubes_fac[i][0]]);
		double lim = dotpr(crossp, vertices[7^cubes_fac[i][0]^cubes_fac[i][1]^cubes_fac[i][2]]-vertices[cubes_fac[i][0]]);
		double res = dotpr(crossp, pt-vertices[cubes_fac[i][0]]);
		if (res > lim || res < 0) {
			return false;
		}
	}
	return true;
}

vector<double> CubeVoid::getBoundingBox() {
	return bbox;
}
