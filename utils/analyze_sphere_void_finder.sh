#!/bin/bash

# Generate 3D spheres

CONTRAST=( 80    40   26_6    20     16         15  13_3         12     10     8   6_6     5     4     2   1_5)
OUTER=(.00400 .0040	.00400 .0040 .00400 .004000000 .0040 .004000000  .0040 .0040 .0040 .0040 .0040 .0040 .0040)
INNER=(.00005 .0001 .00015 .0002 .00025 .000266666 .0003 .000333333  .0004 .0005 .0006 .0008 .0010 .0020 .0027)
WALLM0=(0.07 0.06 0.07 0.078 0.078 0.072 0.082 0.082 0.11 0.14 0.15 0.16 0.18 0.25 0.25)
WALLM1=(0.295 0.288 0.29 0.29 0.29 0.29 0.315 0.3 0.33 0.33 0.35 0.37 0.4 0.47 0.47)
WALLM2=(0.07 0.06 0.07 0.078 0.078 0.072 0.082 0.082 0.085 0.095 0.15 0.15 0.16 0.25 0.25)
WALLM3=(0.12 0.1 0.13 0.09 0.16 0.18 0.14 0.1 0.11 0.11 0.15 0.16 0.18 0.25 0.25)

rm -f spheres_r.dat
rm -f spheres_e.dat
touch spheres_r.dat
touch spheres_e.dat

#for i in ${!CONTRAST[*]}
#do
#	FILEROOT=20spheres_c${CONTRAST[i]}
	
	# Generate points and triangulations
#	./generator/spheres/3dSpheresGenerator -f -n 20 -s 20.0 -x 20.0 -m 100.0 -d ${OUTER[i]} -v ${INNER[i]} -o $FILEROOT.dat
#	qdelaunay TI $FILEROOT.dat Fv Qt TO ${FILEROOT}_vertex.dat
#	qdelaunay TI $FILEROOT.dat Fn Qt TO ${FILEROOT}_neighbours.dat
	
	# Move to data folder
#	mv $FILEROOT.dat ../data/spheres/
#	mv ${FILEROOT}_vertex.dat ../data/spheres/
#	mv ${FILEROOT}_neighbours.dat ../data/spheres/	
	
#done


echo "m0" >> spheres_r.dat
echo "m0" >> spheres_e.dat

for i in ${!CONTRAST[*]}
do
	FILEROOT=20spheres_c${CONTRAST[i]}
	# Run algorithm
	../build/src/delfin ../data/spheres/${FILEROOT} -m0 -w -t${WALLM0[i]} -v10000
	
	# Compare and save results
	./comparator/compare ${FILEROOT}.dat_voids.dat ../data/spheres/${FILEROOT}_output.off -f >${FILEROOT}_short.dat
	
	mv r.dat ${FILEROOT}_r.dat
	mv e.dat ${FILEROOT}_e.dat

    ./r_merger ${CONTRAST[i]} <${FILEROOT}_r.dat >>spheres_r.dat
	./e_merger ${CONTRAST[i]} <${FILEROOT}_e.dat >>spheres_e.dat
done

echo "m1" >> spheres_r.dat
echo "m1" >> spheres_e.dat

for i in ${!CONTRAST[*]}
do
	FILEROOT=20spheres_c${CONTRAST[i]}
	# Run algorithm
	../build/src/delfin ../data/spheres/${FILEROOT} -m1 -w -t${WALLM1[i]} -v10000
	
	# Compare and save results
	./comparator/compare ${FILEROOT}.dat_voids.dat ../data/spheres/${FILEROOT}_output.off -f >${FILEROOT}_short.dat
	
	mv r.dat ${FILEROOT}_r.dat
	mv e.dat ${FILEROOT}_e.dat

    ./r_merger ${CONTRAST[i]} <${FILEROOT}_r.dat >>spheres_r.dat
	./e_merger ${CONTRAST[i]} <${FILEROOT}_e.dat >>spheres_e.dat
done

echo "m2" >> spheres_r.dat
echo "m2" >> spheres_e.dat

for i in ${!CONTRAST[*]}
do
	FILEROOT=20spheres_c${CONTRAST[i]}
	# Run algorithm
	../build/src/delfin ../data/spheres/${FILEROOT} -m2 -w -t${WALLM2[i]} -v10000
	
	# Compare and save results
	./comparator/compare ${FILEROOT}.dat_voids.dat ../data/spheres/${FILEROOT}_output.off -f >${FILEROOT}_short.dat
	
	mv r.dat ${FILEROOT}_r.dat
	mv e.dat ${FILEROOT}_e.dat

    ./r_merger ${CONTRAST[i]} <${FILEROOT}_r.dat >>spheres_r.dat
	./e_merger ${CONTRAST[i]} <${FILEROOT}_e.dat >>spheres_e.dat
done

echo "m3" >> spheres_r.dat
echo "m3" >> spheres_e.dat

for i in ${!CONTRAST[*]}
do
	FILEROOT=20spheres_c${CONTRAST[i]}
	# Run algorithm
	../build/src/delfin ../data/spheres/${FILEROOT} -m3 -w -t${WALLM3[i]} -v10000
	
	# Compare and save results
	./comparator/compare ${FILEROOT}.dat_voids.dat ../data/spheres/${FILEROOT}_output.off -f >${FILEROOT}_short.dat
	
	mv r.dat ${FILEROOT}_r.dat
	mv e.dat ${FILEROOT}_e.dat

    ./r_merger ${CONTRAST[i]} <${FILEROOT}_r.dat >>spheres_r.dat
	./e_merger ${CONTRAST[i]} <${FILEROOT}_e.dat >>spheres_e.dat
done

echo "m4" >> spheres_r.dat
echo "m4" >> spheres_e.dat

for i in ${!CONTRAST[*]}
do
	FILEROOT=20spheres_c${CONTRAST[i]}
	# Run algorithm
	../build/src/delfin ../data/spheres/${FILEROOT} -m4 -v10000
	
	# Compare and save results
	./comparator/compare ${FILEROOT}.dat_voids.dat ../data/spheres/${FILEROOT}_output.off -f >${FILEROOT}_short.dat
	
	mv r.dat ${FILEROOT}_r.dat
	mv e.dat ${FILEROOT}_e.dat

    ./r_merger ${CONTRAST[i]} <${FILEROOT}_r.dat >>spheres_r.dat
	./e_merger ${CONTRAST[i]} <${FILEROOT}_e.dat >>spheres_e.dat
done