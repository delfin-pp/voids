#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>
#include <cstdlib>

using namespace std;

int main(int argc, char *argv[])
{
	vector<vector<double> > mat;
	char line[256];
	while (true) {
		cin.getline(line, 256);
		if (cin.eof())
			break;
		char *tok;
		mat.push_back(vector<double>(1, atof(tok = strtok(line, " \t"))));
		while ((tok = strtok(NULL, " \t")) != NULL) {
			mat.back().push_back(atof(tok));
		}
	}
	double sum = 0;
	for (int j = 0; j < mat[0].size(); j++) {
		double min = 1;
		for (int i = 0; i < mat.size(); i++) {
			if (min > mat[i][j]) {
				min = mat[i][j];
			}
		}
		sum += min;
	}
	cout << "(" << argv[1] << ", " << sum/mat[0].size() << ")\n";
	return 0;
}
