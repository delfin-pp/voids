#!/bin/bash

# Generate 3D cubes

CONTRAST=( 80    40   26_6    20     16         15  13_3         12     10     8   6_6     5     4     2   1_5)
OUTER=(.00400 .0040 .00400 .0040 .00400 .004000000 .0040 .004000000  .0040 .0040 .0040 .0040 .0040 .0040 .0040)
INNER=(.00005 .0001 .00015 .0002 .00025 .000266666 .0003 .000333333  .0004 .0005 .0006 .0008 .0010 .0020 .0027)
WALLM0=(0.09 0.09 0.07 0.09 0.09 0.12 0.11 0.14 0.13 0.14 0.15 0.16 0.18 0.25 0.25)
WALLM1=(0.29 0.28 0.29 0.29 0.32 0.37 0.35 0.38 0.33 0.39 0.4 0.4 0.4 0.47 0.47)
WALLM2=(0.07 0.05 0.07 0.078 0.078 0.072 0.082 0.095 0.09 0.095 0.15 0.15 0.16 0.17 0.195)
WALLM3=(0.12 0.01 0.13 0.09 0.16 0.18 0.14 0.14 0.14 0.14 0.085 0.165 0.15 0.175 0.22)

rm -f cubes_r.dat
rm -f cubes_e.dat
touch cubes_r.dat
touch cubes_e.dat

#for i in ${!CONTRAST[*]}
#do
#	FILEROOT=20cubes_c${CONTRAST[i]}
	
	# Generate points and triangulations
#	./generator/cubes/3dCubesGenerator -n 20 -s 30.0 -x 30.0 -m 100.0 -d ${OUTER[i]} -v ${INNER[i]} -o $FILEROOT.dat
#	qdelaunay TI $FILEROOT.dat Fv Qt TO ${FILEROOT}_vertex.dat
#	qdelaunay TI $FILEROOT.dat Fn Qt TO ${FILEROOT}_neighbours.dat
	
	# Move to data folder
#	mv $FILEROOT.dat ../data/cubes/
#	mv ${FILEROOT}_vertex.dat ../data/cubes/
#	mv ${FILEROOT}_neighbours.dat ../data/cubes/
#done
echo "m0" >> cubes_r.dat
echo "m0" >> cubes_e.dat

for i in ${!CONTRAST[*]}
do
	FILEROOT=20cubes_c${CONTRAST[i]}
	# Run algorithm
	../build/src/delfin ../data/cubes/${FILEROOT} -m0 -w -t${WALLM0[i]} -v10000
	
	# Compare and save results
	./comparator/compare ${FILEROOT}.dat_voids.dat ../data/cubes/${FILEROOT}_output.off -c >${FILEROOT}_short.dat
	
	mv r.dat ${FILEROOT}_r.dat
	mv e.dat ${FILEROOT}_e.dat

    ./r_merger ${CONTRAST[i]} <${FILEROOT}_r.dat >>cubes_r.dat
	./e_merger ${CONTRAST[i]} <${FILEROOT}_e.dat >>cubes_e.dat
done


echo "m1" >> cubes_r.dat
echo "m1" >> cubes_e.dat

for i in ${!CONTRAST[*]}
do
	FILEROOT=20cubes_c${CONTRAST[i]}
	# Run algorithm
	../build/src/delfin ../data/cubes/${FILEROOT} -m1 -w -t${WALLM1[i]} -v10000
	
	# Compare and save results
	./comparator/compare ${FILEROOT}.dat_voids.dat ../data/cubes/${FILEROOT}_output.off -c >${FILEROOT}_short.dat
	
	mv r.dat ${FILEROOT}_r.dat
	mv e.dat ${FILEROOT}_e.dat

    ./r_merger ${CONTRAST[i]} <${FILEROOT}_r.dat >>cubes_r.dat
	./e_merger ${CONTRAST[i]} <${FILEROOT}_e.dat >>cubes_e.dat
done

echo "m2" >> cubes_r.dat
echo "m2" >> cubes_e.dat

for i in ${!CONTRAST[*]}
do
	FILEROOT=20cubes_c${CONTRAST[i]}
	# Run algorithm
	../build/src/delfin ../data/cubes/${FILEROOT} -m2 -w -t${WALLM2[i]} -v10000
	
	# Compare and save results
	./comparator/compare ${FILEROOT}.dat_voids.dat ../data/cubes/${FILEROOT}_output.off -c >${FILEROOT}_short.dat
	
	mv r.dat ${FILEROOT}_r.dat
	mv e.dat ${FILEROOT}_e.dat

    ./r_merger ${CONTRAST[i]} <${FILEROOT}_r.dat >>cubes_r.dat
	./e_merger ${CONTRAST[i]} <${FILEROOT}_e.dat >>cubes_e.dat
done

echo "m3" >> cubes_r.dat
echo "m3" >> cubes_e.dat

for i in ${!CONTRAST[*]}
do
	FILEROOT=20cubes_c${CONTRAST[i]}
	# Run algorithm
	../build/src/delfin ../data/cubes/${FILEROOT} -m3 -w -t${WALLM3[i]} -v10000
	
	# Compare and save results
	./comparator/compare ${FILEROOT}.dat_voids.dat ../data/cubes/${FILEROOT}_output.off -c >${FILEROOT}_short.dat
	
	mv r.dat ${FILEROOT}_r.dat
	mv e.dat ${FILEROOT}_e.dat

    ./r_merger ${CONTRAST[i]} <${FILEROOT}_r.dat >>cubes_r.dat
	./e_merger ${CONTRAST[i]} <${FILEROOT}_e.dat >>cubes_e.dat
done

echo "m4" >> cubes_r.dat
echo "m4" >> cubes_e.dat

for i in ${!CONTRAST[*]}
do
	FILEROOT=20cubes_c${CONTRAST[i]}
	# Run algorithm
	../build/src/delfin ../data/cubes/${FILEROOT} -m4 -v10000
	
	# Compare and save results
	./comparator/compare ${FILEROOT}.dat_voids.dat ../data/cubes/${FILEROOT}_output.off -c >${FILEROOT}_short.dat
	
	mv r.dat ${FILEROOT}_r.dat
	mv e.dat ${FILEROOT}_e.dat

    ./r_merger ${CONTRAST[i]} <${FILEROOT}_r.dat >>cubes_r.dat
	./e_merger ${CONTRAST[i]} <${FILEROOT}_e.dat >>cubes_e.dat
done