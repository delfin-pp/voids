/**
 * DELFIN++ -- Delauney Edge Void Finder (++) -- ./utils/alpha_shape_triangulation.cpp
 * 
 */

// =============================================================================
//
//   Program: alpha_shape_triangulation
//
//   Description: Takes a set of 3 dimensional points and generates the
//                Delaunay triangulation constricted by the alpha shape that 
//                contains all the points inside one (1) connected solid
//                component.
//
// ============================================================================


#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

#include <CGAL/Alpha_shape_3.h>
#include <CGAL/Alpha_shape_cell_base_3.h>
#include <CGAL/Alpha_shape_vertex_base_3.h>
#include <CGAL/Delaunay_triangulation_3.h>

#include <CGAL/Point_set_3.h>

#include <fstream>
#include <vector>
#include <limits>
#include <iostream>
#include <list>


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;

typedef CGAL::Alpha_shape_vertex_base_3<K> Vb;
typedef CGAL::Alpha_shape_cell_base_3<K> Fb;
typedef CGAL::Triangulation_data_structure_3<Vb,Fb> Tds;
typedef CGAL::Delaunay_triangulation_3<K,Tds> Triangulation_3;
typedef CGAL::Alpha_shape_3<Triangulation_3> Alpha_shape_3;

typedef K::Point_3 Point;
typedef K::Vector_3 Vector;
typedef Alpha_shape_3::Alpha_iterator Alpha_iterator;

typedef CGAL::Point_set_3<Point, Vector> Point_set;

typedef Alpha_shape_3::Finite_vertex_handles Finite_vertex_handles;
typedef Finite_vertex_handles::iterator Finite_vertex_handles_iterator;

/**
 * @brief Prints the syntax or usage of this program.
 * 
 */
void syntax();

/**
 * @brief Given a filename or a complete path, retrieves the file extension.
 * 
 * @param s The filename or path
 * @return std::string The file extension
 */
std::string get_file_ext(const std::string& s) 
{

   size_t i = s.rfind('.', s.length());
   if (i != std::string::npos) {
      return(s.substr(i+1, s.length() - i));
   }

   return("");
}

/**
 * @brief Given a full path of a filename, e.g., d:\apps\src\foo.c, retrieves
 * the file name (with path), d:\apps\src\foo.
 * 
 * @param s The filename or path
 * @return std::string The filename without extension
 */
std::string get_file_name(const std::string& s) 
{

   size_t i = s.rfind('.', s.length());
   if (i != std::string::npos) {
      return(s.substr(0, i));
   }

   return("");
}

int main(int argc, char** argv)
{
    if (argc != 2) 
    {
        syntax();
        return -1;
    }
    std::string path = argv[1];
    std::string f_ext = get_file_ext(path);
    std::string f_name = get_file_name(path);

    std::ifstream f(path);
    int n_points;

    // Read file according to its extension

    if (f_ext == "dat" || f_ext== ""){

        int dim;
        f >> dim;
        assert(dim == 3);
        f >> n_points;

    } else if (f_ext == "ply"){
        
        std::string line;
        std::string end_header("end_header");
        while(line.compare(end_header) != 0)
        {
            getline(f,line);
            if (line.find("element vertex") != std::string::npos)
            {
                size_t ls = line.find_last_of(" ");
                n_points = std::stoi(line.substr(ls+1, line.length()));
            }
        }

    } else if (f_ext == "off"){

        std::string line;
        std::string OFF("OFF");
        getline(f, line);
        assert(line.compare(OFF) == 0);
        int a;
        f >> n_points >> a >> a;

    } else {

        syntax();
        return -1;
        
    }

    std::vector<Point> points;
    double x, y, z;

    for( ; n_points > 0 ; n_points--)
    {
        f >> x >> y >> z;
        points.push_back( Point(x, y, z) );
    }

    std::cout << points.size() << " points read." << std::endl;
    // compute alpha shape
    Alpha_shape_3 as(points.begin(), points.end(), 0, Alpha_shape_3::REGULARIZED);
    Alpha_iterator opt = as.find_optimal_alpha(1);
    std::cout << "Optimal alpha value to get one connected component is " << *opt << std::endl;
    as.set_alpha(*opt);
    std::cerr << as.number_of_solid_components() << " number of solid components\n";
    assert(as.number_of_solid_components() == 1);
    // create index for every vertex (point)
    std::unordered_map< Alpha_shape_3::Vertex_handle, std::size_t> verts;
    points.clear();
    //Finite_vertex_handles fvh = as.finite_vertex_handles();
    for(Alpha_shape_3::Vertex_handle vh : as.finite_vertex_handles())
    {
        if (verts.insert( std::make_pair(vh, points.size()) ).second)
            points.push_back( vh->point() );
    }

    std::map< Alpha_shape_3::Cell_handle, int> cells;

    int n_tetra=0;
    // save cells (tetraedra) with one or more faces inside the alpha shape
    for(Alpha_shape_3::Cell_handle ch : as.finite_cell_handles())
    {    
        for (int i = 0; i < 4; ++i)
        {
            if (as.classify(Alpha_shape_3::Facet(ch, i))==Alpha_shape_3::INTERIOR)
            {
                cells.insert(std::make_pair(ch, n_tetra));
                n_tetra++;
                
                break;
            }
        }
        
    }

    // Output triangulation
    std::ofstream out_points(f_name + std::string(".dat"));
    out_points << "3\n";
    out_points << points.size() << "\n";
    std::copy(points.begin(), points.end(), std::ostream_iterator<Point>(out_points, "\n"));

    std::ofstream out_vertex(f_name + std::string("_vertex.dat"));
    std::ofstream out_neighs(f_name + std::string("_neighbours.dat"));
    out_vertex << n_tetra << "\n";
    out_neighs << n_tetra << "\n";

    for (std::map<Alpha_shape_3::Cell_handle, int>::iterator it=cells.begin(); it!=cells.end(); ++it)
    {
        Alpha_shape_3::Cell_handle ch = it->first;
        out_vertex << 4;
        out_neighs << 4; 
        for (int j = 0; j < 4; ++j) // vertex of every face of the facet
        {
            Alpha_shape_3::Vertex_handle vh = ch->vertex(j);
            out_vertex << " " << verts[vh];

            Alpha_shape_3::Cell_handle neigh = ch->neighbor(j);
            try
            {
                out_neighs << " " << cells.at(neigh);
            }
            catch(const std::out_of_range& e)
            {
                out_neighs << "-1";
            }
        }
        out_vertex << "\n";
        out_neighs << "\n";
    }
     
    return 0;
}

// Prints the syntax or usage of this program.
void syntax()
{
    printf("Command Line Syntax: \n");
    printf("\n");
    printf("    alpha_shape_triangulation input_file\n");
    printf("\n");
    printf("Supported File Types: \n");
    printf("\n");
    printf("    ply, off, dat\n");
    printf("\n");
}