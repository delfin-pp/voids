g++ r_merger.cpp -o r_merger
g++ e_merger.cpp -o e_merger

cd generator/cubes
g++ 3dCubesGenerator.cpp -o 3dCubesGenerator

cd ../spheres
g++ 3dSpheresGenerator.cpp -o 3dSpheresGenerator

cd ../comparator
make