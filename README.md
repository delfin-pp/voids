# DELFIN++

Delfin++ (Delaunay Edge Void Finder) is a simple algorithm to find cosmological voids in a 3-dimensional distribution of galaxies, based on Carlos Hervias Caimapo and Rodrigo Alonso's algorithm. 
The voids are found by searching for underdense polyhedral regions in a Delaunay tessellation of the galaxies.

# Output
The output of this program is 4 files:
- An OFF file with the voids. Each void is of a random color and is the polygon formed by the set of tetrahedra found as part of each void.
- A ``..._output_centers.dat.out`` file with the following info:

      center x | center y | center z | volume | radius | void ID | density

- A ``..._shapes_centers.dat.out`` file with the following info:
    
      void ID | ellip | eig(1) | eig(2) | eig(3) | eigv(1)-x | eigv(1)-y | eigv(1)-z | eigv(2)-x | eigv(2)-y | eigv(2)-z | eigv(3)-x | eigv(3)-y | eigv(3)-z 
- A ``..._void_points.dat.out`` file with the following info:

      void ID | particle ID | x | y | z 

# Requirements

- CMake - cross-platform family of tools designed to build, test and package software
- Qhull - qhull is a general dimension code for computing convex hulls, Delaunay triangulations.
- GoogleTest - google unit test suite.

## Specific requirements
Requirements to run test with coverage:

- GCOV - tool to test code coverage in programs
- LCOV - graphical front-end for GCC's coverage testing tool gcov

Requirements to compute alpha shape of the point cloud (alpha_shape_triangulation):

- CGAL 5.0
    - Boost - the Boost libraries are a set of portable C++ source libraries
    - GMP - GNU Multiple Precision Arithmetic
    - MPFR - GNU Multiple Precision Floating-Point Reliably


# Usage
To compile the main program run:
```bash
mkdir build && cd build
cmake ..
make
```

Then the executable is in ```build/src/delfin```.
By default CMake compiles the executable in ``build/utils/alpha_shape_triangulation``, if you dont want to compile this program, instead of the last two lines run: 

```bash
cmake -D USE_ASHAPE_TRIANGULATION=OFF ..
make
```
To run the main program, there needs to be 3 .dat files named in the following way: in.dat, in_vertex.dat and in_neighbours.dat. The composition of the ``in.dat`` file needs to be as follows:

 - First line: Number of dimensions
 - Second line: Number of points
 - Third line: the number of vertices, number of faces, and number of edges.
 - List of vertices: X, Y and Z coordinates.

Due to this program only working on 3 dimensions and on a point cloud, the first and third line are set as "3" and "0 0 0" respectively. Example:

```java
3
5
0 0 0
1523.4 142.337 2.91717
922.989 166.181 -41.633
548.876 437.57 -39.353
1388.53 572.971 16.2987
827.572 386.877 23.2227
```

After checking that the ``in.dat`` file has the correct format, there's two ways to compute the tetrahedralization of the point cloud and generate both ``in_vertex.dat`` and ``in_neighbours.dat``. The first one generates the tetrahedralization of the convex hull, and is done with Qhull:

```bash
qdelaunay TI in.dat Fv Qt TO in_vertex.dat
qdelaunay TI in.dat Fn Qt TO in_neighbours.dat
```
The second one generates the tetrahedralization of the alpha shape of the point cloud, and is done with alpha_shape_triangulation:

```bash
./alpha_shape_triangulation in.dat
```
## Run the program

With the 3 files in the same folder, run the compiled program with

```bash
./delfin in [-bqv_p_e_m_wt_h]
```

Below is a list of short descriptions fot the basic command line syntax of DELFIN. Underscores indicate that numbers follow follow certain switches.  Do not leave any space between a switch and its numeric parameter.

| Switch | Description                                                                                                                                                                                                                                                                                                                          | Default value |
|--------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|
| -b     | Doesn't discard voids with tetrahedrons in the boundary of the convex hull.                                                                                                                                                                                                                                                          |           |
| -q     | Removes poor quality tetrahedra from the edge of  voids with a high percentage of area covered by  these tetrahedra.                                                                                                                                                                                                                 |          |
| -v     | Applies a minimum void volume constraint.                                                                                                                                                                                                                                                                                            | 0.0           |
| -V     | Applies a minimum void volume constraint.                                                                                                                                                                                                                                                                                            | 3.40e+38      |
| -p     | Applies a minimum void percent volume constraint.                                                                                                                                                                                                                                                                                    | 0.0           |
| -e     | Applies a minimun edge length constraint to generate a new void.                                                                                                                                                                                                                                                                     | 0.0           |
| -m     | Specifies the type of void builder to use.                                                                                                                                                                                                                                                                                           | 0      |
|        | -m0: Builder by point density according to the volume of its tetraedra. 
| | -m1: Builder by point density according to the length of its edges. 
| |-m2: Builder by edge density according to the volume of its tetraedra.
| |-m3: Builder by edge density according to its length and the volume of its tetraedra.
| |-m4: Builder by longest edge. |               |
| -w     | Specifies that the threshold density is according  to the mean element density.                                                                                                                                                                                                                                                      |          |
| -t     | Specifies the threshold density of the elements that  are part of a void.                                                                                                                                                                                                                                                            | 0.2           |
| -h     | Help: A brief instruction for using DELFIN                                                                                                                                                                                                                                                                                           |               |

# Tests
This project uses the Google test framework for c++. To use it, clone the googletest repository in the lib directory.

```bash
git clone https://github.com/google/googletest/
```

To run the unit tests, go to the test folder and input the following commands.
```bash
mkdir build && cd build
cmake ..
make delfin_test
./delfin_test
```
To run test with coverage, run (after ```make delfin_test```):
```bash
make init
make gcov
make lcov
```

# Useful links & docs

- [Public Cosmic Void Catalog](http://www.cosmicvoids.net/home) with the VIDE void finding code.
- CGAL 5.0 [Manual](https://doc.cgal.org/5.0.4/Manual/index.html).