#include "gtest/gtest.h"
#include "../src/TetrahedronDictionary.hpp"
#include "../src/geom3d.hpp"
#include <stdlib.h>
#include <vector>
#include <thread>
#include <cmath>

namespace Geom3dTest
{
    class Geom3dTest : public ::testing::Test
    {
    protected:
        Geom3dTest() {}
        virtual ~Geom3dTest() {}

        virtual void SetUp()
        {
            u.push_back(-3.);
            u.push_back(1.);
            u.push_back(0.);

            v.push_back(1);
            v.push_back(6);
            v.push_back(8);

            a.push_back(4.);
            a.push_back(-5.);
            a.push_back(10.);

            b.push_back(2);
            b.push_back(4);
            b.push_back(-2);
        }

        virtual void TearDown()
        {
            u.clear();
            v.clear();

            a.clear();
            b.clear();
        }

        vector<double> u;
        vector<double> v;

        vector<double> a;
        vector<double> b;
    };

    TEST_F(Geom3dTest, dotpr)
    {
        ASSERT_EQ(3., dotpr(u, v));
        ASSERT_EQ(-32., dotpr(a, b));
        ASSERT_EQ(-32., dotpr(b, a));
        ASSERT_EQ(-2., dotpr(u, b));
    }

    TEST_F(Geom3dTest, cross)
    {
        vector<double> aux{8, 24, -19};
        vector<double> res = cross(u, v);
        for (int i = 0; i < 3; i++)
        {
            ASSERT_EQ(aux[i], res[i]);
        }
        res = cross(v, u);
        for (int i = 0; i < 3; i++)
        {
            ASSERT_EQ(-aux[i], res[i]);
        }
    }

    TEST_F(Geom3dTest, halfpVector)
    {
        ASSERT_EQ(-656., halfp(a, b, v, u));
        ASSERT_EQ(656., halfp(u, a, b, v));
    }

    TEST_F(Geom3dTest, halfpFloat)
    {
        float u2[3] = {-3, 1, 0};
        float v2[3] = {1, 6, 8};
        float a2[3] = {4, -5, 10};
        float b2[3] = {2, 4, -2};
        ASSERT_EQ(-656., halfp(a2, b2, v2, u2));
        ASSERT_EQ(656., halfp(u2, a2, b2, v2));
    }

    TEST_F(Geom3dTest, triangleArea)
    {
        float a[3] = {-1.64, 2.84, 0};
        float b[3] = {3.58, -1.52, 0};
        float c[3] = {-3.56, -1.2, 0};
        ASSERT_NEAR(14.73, triangleArea(a, b, c), 0.01);
        float x[3] = {-4.26, 0.0, 4.2};
        float y[3] = {4.0, 0.0, 0.0};
        float z[3] = {-2.68, 0.0, -1.56};
        ASSERT_NEAR(20.4708, triangleArea(x, y, z), 0.01);
    }

    TEST_F(Geom3dTest, angle2vec)
    {
        vector<double> u{0, 1, 0};
        vector<double> v{0, 0, 1};
        ASSERT_DOUBLE_EQ(90, angle2vec(u, v));

        vector<double> x{0.5, 0.5, 0.5};
        vector<double> y{1, 0, 1};
        ASSERT_NEAR(35.264, angle2vec(x, y), 0.001);
    }
}