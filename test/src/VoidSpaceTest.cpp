#include "gtest/gtest.h"
#include "../src/TetrahedronDictionary.hpp"
#include "../src/VoidSpace.hpp"
#include <vector>

namespace VoidSpaceTest
{
    class VoidSpaceTest : public ::testing::Test
    {
    protected:
        VoidSpaceTest() {}
        virtual ~VoidSpaceTest() {}

        virtual void SetUp()
        {
            voidSpace = new VoidSpace();
            dictPoints = new PointDictionary();
            dictTetrahedra = new TetrahedronDictionary();
            string output_file = "dictPointsTestFile.dat";
            ofstream ofs(output_file.c_str());
            if (ofs.good())
            {
                ofs << "3\n";
                ofs << "6\n";
                ofs << "1.\t1.\t1.\n";
                ofs << "1.\t1.\t3.\n";
                ofs << "3.\t1.\t1.\n";
                ofs << "3.\t1.\t3.\n";
                ofs << "2.\t1.\t2.\n";
                ofs << "2.\t3.\t2.\n";
                ofs.close();

                dictPoints->load("dictPointsTestFile");
                remove(output_file.c_str());
            }

            string output_file_neighbours = "dictTetrahedraTestFile_neighbours.dat";
            string output_file_vertexes = "dictTetrahedraTestFile_vertex.dat";
            ofstream neighbours_ofs(output_file_neighbours.c_str());
            ofstream vertexes_ofs(output_file_vertexes.c_str());
            if (neighbours_ofs.good())
            {
                neighbours_ofs << "4\n";
                neighbours_ofs << "4 -11 -18 1 2\n";
                neighbours_ofs << "4 -10 0 -19 3\n";
                neighbours_ofs << "4 0 -11 -18 3\n";
                neighbours_ofs << "4 1 -10 2 -19\n";
                neighbours_ofs.close();
            }

            if (vertexes_ofs.good())
            {
                vertexes_ofs << "4\n";
                vertexes_ofs << "4 4 5 2 0\n";
                vertexes_ofs << "4 4 1 5 0\n";
                vertexes_ofs << "4 3 4 5 2\n";
                vertexes_ofs << "4 3 4 1 5\n";
                vertexes_ofs.close();
            }

            dictTetrahedra->load("dictTetrahedraTestFile");
            remove("dictTetrahedraTestFile_vertex.dat");
            remove("dictTetrahedraTestFile_neighbours.dat");

            dictTetrahedra->setPointDictionary(dictPoints);

            //

            dictPoints2 = new PointDictionary();
            dictTetrahedra2 = new TetrahedronDictionary();
            string output_file2 = "dictPointsTestFile2.dat";
            ofstream ofs2(output_file2.c_str());
            if (ofs2.good())
            {
                ofs2 << "3\n";
                ofs2 << "7\n";
                ofs2 << "-3.13\t-1.22\t0.\n";
                ofs2 << "-1.56\t3.19\t0.\n";
                ofs2 << "1.99\t2.18\t0.\n";
                ofs2 << "0.99\t-2.71\t0.\n";
                ofs2 << "-0.66\t0.27\t4.4\n";
                ofs2 << "-0.66\t0.27\t-4.4\n";
                ofs2 << "-0.66\t0.27\t0.\n";
                ofs2.close();

                dictPoints2->load("dictPointsTestFile2");
                remove(output_file2.c_str());
            }

            string output_file_neighbours2 = "dictTetrahedraTestFile2_neighbours.dat";
            string output_file_vertexes2 = "dictTetrahedraTestFile2_vertex.dat";
            ofstream neighbours_ofs2(output_file_neighbours2.c_str());
            ofstream vertexes_ofs2(output_file_vertexes2.c_str());
            if (neighbours_ofs2.good())
            {
                neighbours_ofs2 << "8\n";
                neighbours_ofs2 << "4 -4 2 4 1\n";
                neighbours_ofs2 << "4 -5 3 5 0\n";
                neighbours_ofs2 << "4 -4 0 6 3\n";
                neighbours_ofs2 << "4 -5 1 7 2\n";
                neighbours_ofs2 << "4 0 -12 6 5\n";
                neighbours_ofs2 << "4 1 -13 7 4\n";
                neighbours_ofs2 << "4 2 -12 4 7\n";
                neighbours_ofs2 << "4 3 -13 5 6\n";

                neighbours_ofs2.close();
            }

            if (vertexes_ofs2.good())
            {
                vertexes_ofs2 << "8\n";
                vertexes_ofs2 << "4 6 4 3 0\n";
                vertexes_ofs2 << "4 6 4 3 2\n";
                vertexes_ofs2 << "4 6 5 3 0\n";
                vertexes_ofs2 << "4 6 5 3 2\n";
                vertexes_ofs2 << "4 1 6 4 0\n";
                vertexes_ofs2 << "4 1 6 4 2\n";
                vertexes_ofs2 << "4 1 6 5 0\n";
                vertexes_ofs2 << "4 1 6 5 2\n";
                vertexes_ofs2.close();
            }

            dictTetrahedra2->load("dictTetrahedraTestFile2");
            remove("dictTetrahedraTestFile2_vertex.dat");
            remove("dictTetrahedraTestFile2_neighbours.dat");

            dictTetrahedra2->setPointDictionary(dictPoints2);
        }

        virtual void TearDown()
        {
            // Code here will be called immediately after each test (right
            // before the destructor).
        }
        // Objects declared here can be used by all tests in the test case for Foo.
        TetrahedronDictionary *dictTetrahedra;
        PointDictionary *dictPoints;
        TetrahedronDictionary *dictTetrahedra2;
        PointDictionary *dictPoints2;
        VoidSpace *voidSpace;
    };

    TEST_F(VoidSpaceTest, voidSpace)
    {
        ASSERT_EQ(0, voidSpace->getId());
        ASSERT_FLOAT_EQ(0., voidSpace->getVolume());
    }

    TEST_F(VoidSpaceTest, Id)
    {
        ASSERT_EQ(0, voidSpace->getId());
        voidSpace->setId(1);
        ASSERT_EQ(1, voidSpace->getId());
        voidSpace->setId(20);
        ASSERT_EQ(20, voidSpace->getId());
    }

    TEST_F(VoidSpaceTest, tetrahedronDict)
    {
        ASSERT_NE(dictTetrahedra, voidSpace->getTetrahedronDictionary());
        voidSpace->setTetrahedronDictionary(dictTetrahedra);
        ASSERT_EQ(dictTetrahedra, voidSpace->getTetrahedronDictionary());
    }

    TEST_F(VoidSpaceTest, tetrahedraId)
    {
        vector<int> res = voidSpace->getTetrahedraId();
        ASSERT_EQ(0, res.size());

        vector<int> t = {1, 2};
        voidSpace->setTetrahedronDictionary(dictTetrahedra);
        voidSpace->setTetrahedraId(t);
        res = voidSpace->getTetrahedraId();
        ASSERT_EQ(2, res.size());
        for (int i = 0; i < 2; i++)
        {
            ASSERT_EQ(t[i], res[i]);
        }
    }

    TEST_F(VoidSpaceTest, addTetrahedronId)
    {
        vector<int> res = voidSpace->getTetrahedraId();
        ASSERT_EQ(0, res.size());
        voidSpace->setTetrahedronDictionary(dictTetrahedra);
        voidSpace->addTetrahedronId(2);
        voidSpace->addTetrahedronId(3);
        res = voidSpace->getTetrahedraId();
        ASSERT_EQ(2, res.size());
        ASSERT_EQ(2, res[0]);
        ASSERT_EQ(3, res[1]);
    }

    TEST_F(VoidSpaceTest, getVolume)
    {
        float res = voidSpace->getVolume();
        ASSERT_FLOAT_EQ(0, res);
        voidSpace->setTetrahedronDictionary(dictTetrahedra);
        voidSpace->addTetrahedronId(2);
        voidSpace->addTetrahedronId(3);
        ASSERT_FLOAT_EQ(4. / 3., voidSpace->getVolume());
    }

    TEST_F(VoidSpaceTest, isInBorder)
    {
        ASSERT_EQ(0, voidSpace->isInBorder());
        voidSpace->setTetrahedronDictionary(dictTetrahedra);
        voidSpace->addTetrahedronId(2);
        voidSpace->addTetrahedronId(3);
        ASSERT_EQ(1, voidSpace->isInBorder());
    }

    TEST_F(VoidSpaceTest, getTetrahedraLength)
    {
        voidSpace->setTetrahedronDictionary(dictTetrahedra);
        ASSERT_EQ(0, voidSpace->getTetrahedraLength());
        voidSpace->addTetrahedronId(2);
        voidSpace->addTetrahedronId(3);
        ASSERT_EQ(2, voidSpace->getTetrahedraLength());
    }

    TEST_F(VoidSpaceTest, getPointsId)
    {
        vector<int> v = voidSpace->getPointsId();
        ASSERT_EQ(0, v.size());
        voidSpace->setTetrahedronDictionary(dictTetrahedra);
        voidSpace->addTetrahedronId(2);
        voidSpace->addTetrahedronId(3);
        vector<int> p = {1, 2, 3, 4, 5};
        v = voidSpace->getPointsId();
        ASSERT_EQ(5, v.size());
        sort(v.begin(), v.end());
        for (int i = 0; i < 5; i++)
        {
            ASSERT_EQ(p[i], v[i]);
        }
        voidSpace->addTetrahedronId(1);
        v = voidSpace->getPointsId();
        ASSERT_EQ(6, v.size());
        sort(v.begin(), v.end());
        ASSERT_EQ(0, v[0]);
    }

    TEST_F(VoidSpaceTest, clearTetrahedra)
    {
        voidSpace->setTetrahedronDictionary(dictTetrahedra);
        voidSpace->addTetrahedronId(2);
        voidSpace->addTetrahedronId(3);
        ASSERT_FLOAT_EQ(4. / 3., voidSpace->getVolume());
        ASSERT_EQ(2, voidSpace->getTetrahedraLength());
        voidSpace->clearTetrahedra();
        ASSERT_FLOAT_EQ(0., voidSpace->getVolume());
        ASSERT_EQ(0, voidSpace->getTetrahedraLength());
    }

    TEST_F(VoidSpaceTest, getDensity)
    {
        vector<int> tId = {0, 1, 2, 3, 4, 5, 6, 7};
        voidSpace->setTetrahedronDictionary(dictTetrahedra2);
        voidSpace->setTetrahedraId(tId);
        ASSERT_FLOAT_EQ(57.02106667, voidSpace->getVolume());
        ASSERT_FLOAT_EQ(1. / 57.02106667, voidSpace->getDensity());
    }

    TEST_F(VoidSpaceTest, getPercentageBorderSurfaceArea)
    {
        vector<int> tId = {0, 1, 2, 3, 4, 5, 6, 7};
        voidSpace->setTetrahedronDictionary(dictTetrahedra2);
        voidSpace->setTetrahedraId(tId);
        voidSpace->analyzeSurfaceArea();
        ASSERT_FLOAT_EQ(1.0, voidSpace->getPercentageBorderSurfaceArea());
    }

    TEST_F(VoidSpaceTest, getPercentagePoorQualitySurfaceArea)
    {
        vector<int> tId = {0, 1, 2, 3, 4, 5, 6, 7};
        voidSpace->setTetrahedronDictionary(dictTetrahedra2);
        voidSpace->setTetrahedraId(tId);
        voidSpace->analyzeSurfaceArea();
        ASSERT_FLOAT_EQ(0.0, voidSpace->getPercentagePoorQualitySurfaceArea());
    }

    TEST_F(VoidSpaceTest, deleteExternalTetrahedra)
    {
        vector<int> tId = {0, 1, 2, 3};
        voidSpace->setTetrahedronDictionary(dictTetrahedra2);
        voidSpace->setTetrahedraId(tId);
        voidSpace->deleteExternalTetrahedra();
        ASSERT_EQ(0, voidSpace->getTetrahedraLength());
        ASSERT_FLOAT_EQ(0.0, voidSpace->getVolume());
    }
}