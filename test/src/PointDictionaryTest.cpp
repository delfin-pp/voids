#include "gtest/gtest.h"
#include "../src/PointDictionary.hpp"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>

namespace PointDictionaryTest
{
    class PointDictionaryTest : public ::testing::Test
    {
    protected:
        PointDictionaryTest() {}
        virtual ~PointDictionaryTest() {}

        virtual void SetUp()
        {
            points = new PointDictionary();
        }

        virtual void TearDown()
        {
        }
        PointDictionary *points;
    };

    TEST_F(PointDictionaryTest, pointDictionary)
    {
        ASSERT_EQ(0, points->getDataLength());
    }

    TEST_F(PointDictionaryTest, getById)
    {
        float a[3] = {1.2, 4.3, 7.4};
        float b[3] = {2.2, 5.3, 8.4};
        float c[3] = {3.2, 6.3, 9.4};
        points->add(a);
        points->add(b);
        points->add(c);

        float *point = points->getById(1);
        ASSERT_FLOAT_EQ(2.2, point[0]);
        ASSERT_FLOAT_EQ(5.3, point[1]);
        ASSERT_FLOAT_EQ(8.4, point[2]);
    }

    TEST_F(PointDictionaryTest, addFloat)
    {
        float *v = new float[3];
        v[0] = 1;
        v[1] = 6;
        v[2] = 8;
        int res = points->add(v);
        ASSERT_EQ(1, points->getDataLength());
        ASSERT_EQ(0, res);

        float u[3] = {123.2, 1123.0001, 66.4};
        res = points->add(u);
        ASSERT_EQ(2, points->getDataLength());
        ASSERT_EQ(1, res);

        float *firstPoint = points->getById(0);
        float *secondPoint = points->getById(1);

        ASSERT_EQ(1, firstPoint[0]);
        ASSERT_EQ(6, firstPoint[1]);
        ASSERT_EQ(8, firstPoint[2]);

        ASSERT_FLOAT_EQ(123.2, secondPoint[0]);
        ASSERT_FLOAT_EQ(1123.0001, secondPoint[1]);
        ASSERT_FLOAT_EQ(66.4, secondPoint[2]);
    }

    TEST_F(PointDictionaryTest, addFloatPointer)
    {
        float *v = new float[3];
        v[0] = 1;
        v[1] = 6;
        v[2] = 8;
        int res = points->add(&v);
        ASSERT_EQ(1, points->getDataLength());
        ASSERT_EQ(0, res);

        float *u = new float[3];
        u[0] = 123.2;
        u[1] = 1123.0001;
        u[2] = 66.4;
        res = points->add(&u);
        ASSERT_EQ(2, points->getDataLength());
        ASSERT_EQ(1, res);

        float *firstPoint = points->getById(0);
        float *secondPoint = points->getById(1);

        ASSERT_EQ(1, firstPoint[0]);
        ASSERT_EQ(6, firstPoint[1]);
        ASSERT_EQ(8, firstPoint[2]);

        ASSERT_FLOAT_EQ(123.2, secondPoint[0]);
        ASSERT_FLOAT_EQ(1123.0001, secondPoint[1]);
        ASSERT_FLOAT_EQ(66.4, secondPoint[2]);
    }

    TEST_F(PointDictionaryTest, loadPoints)
    {
        string output_file = "dictPointsTestFile.dat";
        ofstream ofs(output_file.c_str());
        if (ofs.good())
        {
            ofs << "3\n";
            ofs << "4\n";
            ofs << "456.3431\t7819.43523452\t9991.20\n";
            ofs << "1232134.432\t43124231.432\t-12314.23\n";
            ofs << "6542.432\t7543.432\t65414.23\n";
            ofs << "13554.432\t-9865.432\t44.23\n";
            ofs.close();

            points->load("dictPointsTestFile");
            float *firstPoint = points->getById(0);
            float *secondPoint = points->getById(1);
            float *thirdPoint = points->getById(2);
            float *fourthPoint = points->getById(3);

            remove(output_file.c_str());

            ASSERT_EQ(firstPoint[0], float(456.3431));
            ASSERT_EQ(firstPoint[1], float(7819.43523452));
            ASSERT_EQ(firstPoint[2], float(9991.20));

            ASSERT_EQ(secondPoint[0], float(1232134.432));
            ASSERT_EQ(secondPoint[1], float(43124231.432));
            ASSERT_EQ(secondPoint[2], float(-12314.23));

            ASSERT_EQ(thirdPoint[0], float(6542.432));
            ASSERT_EQ(thirdPoint[1], float(7543.432));
            ASSERT_EQ(thirdPoint[2], float(65414.23));

            ASSERT_EQ(fourthPoint[0], float(13554.432));
            ASSERT_EQ(fourthPoint[1], float(-9865.432));
            ASSERT_EQ(fourthPoint[2], float(44.23));
        }
    }
}