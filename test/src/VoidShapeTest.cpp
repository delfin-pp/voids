#include "gtest/gtest.h"
#include "../src/VoidShape.hpp"
#include "../src/PointDictionary.hpp"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <vector>
#include <thread>
#include <cmath>

namespace VoidShapeTest
{
    class VoidShapeTest : public ::testing::Test
    {
    protected:
        VoidShapeTest() {}
        virtual ~VoidShapeTest() {}

        virtual void SetUp()
        {
            // Code here will be called immediately after each test (right
            // before the destructor).
            pointDict = new PointDictionary();
            voidShape = new VoidShape();

            string output_file = "dictPointsTestFile.dat";
            ofstream ofs(output_file.c_str());
            if (ofs.good())
            {
                ofs << "3\n";
                ofs << "6\n";
                ofs << "0.0\t8.0\t0.0\n";
                ofs << "0.0\t-8.0\t0.0\n";
                ofs << "6.0\t0.0\t0.0\n";
                ofs << "-6.0\t0.0\t0.0\n";
                ofs << "0.0\t0.0\t5.0\n";
                ofs << "0.0\t0.0\t-5.0\n";
                ofs.close();

                pointDict->load("dictPointsTestFile");
                remove(output_file.c_str());
            }
            pointsId.push_back(0);
            pointsId.push_back(1);
            pointsId.push_back(2);
            pointsId.push_back(3);
            pointsId.push_back(4);
            pointsId.push_back(5);
        }

        virtual void TearDown()
        {
            // Code here will be called immediately after each test (right
            // before the destructor).
        }
        // Objects declared here can be used by all tests in the test case for Foo.
        PointDictionary *pointDict;
        VoidShape *voidShape;
        vector<int> pointsId;
    };

    TEST_F(VoidShapeTest, ellipticity)
    {
        voidShape->analyze(pointsId, pointDict);
        ASSERT_FLOAT_EQ(0.11624383023, voidShape->getEllipticity());
    }

    TEST_F(VoidShapeTest, massCenter)
    {
        voidShape->analyze(pointsId, pointDict);
        float *massCenter = voidShape->getMassCenter();

        ASSERT_FLOAT_EQ(0.0, massCenter[0]);
        ASSERT_FLOAT_EQ(0.0, massCenter[1]);
        ASSERT_FLOAT_EQ(0.0, massCenter[2]);
    }

    TEST_F(VoidShapeTest, eigenValues)
    {
        voidShape->analyze(pointsId, pointDict);
        float *eigenValues = voidShape->getEigenValues();
        ASSERT_FLOAT_EQ(200.0, eigenValues[0]); // ASSERT_FLOAT_EQ(40.0, eigenValues[0]);
        ASSERT_FLOAT_EQ(178.0, eigenValues[1]); // ASSERT_FLOAT_EQ(35.6, eigenValues[1]);
        ASSERT_FLOAT_EQ(122.0, eigenValues[2]); // ASSERT_FLOAT_EQ(24.4, eigenValues[2]);
    }

    TEST_F(VoidShapeTest, eigenVectors)
    {
        voidShape->analyze(pointsId, pointDict);
        float **eV = voidShape->getEigenVectors();
        //(0, 0, 1) -> 40
        ASSERT_FLOAT_EQ(0., eV[0][0]);
        ASSERT_FLOAT_EQ(0., eV[0][1]);
        ASSERT_FLOAT_EQ(1., eV[0][2]);
        //(1, 0, 0) -> 36.6
        ASSERT_FLOAT_EQ(1., eV[1][0]);
        ASSERT_FLOAT_EQ(0., eV[1][1]);
        ASSERT_FLOAT_EQ(0., eV[1][2]);
        //(0, 1, 0) -> 24.4
        ASSERT_FLOAT_EQ(0., eV[2][0]);
        ASSERT_FLOAT_EQ(1., eV[2][1]);
        ASSERT_FLOAT_EQ(0., eV[2][2]);
    }

    TEST_F(VoidShapeTest, inertiaTensor)
    {
        voidShape->analyze(pointsId, pointDict);
        float **I = voidShape->getInertiaTensor();
        ASSERT_FLOAT_EQ(178., I[0][0]); // ASSERT_FLOAT_EQ(35.6, I[0][0]);
        ASSERT_FLOAT_EQ(0., I[0][1]);
        ASSERT_FLOAT_EQ(0., I[0][2]);
        ASSERT_FLOAT_EQ(0., I[1][0]);
        ASSERT_FLOAT_EQ(122., I[1][1]); // ASSERT_FLOAT_EQ(24.4, I[1][1]);
        ASSERT_FLOAT_EQ(0., I[1][2]);
        ASSERT_FLOAT_EQ(0., I[2][0]);
        ASSERT_FLOAT_EQ(0., I[2][1]);
        ASSERT_FLOAT_EQ(200., I[2][2]); // ASSERT_FLOAT_EQ(40., I[2][2]);
    }
}