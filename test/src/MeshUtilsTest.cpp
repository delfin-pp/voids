#include "gtest/gtest.h"
#include "../src/TetrahedronDictionary.hpp"
#include "../src/Edge.hpp"
#include "../src/meshUtils.hpp"
#include <stdlib.h>
#include <vector>
#include <thread>
#include <cmath>

namespace MeshUtilsTest
{
    class MeshUtilsTest : public ::testing::Test
    {
    protected:
        MeshUtilsTest() {}
        virtual ~MeshUtilsTest() {}

        virtual void SetUp()
        {
            dictPoints = new PointDictionary();
            dictEdges = new EdgeDictionary();
            dictTetrahedra = new TetrahedronDictionary();
        }

        virtual void TearDown()
        {
            // Code here will be called immediately after each test (right
            // before the destructor).
        }
        PointDictionary *dictPoints;
        EdgeDictionary *dictEdges;
        TetrahedronDictionary *dictTetrahedra;
    };

    TEST_F(MeshUtilsTest, GetR3Test)
    {
        string output_file = "dictPointsTestFile.dat";
        ofstream ofs(output_file.c_str());
        if (ofs.good())
        {
            ofs << "3\n";
            ofs << "6\n";
            ofs << "1.\t1.\t1.\n";
            ofs << "1.\t1.\t3.\n";
            ofs << "3.\t1.\t1.\n";
            ofs << "3.\t1.\t3.\n";
            ofs << "2.\t1.\t2.\n";
            ofs << "2.\t3.\t2.\n";
            ofs.close();

            dictPoints->load("dictPointsTestFile");

            remove(output_file.c_str());
        }

        string output_file_neighbours = "dictTetrahedraTestFile_neighbours.dat";
        string output_file_vertexes = "dictTetrahedraTestFile_vertex.dat";

        ofstream neighbours_ofs(output_file_neighbours.c_str());
        ofstream vertexes_ofs(output_file_vertexes.c_str());

        if (neighbours_ofs.good())
        {
            neighbours_ofs << "4\n";
            neighbours_ofs << "4 -11 -18 1 2\n";
            neighbours_ofs << "4 -10 0 -19 3\n";
            neighbours_ofs << "4 0 -11 -18 3\n";
            neighbours_ofs << "4 1 -10 2 -19\n";
            neighbours_ofs.close();
        }

        if (vertexes_ofs.good())
        {
            vertexes_ofs << "4\n";
            vertexes_ofs << "4 4 5 2 0\n";
            vertexes_ofs << "4 4 1 5 0\n";
            vertexes_ofs << "4 3 4 5 2\n";
            vertexes_ofs << "4 3 4 1 5\n";
            vertexes_ofs.close();
        }

        dictTetrahedra->load("dictTetrahedraTestFile");
        remove("dictTetrahedraTestFile_vertex.dat");
        remove("dictTetrahedraTestFile_neighbours.dat");

        dictTetrahedra->setPointDictionary(dictPoints);
        ASSERT_EQ(dictTetrahedra->getEdgeDictionary()->getDataLength(), 13);
        ASSERT_EQ(dictTetrahedra->getDataLength(), 4);

        ASSERT_FLOAT_EQ(dictTetrahedra->getById(0).getLongestEdge(), sqrt(6));
        ASSERT_FLOAT_EQ(dictTetrahedra->getById(1).getLongestEdge(), sqrt(6));
        ASSERT_FLOAT_EQ(dictTetrahedra->getById(2).getLongestEdge(), sqrt(6));
        ASSERT_FLOAT_EQ(dictTetrahedra->getById(3).getLongestEdge(), sqrt(6));

        ASSERT_FLOAT_EQ(getR3(0, dictTetrahedra), (8 + sqrt(6) + sqrt(2)) / 6);
        float res = ((8 + sqrt(6) + sqrt(2)) / 6) + 2 * sqrt(2 * (18 - 4 * sqrt(2) - sqrt(3) - 4 * sqrt(6)) / 15);
        ASSERT_FLOAT_EQ(getR3(2, dictTetrahedra), res);
    }
}