#include "gtest/gtest.h"
#include "../src/EdgeVolumeDensity.hpp"
#include "../src/PointDictionary.hpp"
#include "../src/TetrahedronDictionary.hpp"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <vector>
#include <thread>
#include <cmath>

namespace EdgeVolumeDensityTest
{
    class EdgeVolumeDensityTest : public ::testing::Test
    {
    protected:
        EdgeVolumeDensityTest() {}
        virtual ~EdgeVolumeDensityTest() {}

        virtual void SetUp()
        {
            edgeDensity = new EdgeVolumeDensity();
            dictPoints = new PointDictionary();
            dictTetrahedra = new TetrahedronDictionary();
            string output_file = "dictPointsTestFile.dat";
            ofstream ofs(output_file.c_str());
            if (ofs.good())
            {
                ofs << "3\n";
                ofs << "6\n";
                ofs << "1.\t1.\t1.\n";
                ofs << "1.\t1.\t3.\n";
                ofs << "3.\t1.\t1.\n";
                ofs << "3.\t1.\t3.\n";
                ofs << "2.\t1.\t2.\n";
                ofs << "2.\t3.\t2.\n";
                ofs.close();

                dictPoints->load("dictPointsTestFile");

                remove(output_file.c_str());
            }

            string output_file_neighbours = "dictTetrahedraTestFile_neighbours.dat";
            string output_file_vertexes = "dictTetrahedraTestFile_vertex.dat";

            ofstream neighbours_ofs(output_file_neighbours.c_str());
            ofstream vertexes_ofs(output_file_vertexes.c_str());

            if (neighbours_ofs.good())
            {
                neighbours_ofs << "4\n";
                neighbours_ofs << "4 -11 -18 1 2\n";
                neighbours_ofs << "4 -10 0 -19 3\n";
                neighbours_ofs << "4 0 -11 -18 3\n";
                neighbours_ofs << "4 1 -10 2 -19\n";
                neighbours_ofs.close();
            }

            if (vertexes_ofs.good())
            {
                vertexes_ofs << "4\n";
                vertexes_ofs << "4 4 5 2 0\n";
                vertexes_ofs << "4 4 1 5 0\n";
                vertexes_ofs << "4 3 4 5 2\n";
                vertexes_ofs << "4 3 4 1 5\n";
                vertexes_ofs.close();
            }

            dictTetrahedra->load("dictTetrahedraTestFile");
            remove("dictTetrahedraTestFile_vertex.dat");
            remove("dictTetrahedraTestFile_neighbours.dat");

            dictTetrahedra->setPointDictionary(dictPoints);
        }

        virtual void TearDown()
        {
        }

        EdgeVolumeDensity *edgeDensity;
        PointDictionary *dictPoints;
        TetrahedronDictionary *dictTetrahedra;
    };

    TEST_F(EdgeVolumeDensityTest, getMeanDensity)
    {
        ASSERT_EQ(0, edgeDensity->getMeanDensity());
    }

    TEST_F(EdgeVolumeDensityTest, apply)
    {
        edgeDensity->apply(dictTetrahedra);
        ASSERT_NE(0, edgeDensity->getMeanDensity());
        ASSERT_FLOAT_EQ(12.375 / 13., edgeDensity->getMeanDensity());
        vector<pair<int, float>> vDensity = edgeDensity->getEdgeDensity();
        ASSERT_EQ(13, vDensity.size());
    }

    TEST_F(EdgeVolumeDensityTest, getEdgeDensity)
    {
        edgeDensity->apply(dictTetrahedra);
        float d[13] = {3. / 8., 3. / 4., 3. / 4., 3. / 4., 3. / 4., 3. / 2., 3. / 4., 3. / 4., 3. / 2., 3. / 4., 3. / 4., 3. / 2., 3. / 2.};
        vector<pair<int, float>> vDensity = edgeDensity->getEdgeDensity();
        for (int i = 0; i < 13; i++)
        {
            ASSERT_EQ(i, vDensity[i].first);
            ASSERT_EQ(d[i], vDensity[i].second);
        }
    }

    TEST_F(EdgeVolumeDensityTest, getEdgeIdDensity)
    {
        edgeDensity->apply(dictTetrahedra);
        ASSERT_FLOAT_EQ(3. / 4., edgeDensity->getEdgeIdDensity(4));
        ASSERT_FLOAT_EQ(3. / 8., edgeDensity->getEdgeIdDensity(0));
        ASSERT_FLOAT_EQ(3. / 4., edgeDensity->getEdgeIdDensity(1));
        ASSERT_FLOAT_EQ(3. / 4., edgeDensity->getEdgeIdDensity(10));
        ASSERT_FLOAT_EQ(3. / 2., edgeDensity->getEdgeIdDensity(5));
    }

    TEST_F(EdgeVolumeDensityTest, getSortedEdges)
    {
        edgeDensity->apply(dictTetrahedra);
        vector<pair<int, float>> sorted = edgeDensity->getSortedEdges();
        float d[13] = {3. / 8., 3. / 4., 3. / 4, 3. / 4, 3. / 4, 3. / 4, 3. / 4, 3. / 4, 3. / 4, 3. / 2., 3. / 2., 3. / 2., 3. / 2.};
        for (int i = 0; i < 13; i++)
        {
            ASSERT_EQ(d[i], sorted[i].second);
        }
    }

    TEST_F(EdgeVolumeDensityTest, getEdgeIdDensitySorted)
    {
        edgeDensity->apply(dictTetrahedra);
        edgeDensity->getSortedEdges();
        ASSERT_FLOAT_EQ(3. / 4., edgeDensity->getEdgeIdDensity(4));
        ASSERT_FLOAT_EQ(3. / 8., edgeDensity->getEdgeIdDensity(0));
        ASSERT_FLOAT_EQ(3. / 4., edgeDensity->getEdgeIdDensity(1));
        ASSERT_FLOAT_EQ(3. / 4., edgeDensity->getEdgeIdDensity(10));
        ASSERT_FLOAT_EQ(3. / 2., edgeDensity->getEdgeIdDensity(5));
    }

}