#include "gtest/gtest.h"
#include "../src/DensityBuilder.hpp"
#include "../src/PointDensityBuilder.hpp"
#include "../src/EdgeDensityBuilder.hpp"
#include "../src/TetrahedronDictionary.hpp"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>

namespace DensityBuilderTest
{
    class WrapDensityBuilder : public DensityBuilder
    {
    public:
        WrapDensityBuilder() : DensityBuilder()
        {
        }
        WrapDensityBuilder(float _thresholdPercent, bool _wallDensity, float _minVolume, float _maxVolume) : DensityBuilder(_thresholdPercent, _wallDensity, _minVolume, _maxVolume)
        {
        }
        void analyze(TetrahedronDictionary *tetrahedraDict) { return; }
        void wrap_addTetrahedraToVoid(TetrahedronDictionary *tetrahedraDict, vector<int> tetrahedraId, int voidId)
        {
            return addTetrahedraToVoid(tetrahedraDict, tetrahedraId, voidId);
        }
        void wrap_joinVoids(int firstVoid, int secondVoid)
        {
            return joinVoids(firstVoid, secondVoid);
        }
        void wrap_discardVoids(bool discardBorder, bool deletePoorQuality)
        {
            return discardVoids(discardBorder, deletePoorQuality);
        }
        int wrap_getBiggerNeighbourVoidId(TetrahedronDictionary *tetrahedraDict, Tetrahedron tetrahedron)
        {
            return getBiggerNeighbourVoidId(tetrahedraDict, tetrahedron);
        }
    };

    class DensityBuilderTest : public ::testing::Test
    {
    protected:
        DensityBuilderTest() {}
        virtual ~DensityBuilderTest() {}

        virtual void SetUp()
        {
            densBuilder = new WrapDensityBuilder();
            dictPoints = new PointDictionary();
            dictTetrahedra = new TetrahedronDictionary();
            string output_file = "dictPointsTestFile.dat";
            ofstream ofs(output_file.c_str());
            if (ofs.good())
            {
                ofs << "3\n";
                ofs << "6\n";
                ofs << "1.\t1.\t1.\n";
                ofs << "1.\t1.\t3.\n";
                ofs << "3.\t1.\t1.\n";
                ofs << "3.\t1.\t3.\n";
                ofs << "2.\t1.\t2.\n";
                ofs << "2.\t3.\t2.\n";
                ofs.close();

                dictPoints->load("dictPointsTestFile");
                remove(output_file.c_str());
            }

            string output_file_neighbours = "dictTetrahedraTestFile_neighbours.dat";
            string output_file_vertexes = "dictTetrahedraTestFile_vertex.dat";
            ofstream neighbours_ofs(output_file_neighbours.c_str());
            ofstream vertexes_ofs(output_file_vertexes.c_str());
            if (neighbours_ofs.good())
            {
                neighbours_ofs << "4\n";
                neighbours_ofs << "4 -11 -18 1 2\n";
                neighbours_ofs << "4 -10 0 -19 3\n";
                neighbours_ofs << "4 0 -11 -18 3\n";
                neighbours_ofs << "4 1 -10 2 -19\n";
                neighbours_ofs.close();
            }

            if (vertexes_ofs.good())
            {
                vertexes_ofs << "4\n";
                vertexes_ofs << "4 4 5 2 0\n";
                vertexes_ofs << "4 4 1 5 0\n";
                vertexes_ofs << "4 3 4 5 2\n";
                vertexes_ofs << "4 3 4 1 5\n";
                vertexes_ofs.close();
            }

            dictTetrahedra->load("dictTetrahedraTestFile");
            remove("dictTetrahedraTestFile_vertex.dat");
            remove("dictTetrahedraTestFile_neighbours.dat");

            dictTetrahedra->setPointDictionary(dictPoints);
        }
        virtual void TearDown()
        {
            // Code here will be called immediately after each test (right
            // before the destructor).
        }
        WrapDensityBuilder *densBuilder;
        TetrahedronDictionary *dictTetrahedra;
        PointDictionary *dictPoints;
    };

    TEST_F(DensityBuilderTest, initState)
    {
        ASSERT_EQ(0, densBuilder->getVoidsLength());
    }

    TEST_F(DensityBuilderTest, addVoid)
    {
        ASSERT_EQ(0, densBuilder->getVoidsLength());

        VoidSpace *voidSpace = new VoidSpace();
        voidSpace->setTetrahedronDictionary(dictTetrahedra);

        densBuilder->addVoid(voidSpace);
        ASSERT_EQ(1, densBuilder->getVoidsLength());
    }

    TEST_F(DensityBuilderTest, addTetrahedraToVoid)
    {
        vector<int> tetId = {0, 1};

        VoidSpace *voidSpace = new VoidSpace();
        voidSpace->setTetrahedronDictionary(dictTetrahedra);

        densBuilder->addVoid(voidSpace);
        ASSERT_EQ(0, voidSpace->getTetrahedraLength());

        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId, 0);
        ASSERT_EQ(2, voidSpace->getTetrahedraLength());
        vector<int> tetres = voidSpace->getTetrahedraId();
        ASSERT_EQ(0, tetres[0]);
        ASSERT_EQ(1, tetres[1]);
    }

    TEST_F(DensityBuilderTest, add2TetrahedraToVoid)
    {
        vector<int> tetId = {0, 1};
        vector<int> tetId2 = {1, 2};

        VoidSpace *voidSpace = new VoidSpace();
        voidSpace->setTetrahedronDictionary(dictTetrahedra);

        densBuilder->addVoid(voidSpace);
        ASSERT_EQ(0, voidSpace->getTetrahedraLength());

        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId, 0);
        ASSERT_EQ(2, voidSpace->getTetrahedraLength());

        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId2, 0);
        ASSERT_EQ(3, voidSpace->getTetrahedraLength());
        vector<int> tetres = voidSpace->getTetrahedraId();
        ASSERT_EQ(0, tetres[0]);
        ASSERT_EQ(1, tetres[1]);
        ASSERT_EQ(2, tetres[2]);
    }

    TEST_F(DensityBuilderTest, addTetrahedraToVoidFromOtherVoid)
    {
        vector<int> tetId1 = {0, 1};
        vector<int> tetId2 = {2, 3};
        VoidSpace *voidSpace1 = new VoidSpace();
        VoidSpace *voidSpace2 = new VoidSpace();
        voidSpace1->setTetrahedronDictionary(dictTetrahedra);
        voidSpace2->setTetrahedronDictionary(dictTetrahedra);

        densBuilder->addVoid(voidSpace1);
        densBuilder->addVoid(voidSpace2);
        ASSERT_EQ(2, densBuilder->getVoidsLength());
        ASSERT_EQ(0, voidSpace1->getTetrahedraLength());
        ASSERT_EQ(0, voidSpace2->getTetrahedraLength());
        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId1, 0);
        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId2, 1);
        ASSERT_EQ(2, voidSpace1->getTetrahedraLength());
        ASSERT_EQ(2, voidSpace2->getTetrahedraLength());
        vector<int> tetId = {1, 2};

        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId, 0);
        ASSERT_EQ(4, voidSpace1->getTetrahedraLength());
        ASSERT_EQ(0, voidSpace2->getTetrahedraLength());
    }

    TEST_F(DensityBuilderTest, joinVoids)
    {
        vector<int> tetId1 = {0, 1};
        vector<int> tetId2 = {2, 3};
        VoidSpace *voidSpace1 = new VoidSpace();
        VoidSpace *voidSpace2 = new VoidSpace();
        voidSpace1->setTetrahedronDictionary(dictTetrahedra);
        voidSpace2->setTetrahedronDictionary(dictTetrahedra);

        densBuilder->addVoid(voidSpace1);
        densBuilder->addVoid(voidSpace2);
        ASSERT_EQ(2, densBuilder->getVoidsLength());

        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId1, 0);
        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId2, 1);

        ASSERT_EQ(2, voidSpace1->getTetrahedraLength());
        ASSERT_EQ(0, voidSpace1->getId());
        ASSERT_EQ(2, voidSpace2->getTetrahedraLength());
        ASSERT_EQ(1, voidSpace2->getId());

        densBuilder->wrap_joinVoids(0, 1);
        ASSERT_EQ(4, voidSpace1->getTetrahedraLength());
        ASSERT_EQ(0, voidSpace1->getId());
        ASSERT_EQ(0, voidSpace2->getTetrahedraLength());
        ASSERT_EQ(-1, voidSpace2->getId());
    }

    TEST_F(DensityBuilderTest, getBiggerNeighbourVoidId)
    {
        vector<int> tetId1 = {1};    // to void 0
        vector<int> tetId2 = {2, 3}; // to void 1
        VoidSpace *voidSpace1 = new VoidSpace();
        VoidSpace *voidSpace2 = new VoidSpace();
        voidSpace1->setTetrahedronDictionary(dictTetrahedra);
        voidSpace2->setTetrahedronDictionary(dictTetrahedra);
        densBuilder->addVoid(voidSpace1);
        densBuilder->addVoid(voidSpace2);
        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId1, 0);
        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId2, 1);
        ASSERT_EQ(1, voidSpace1->getTetrahedraLength());
        ASSERT_EQ(2, voidSpace2->getTetrahedraLength());
        Tetrahedron t = dictTetrahedra->getById(0);
        ASSERT_EQ(1, densBuilder->wrap_getBiggerNeighbourVoidId(dictTetrahedra, t));
    }

    TEST_F(DensityBuilderTest, discardVoidsDeleteNothing)
    {
        vector<int> tetId1 = {1};    // to void 0
        vector<int> tetId2 = {2, 3}; // to void 1
        VoidSpace *voidSpace1 = new VoidSpace();
        VoidSpace *voidSpace2 = new VoidSpace();
        voidSpace1->setTetrahedronDictionary(dictTetrahedra);
        voidSpace2->setTetrahedronDictionary(dictTetrahedra);
        densBuilder->addVoid(voidSpace1);
        densBuilder->addVoid(voidSpace2);
        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId1, 0);
        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId2, 1);

        densBuilder->wrap_discardVoids(false, false);
        ASSERT_EQ(2, densBuilder->getVoidsLength());
    }

    TEST_F(DensityBuilderTest, discardVoidsDeleteBorder)
    {
        vector<int> tetId1 = {1};    // to void 0
        vector<int> tetId2 = {2, 3}; // to void 1
        VoidSpace *voidSpace1 = new VoidSpace();
        VoidSpace *voidSpace2 = new VoidSpace();
        voidSpace1->setTetrahedronDictionary(dictTetrahedra);
        voidSpace2->setTetrahedronDictionary(dictTetrahedra);
        densBuilder->addVoid(voidSpace1);
        densBuilder->addVoid(voidSpace2);
        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId1, 0);
        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId2, 1);

        densBuilder->wrap_discardVoids(true, false);
        ASSERT_EQ(0, densBuilder->getVoidsLength());
    }

    TEST_F(DensityBuilderTest, discardVoidsDeleteJustVolume)
    {
        WrapDensityBuilder *densBuilder = new WrapDensityBuilder(0.02, false, 1.0, 10000.0);
        vector<int> tetId1 = {1};    // to void 0
        vector<int> tetId2 = {2, 3}; // to void 1
        VoidSpace *voidSpace1 = new VoidSpace();
        VoidSpace *voidSpace2 = new VoidSpace();
        voidSpace1->setTetrahedronDictionary(dictTetrahedra);
        voidSpace2->setTetrahedronDictionary(dictTetrahedra);
        densBuilder->addVoid(voidSpace1);
        densBuilder->addVoid(voidSpace2);
        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId1, 0);
        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId2, 1);

        densBuilder->wrap_discardVoids(false, false);
        ASSERT_EQ(1, densBuilder->getVoidsLength());
    }

    TEST_F(DensityBuilderTest, discardVoidsEvalPoorQuality)
    {
        vector<int> tetId1 = {1};    // to void 0
        vector<int> tetId2 = {2, 3}; // to void 1
        VoidSpace *voidSpace1 = new VoidSpace();
        VoidSpace *voidSpace2 = new VoidSpace();
        voidSpace1->setTetrahedronDictionary(dictTetrahedra);
        voidSpace2->setTetrahedronDictionary(dictTetrahedra);
        densBuilder->addVoid(voidSpace1);
        densBuilder->addVoid(voidSpace2);
        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId1, 0);
        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId2, 1);

        densBuilder->wrap_discardVoids(false, true);
        ASSERT_EQ(2, densBuilder->getVoidsLength());
        ASSERT_EQ(1, voidSpace1->getTetrahedraLength());
        ASSERT_EQ(2, voidSpace2->getTetrahedraLength());
    }

    TEST_F(DensityBuilderTest, getVoid)
    {
        vector<int> tetId1 = {1};    // to void 0
        vector<int> tetId2 = {2, 3}; // to void 1
        VoidSpace *voidSpace1 = new VoidSpace();
        VoidSpace *voidSpace2 = new VoidSpace();
        voidSpace1->setTetrahedronDictionary(dictTetrahedra);
        voidSpace2->setTetrahedronDictionary(dictTetrahedra);
        densBuilder->addVoid(voidSpace1);
        densBuilder->addVoid(voidSpace2);
        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId1, 0);
        densBuilder->wrap_addTetrahedraToVoid(dictTetrahedra, tetId2, 1);

        VoidSpace *getVoid = densBuilder->getVoid(1);
        ASSERT_EQ(1, getVoid->getId());
        ASSERT_FLOAT_EQ(4. / 3., getVoid->getVolume());
        ASSERT_EQ(voidSpace2, getVoid);
    }
}