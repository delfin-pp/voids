#include "gtest/gtest.h"
#include "../src/TetrahedronDictionary.hpp"
#include "../src/VoidContainer.hpp"

namespace VoidContainerTest
{
    class VoidContainerTest : public ::testing::Test
    {
    protected:
        VoidContainerTest() {}
        virtual ~VoidContainerTest() {}

        virtual void SetUp()
        {
            voidContainer = new VoidContainer();
            dictPoints = new PointDictionary();
            dictTetrahedra = new TetrahedronDictionary();
            string output_file = "dictPointsTestFile.dat";
            ofstream ofs(output_file.c_str());
            if (ofs.good())
            {
                ofs << "3\n";
                ofs << "6\n";
                ofs << "1.\t1.\t1.\n";
                ofs << "1.\t1.\t3.\n";
                ofs << "3.\t1.\t1.\n";
                ofs << "3.\t1.\t3.\n";
                ofs << "2.\t1.\t2.\n";
                ofs << "2.\t3.\t2.\n";
                ofs.close();

                dictPoints->load("dictPointsTestFile");
                remove(output_file.c_str());
            }

            string output_file_neighbours = "dictTetrahedraTestFile_neighbours.dat";
            string output_file_vertexes = "dictTetrahedraTestFile_vertex.dat";
            ofstream neighbours_ofs(output_file_neighbours.c_str());
            ofstream vertexes_ofs(output_file_vertexes.c_str());
            if (neighbours_ofs.good())
            {
                neighbours_ofs << "4\n";
                neighbours_ofs << "4 -11 -18 1 2\n";
                neighbours_ofs << "4 -10 0 -19 3\n";
                neighbours_ofs << "4 0 -11 -18 3\n";
                neighbours_ofs << "4 1 -10 2 -19\n";
                neighbours_ofs.close();
            }

            if (vertexes_ofs.good())
            {
                vertexes_ofs << "4\n";
                vertexes_ofs << "4 4 5 2 0\n";
                vertexes_ofs << "4 4 1 5 0\n";
                vertexes_ofs << "4 3 4 5 2\n";
                vertexes_ofs << "4 3 4 1 5\n";
                vertexes_ofs.close();
            }

            dictTetrahedra->load("dictTetrahedraTestFile");
            remove("dictTetrahedraTestFile_vertex.dat");
            remove("dictTetrahedraTestFile_neighbours.dat");

            dictTetrahedra->setPointDictionary(dictPoints);
        }

        virtual void TearDown()
        {
            // Code here will be called immediately after each test (right
            // before the destructor).
        }
        // Objects declared here can be used by all tests in the test case for Foo.
        VoidContainer *voidContainer;
        TetrahedronDictionary *dictTetrahedra;
        PointDictionary *dictPoints;
    };

    TEST_F(VoidContainerTest, initState)
    {
        ASSERT_EQ(0, voidContainer->getNextId());
        ASSERT_EQ(0, voidContainer->getDataLength());
    }

    TEST_F(VoidContainerTest, tetrahedronDict)
    {
        ASSERT_NE(dictTetrahedra, voidContainer->getTetrahedronDictionary());
        voidContainer->setTetrahedronDictionary(dictTetrahedra);
        ASSERT_EQ(dictTetrahedra, voidContainer->getTetrahedronDictionary());
    }

    TEST_F(VoidContainerTest, addVoid)
    {
        voidContainer->setTetrahedronDictionary(dictTetrahedra);
        vector<int> tetId = {0, 2};
        int nv = voidContainer->addVoid(tetId);
        ASSERT_EQ(0, nv);
        ASSERT_EQ(1, voidContainer->getNextId());
        ASSERT_EQ(1, voidContainer->getDataLength());
        vector<int> tetId2 = {1, 3};
        nv = voidContainer->addVoid(tetId2);
        ASSERT_EQ(1, nv);
        ASSERT_EQ(2, voidContainer->getNextId());
        ASSERT_EQ(2, voidContainer->getDataLength());
    }

    TEST_F(VoidContainerTest, deleteVoid)
    {
        voidContainer->setTetrahedronDictionary(dictTetrahedra);
        vector<int> tetId = {0, 2};
        vector<int> tetId2 = {1, 3};
        voidContainer->addVoid(tetId);
        voidContainer->addVoid(tetId2);
        ASSERT_EQ(2, voidContainer->getNextId());
        ASSERT_EQ(2, voidContainer->getDataLength());
        voidContainer->deleteVoid(1);
        ASSERT_EQ(2, voidContainer->getNextId());
        ASSERT_EQ(1, voidContainer->getDataLength());
    }

    TEST_F(VoidContainerTest, getById)
    {
        voidContainer->setTetrahedronDictionary(dictTetrahedra);
        vector<int> tetId = {0, 2};
        vector<int> tetId2 = {1, 3};
        voidContainer->addVoid(tetId);
        voidContainer->addVoid(tetId2);
        ASSERT_EQ(2, voidContainer->getNextId());
        ASSERT_EQ(2, voidContainer->getDataLength());

        VoidSpace voidSpace = voidContainer->getById(1);

        ASSERT_EQ(1, voidSpace.getId());
        vector<int> vid = voidSpace.getTetrahedraId();
        ASSERT_EQ(2, vid.size());
        ASSERT_EQ(1, vid[0]);
        ASSERT_EQ(3, vid[1]);
    }
}