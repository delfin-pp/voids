#include "gtest/gtest.h"
#include "../src/Edge.hpp"
#include "../src/EdgeDictionary.hpp"

namespace EdgeDictionaryTest
{
    class EdgeDictionaryTest : public ::testing::Test
    {
    protected:
        EdgeDictionaryTest() {}
        virtual ~EdgeDictionaryTest() {}

        virtual void SetUp()
        {
            edges = new EdgeDictionary();
            points = new PointDictionary();
        }

        virtual void TearDown()
        {
        }
        EdgeDictionary *edges;
        PointDictionary *points;
    };

    TEST_F(EdgeDictionaryTest, newDictionary)
    {
        int res = edges->getDataLength();
        ASSERT_EQ(0, res);
    }

    TEST_F(EdgeDictionaryTest, addEdge)
    {
        Edge *edge = new Edge();
        int res = edges->add(edge);
        ASSERT_EQ(0, res);
    }

    TEST_F(EdgeDictionaryTest, addEdgeTetrahedron)
    {
        int res = edges->add(2, 4, 1);
        ASSERT_EQ(0, res);
        res = edges->add(7, 5, 2);
        ASSERT_EQ(1, res);
        res = edges->add(4, 2, 3);
        ASSERT_EQ(0, res);
        ASSERT_EQ(2, edges->getDataLength());
    }

    TEST_F(EdgeDictionaryTest, getById)
    {
        edges->add(2, 4, 1);
        edges->add(7, 5, 2);
        Edge e = edges->getById(1);
        int *p = e.getPointsId();

        ASSERT_EQ(1, e.getId());
        ASSERT_EQ(5, p[0]);
        ASSERT_EQ(7, p[1]);
        ASSERT_EQ(2, e.getTetrahedraId()[0]);
        ASSERT_EQ(0, e.isInBorder());
    }

    TEST_F(EdgeDictionaryTest, setInBorder)
    {
        edges->add(2, 4, 1);
        edges->add(7, 5, 2);
        Edge e = edges->getById(1);
        ASSERT_EQ(0, e.isInBorder());

        edges->setInBorder(1, 1);
        e = edges->getById(1);
        ASSERT_EQ(1, e.isInBorder());
    }

    TEST_F(EdgeDictionaryTest, mockLoad)
    {
        int res = edges->getDataLength();
        ASSERT_EQ(0, res);
        edges->load("test");
        res = edges->getDataLength();
        ASSERT_EQ(0, res);
    }
}