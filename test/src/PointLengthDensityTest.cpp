#include "gtest/gtest.h"
#include "../src/PointLengthDensity.hpp"
#include "../src/PointDictionary.hpp"
#include "../src/TetrahedronDictionary.hpp"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <vector>
#include <thread>
#include <cmath>

namespace PointLengthDensityTest
{
    class PointLengthDensityTest : public ::testing::Test
    {
    protected:
        PointLengthDensityTest() {}
        virtual ~PointLengthDensityTest() {}

        virtual void SetUp()
        {
            pointDensity = new PointLengthDensity();
            dictPoints = new PointDictionary();
            dictTetrahedra = new TetrahedronDictionary();
            string output_file = "dictPointsTestFile.dat";
            ofstream ofs(output_file.c_str());
            if (ofs.good())
            {
                ofs << "3\n";
                ofs << "6\n";
                ofs << "1.\t1.\t1.\n";
                ofs << "1.\t1.\t3.\n";
                ofs << "3.\t1.\t1.\n";
                ofs << "3.\t1.\t3.\n";
                ofs << "2.\t1.\t2.\n";
                ofs << "2.\t3.\t2.\n";
                ofs.close();

                dictPoints->load("dictPointsTestFile");

                remove(output_file.c_str());
            }

            string output_file_neighbours = "dictTetrahedraTestFile_neighbours.dat";
            string output_file_vertexes = "dictTetrahedraTestFile_vertex.dat";

            ofstream neighbours_ofs(output_file_neighbours.c_str());
            ofstream vertexes_ofs(output_file_vertexes.c_str());

            if (neighbours_ofs.good())
            {
                neighbours_ofs << "4\n";
                neighbours_ofs << "4 -11 -18 1 2\n";
                neighbours_ofs << "4 -10 0 -19 3\n";
                neighbours_ofs << "4 0 -11 -18 3\n";
                neighbours_ofs << "4 1 -10 2 -19\n";
                neighbours_ofs.close();
            }

            if (vertexes_ofs.good())
            {
                vertexes_ofs << "4\n";
                vertexes_ofs << "4 4 5 2 0\n";
                vertexes_ofs << "4 4 1 5 0\n";
                vertexes_ofs << "4 3 4 5 2\n";
                vertexes_ofs << "4 3 4 1 5\n";
                vertexes_ofs.close();
            }

            dictTetrahedra->load("dictTetrahedraTestFile");
            remove("dictTetrahedraTestFile_vertex.dat");
            remove("dictTetrahedraTestFile_neighbours.dat");

            dictTetrahedra->setPointDictionary(dictPoints);
        }

        virtual void TearDown()
        {
        }

        PointDictionary *dictPoints;
        TetrahedronDictionary *dictTetrahedra;
        PointLengthDensity *pointDensity;
    };

    TEST_F(PointLengthDensityTest, getMeanDensity)
    {
        ASSERT_EQ(0, pointDensity->getMeanDensity());
    }

    TEST_F(PointLengthDensityTest, apply)
    {
        pointDensity->apply(dictTetrahedra);
        ASSERT_EQ(4, dictTetrahedra->getDataLength());
        float sum = 4 * (1. / (4. + sqrt(6) + sqrt(2))) + 1. / (2. + 4 * sqrt(6)) + 1. / (2. + 4 * sqrt(2));
        ASSERT_FLOAT_EQ(sum / 6, pointDensity->getMeanDensity());
        vector<pair<int, float>> vDensity = pointDensity->getPointDensity();
        ASSERT_EQ(6, vDensity.size());
    }

    TEST_F(PointLengthDensityTest, getSortedPoints)
    {
        pointDensity->apply(dictTetrahedra);
        vector<pair<int, float>> sorted = pointDensity->getSortedPoints();
        float d[6] = {1. / (2. + 4 * sqrt(6)),
                      1. / (4. + sqrt(6) + sqrt(2)),
                      1. / (4. + sqrt(6) + sqrt(2)),
                      1. / (4. + sqrt(6) + sqrt(2)),
                      1. / (4. + sqrt(6) + sqrt(2)),
                      1. / (2. + 4 * sqrt(2))};
        for (int i = 0; i < 6; i++)
        {
            ASSERT_FLOAT_EQ(d[i], sorted[i].second);
        }
    }

    TEST_F(PointLengthDensityTest, getPointDensity)
    {
        pointDensity->apply(dictTetrahedra);
        vector<pair<int, float>> p = pointDensity->getPointDensity();
        float d[6] = {1. / (4. + sqrt(6) + sqrt(2)),
                      1. / (4. + sqrt(6) + sqrt(2)),
                      1. / (4. + sqrt(6) + sqrt(2)),
                      1. / (4. + sqrt(6) + sqrt(2)),
                      1. / (2. + 4 * sqrt(2)),
                      1. / (2. + 4 * sqrt(6))};
        for (int i = 0; i < 6; i++)
        {
            ASSERT_EQ(i, p[i].first);
            ASSERT_FLOAT_EQ(d[i], p[i].second);
        }
    }

    TEST_F(PointLengthDensityTest, getPointIdDensity)
    {
        pointDensity->apply(dictTetrahedra);
        ASSERT_FLOAT_EQ(1. / (2. + 4 * sqrt(2)), pointDensity->getPointIdDensity(4));
        ASSERT_FLOAT_EQ(1. / (4. + sqrt(6) + sqrt(2)), pointDensity->getPointIdDensity(0));
        ASSERT_FLOAT_EQ(1. / (4. + sqrt(6) + sqrt(2)), pointDensity->getPointIdDensity(1));
        ASSERT_FLOAT_EQ(1. / (2. + 4 * sqrt(6)), pointDensity->getPointIdDensity(5));
    }

    TEST_F(PointLengthDensityTest, getPointIdDensitySorted)
    {
        pointDensity->apply(dictTetrahedra);
        pointDensity->getSortedPoints();
        ASSERT_FLOAT_EQ(1. / (2. + 4 * sqrt(2)), pointDensity->getPointIdDensity(4));
        ASSERT_FLOAT_EQ(1. / (4. + sqrt(6) + sqrt(2)), pointDensity->getPointIdDensity(0));
        ASSERT_FLOAT_EQ(1. / (4. + sqrt(6) + sqrt(2)), pointDensity->getPointIdDensity(1));
        ASSERT_FLOAT_EQ(1. / (2. + 4 * sqrt(6)), pointDensity->getPointIdDensity(5));
    }

    TEST_F(PointLengthDensityTest, getPointIdTetrahedra)
    {
        pointDensity->apply(dictTetrahedra);
        vector<vector<int>> t = {{0, 1}, {1, 3}, {0, 2}, {2, 3}, {0, 1, 2, 3}, {0, 1, 2, 3}};

        for (int i = 0; i < 4; i++)
        {
            ASSERT_EQ(2, pointDensity->getPointIdTetrahedra(i).size());
            ASSERT_EQ(t[i][0], pointDensity->getPointIdTetrahedra(i)[0]);
            ASSERT_EQ(t[i][1], pointDensity->getPointIdTetrahedra(i)[1]);
        }

        for (int i = 4; i < 6; i++)
        {
            ASSERT_EQ(4, pointDensity->getPointIdTetrahedra(i).size());
            ASSERT_EQ(t[i][0], pointDensity->getPointIdTetrahedra(i)[0]);
            ASSERT_EQ(t[i][1], pointDensity->getPointIdTetrahedra(i)[1]);
            ASSERT_EQ(t[i][2], pointDensity->getPointIdTetrahedra(i)[2]);
            ASSERT_EQ(t[i][3], pointDensity->getPointIdTetrahedra(i)[3]);
        }
    }

}