#include "gtest/gtest.h"
#include "../src/TetrahedronDictionary.hpp"
#include "../src/EdgeJoiner.hpp"
#include "../src/VoidSpace.hpp"

namespace EdgeJoinerTest
{
    class EdgeJoinerTest : public ::testing::Test
    {
    protected:
        EdgeJoinerTest() {}
        virtual ~EdgeJoinerTest() {}

        virtual void SetUp()
        {
            dictPoints = new PointDictionary();
            dictTetrahedra = new TetrahedronDictionary();
            string output_file = "dictPointsTestFile.dat";
            ofstream ofs(output_file.c_str());
            if (ofs.good())
            {
                ofs << "3\n";
                ofs << "6\n";
                ofs << "1.\t1.\t1.\n";
                ofs << "1.\t1.\t3.\n";
                ofs << "3.\t1.\t1.\n";
                ofs << "3.\t1.\t3.\n";
                ofs << "2.\t1.\t2.\n";
                ofs << "2.\t3.\t2.\n";
                ofs.close();

                dictPoints->load("dictPointsTestFile");
                remove(output_file.c_str());
            }

            string output_file_neighbours = "dictTetrahedraTestFile_neighbours.dat";
            string output_file_vertexes = "dictTetrahedraTestFile_vertex.dat";
            ofstream neighbours_ofs(output_file_neighbours.c_str());
            ofstream vertexes_ofs(output_file_vertexes.c_str());
            if (neighbours_ofs.good())
            {
                neighbours_ofs << "4\n";
                neighbours_ofs << "4 -11 -18 1 2\n";
                neighbours_ofs << "4 -10 0 -19 3\n";
                neighbours_ofs << "4 0 -11 -18 3\n";
                neighbours_ofs << "4 1 -10 2 -19\n";
                neighbours_ofs.close();
            }

            if (vertexes_ofs.good())
            {
                vertexes_ofs << "4\n";
                vertexes_ofs << "4 4 5 2 0\n";
                vertexes_ofs << "4 4 1 5 0\n";
                vertexes_ofs << "4 3 4 5 2\n";
                vertexes_ofs << "4 3 4 1 5\n";
                vertexes_ofs.close();
            }

            dictTetrahedra->load("dictTetrahedraTestFile");
            remove("dictTetrahedraTestFile_vertex.dat");
            remove("dictTetrahedraTestFile_neighbours.dat");

            dictTetrahedra->setPointDictionary(dictPoints);
        }

        virtual void TearDown()
        {
            // Code here will be called immediately after each test (right
            // before the destructor).
        }
        EdgeJoiner *joiner;
        TetrahedronDictionary *dictTetrahedra;
        PointDictionary *dictPoints;
    };

    TEST_F(EdgeJoinerTest, result)
    {
        joiner = new EdgeJoiner(5.0, false);

        vector<int> a = {0};
        vector<int> b = {1};
        vector<int> c = {2};
        vector<int> d = {3};
        vector<vector<int>> subvoids = {a, b, c, d};

        dictTetrahedra->setTetrahedronVoidId(0, 0);
        dictTetrahedra->setTetrahedronVoidId(1, 1);
        dictTetrahedra->setTetrahedronVoidId(2, 2);
        dictTetrahedra->setTetrahedronVoidId(3, 3);

        joiner->join(subvoids, dictTetrahedra);

        vector<vector<int>> result = joiner->getResult();

        ASSERT_EQ(4, result.size());
    }

    TEST_F(EdgeJoinerTest, resultDeleteBorder)
    {
        joiner = new EdgeJoiner(0.0, true);

        vector<int> a = {0};
        vector<int> b = {1};
        vector<int> c = {2};
        vector<int> d = {3};
        vector<vector<int>> subvoids = {a, b, c, d};

        dictTetrahedra->setTetrahedronVoidId(0, 0);
        dictTetrahedra->setTetrahedronVoidId(1, 1);
        dictTetrahedra->setTetrahedronVoidId(2, 2);
        dictTetrahedra->setTetrahedronVoidId(3, 3);

        joiner->join(subvoids, dictTetrahedra);

        vector<vector<int>> result = joiner->getResult();

        ASSERT_EQ(0, result.size());
    }

    TEST_F(EdgeJoinerTest, resultOneVoid)
    {
        joiner = new EdgeJoiner(1.5, false);

        vector<int> a = {0};
        vector<int> b = {1};
        vector<int> c = {2};
        vector<int> d = {3};
        vector<vector<int>> subvoids = {a, b, c, d};

        dictTetrahedra->setTetrahedronVoidId(0, 0);
        dictTetrahedra->setTetrahedronVoidId(1, 1);
        dictTetrahedra->setTetrahedronVoidId(2, 2);
        dictTetrahedra->setTetrahedronVoidId(3, 3);

        joiner->join(subvoids, dictTetrahedra);

        vector<vector<int>> result = joiner->getResult();

        ASSERT_EQ(1, result.size());
    }

    TEST_F(EdgeJoinerTest, voids)
    {
        joiner = new EdgeJoiner(5.0, false);

        vector<int> a = {0};
        vector<int> b = {1};
        vector<int> c = {2};
        vector<int> d = {3};
        vector<vector<int>> subvoids = {a, b, c, d};

        dictTetrahedra->setTetrahedronVoidId(0, 0);
        dictTetrahedra->setTetrahedronVoidId(1, 1);
        dictTetrahedra->setTetrahedronVoidId(2, 2);
        dictTetrahedra->setTetrahedronVoidId(3, 3);

        joiner->join(subvoids, dictTetrahedra);

        vector<VoidSpace *> voids = joiner->getVoids(dictTetrahedra);

        ASSERT_EQ(4, voids.size());
        ASSERT_EQ(0, voids[0]->getId());
        ASSERT_EQ(1, voids[1]->getId());
        ASSERT_EQ(2, voids[2]->getId());
        ASSERT_EQ(3, voids[3]->getId());

        ASSERT_EQ(1, voids[0]->getTetrahedraLength());
        ASSERT_EQ(1, voids[1]->getTetrahedraLength());
        ASSERT_EQ(1, voids[2]->getTetrahedraLength());
        ASSERT_EQ(1, voids[3]->getTetrahedraLength());
    }
}