#include "../src/Tetrahedron.hpp"
#include "../src/PointDictionary.hpp"
#include "../src/EdgeDictionary.hpp"
#include "../src/Edge.hpp"
#include "gtest/gtest.h"
#include <stdlib.h>
#include <vector>
#include <thread>
#include <cmath>
#include <fstream>

namespace TetrahedronTest
{
    class TetrahedronTest : public ::testing::Test
    {
    protected:
        TetrahedronTest() {}
        virtual ~TetrahedronTest() {}

        virtual void SetUp()
        {
            // Set up variables for test
            int points[4] = {34, 56, 12, 78};
            int neighbours[4] = {0, 41, 2, -9};
            int edges[6] = {0, 1, 2, 3, 4, 5};
            tetrahedron = new Tetrahedron(1, points, neighbours);
            tetrahedron->setEdgesId(edges);

            dictPoints = new PointDictionary();
            string output_file = "dictPointsTestFile.dat";
            ofstream ofs(output_file.c_str());
            if (ofs.good())
            {
                ofs << "3\n";
                ofs << "4\n";
                ofs << "0.0\t0.0\t0.0\n";
                ofs << "2.0\t0.0\t0.0\n";
                ofs << "0.0\t0.0\t2.0\n";
                ofs << "0.0\t4.0\t0.0\n";
                ofs.close();

                dictPoints->load("dictPointsTestFile");
                remove(output_file.c_str());
            }
        }
        virtual void TearDown()
        {
            // Code here will be called immediately after each test (right
            // before the destructor).
        }
        // Objects declared here can be used by all tests in the test case for Foo.
        Tetrahedron *tetrahedron;
        PointDictionary *dictPoints;
    };

    // Tests that the Foo::Bar() method does Abc.
    TEST_F(TetrahedronTest, getId)
    {
        ASSERT_EQ(1, tetrahedron->getId());
    }
    TEST_F(TetrahedronTest, setId)
    {
        tetrahedron->setId(4);
        ASSERT_EQ(4, tetrahedron->getId());
    }
    TEST_F(TetrahedronTest, getPointsId)
    {
        int aux[4] = {34, 56, 12, 78};
        int *result = tetrahedron->getPointsId();
        for (int it = 0; it < 4; it++)
        {
            ASSERT_EQ(aux[it], result[it]);
        }
    }
    TEST_F(TetrahedronTest, setPointsId)
    {
        int aux[4] = {58, 90, 102, 3};
        tetrahedron->setPointsId(aux);
        int *result = tetrahedron->getPointsId();
        for (int it = 0; it < 4; it++)
        {
            ASSERT_EQ(aux[it], result[it]);
        }
    }
    TEST_F(TetrahedronTest, getNeighboursId)
    {
        int aux[4] = {0, 41, 2, -9};
        int *result = tetrahedron->getNeighboursId();
        for (int it = 0; it < 4; it++)
        {
            ASSERT_EQ(aux[it], result[it]);
        }
    }
    TEST_F(TetrahedronTest, setNeighboursId)
    {
        int aux[4] = {-4, 51, 72, -10};
        tetrahedron->setNeighboursId(aux);
        int *result = tetrahedron->getNeighboursId();
        for (int it = 0; it < 4; it++)
        {
            ASSERT_EQ(aux[it], result[it]);
        }
    }

    TEST_F(TetrahedronTest, getEdgesId)
    {
        int aux[6] = {0, 1, 2, 3, 4, 5};
        int *result = tetrahedron->getEdgesId();
        for (int it = 0; it < 4; it++)
        {
            ASSERT_EQ(aux[it], result[it]);
        }
    }
    TEST_F(TetrahedronTest, setEdgesId)
    {
        int aux[6] = {6, 9, 8, 11, 23, 34};
        tetrahedron->setEdgesId(aux);
        int *result = tetrahedron->getEdgesId();
        for (int it = 0; it < 4; it++)
        {
            ASSERT_EQ(aux[it], result[it]);
        }
    }

    TEST_F(TetrahedronTest, getLongestEdge)
    {
        int aux[4] = {0, 1, 2, 3};
        tetrahedron->setPointDictionary(dictPoints);
        tetrahedron->setPointsId(aux);

        EdgeDictionary *edges = new EdgeDictionary();
        edges->setPointDictionary(dictPoints);
        edges->add(0, 1, 1);
        edges->add(0, 2, 1);
        edges->add(0, 3, 1);
        edges->add(1, 2, 1);
        edges->add(1, 3, 1);
        edges->add(2, 3, 1);

        tetrahedron->setEdgeDictionary(edges);

        ASSERT_FLOAT_EQ(sqrt(20), tetrahedron->getLongestEdge());
    }

    TEST_F(TetrahedronTest, getVolume)
    {
        int aux[4] = {0, 1, 2, 3};
        tetrahedron->setPointDictionary(dictPoints);
        tetrahedron->setPointsId(aux);
        ASSERT_FLOAT_EQ(8. / 3., tetrahedron->getVolume());
    }

    TEST_F(TetrahedronTest, getCentroid)
    {
        int aux[4] = {0, 1, 2, 3};
        tetrahedron->setPointDictionary(dictPoints);
        tetrahedron->setPointsId(aux);
        float *result = tetrahedron->getCentroid();
        float c[3] = {.5, 1., .5};
        for (int it = 0; it < 3; it++)
        {
            ASSERT_EQ(c[it], result[it]);
        }
    }

    TEST_F(TetrahedronTest, isInBorder)
    {
        ASSERT_EQ(1, tetrahedron->isInBorder());
    }

    TEST_F(TetrahedronTest, reset)
    {
        int aux[4] = {0, 1, 2, 3};
        tetrahedron->setPointDictionary(dictPoints);
        tetrahedron->setPointsId(aux);
        ASSERT_FLOAT_EQ(8. / 3., tetrahedron->getVolume());
        tetrahedron->reset();
        ASSERT_FLOAT_EQ(8. / 3., tetrahedron->getVolume());
    }

    TEST_F(TetrahedronTest, getVoidId)
    {
        ASSERT_EQ(-1, tetrahedron->getVoidId());
    }

    TEST_F(TetrahedronTest, setVoidId)
    {
        tetrahedron->setVoidId(3);
        ASSERT_EQ(3, tetrahedron->getVoidId());
    }

    TEST_F(TetrahedronTest, isSliver)
    {
        int aux[4] = {0, 1, 2, 3};
        tetrahedron->setPointDictionary(dictPoints);
        tetrahedron->setPointsId(aux);
        ASSERT_FALSE(tetrahedron->isSliver());
    }

    TEST_F(TetrahedronTest, getARG)
    {
        // equilateral tetrahedra
        float O[4] = {0.0, 0.0, 0.0};
        float B[4] = {0.0, 3.0, 0.0};
        float A[4] = {3.0 * sqrt(3.0) / 2.0, 3.0 / 2.0, 0.0};
        float C[4] = {3.0 * sqrt(3.0) / 6.0, 3.0 / 2.0, 3.0 * sqrt(6.0) / 3.0};
        PointDictionary *pd = new PointDictionary();
        EdgeDictionary *ed = new EdgeDictionary();
        ed->setPointDictionary(pd);
        ed->add(0, 1, 2);
        ed->add(0, 2, 2);
        ed->add(0, 3, 2);
        ed->add(1, 2, 2);
        ed->add(1, 3, 2);
        ed->add(2, 3, 2);

        pd->add(O);
        pd->add(A);
        pd->add(B);
        pd->add(C);
        int tp[4] = {0, 1, 2, 3};
        int tn[4] = {-1, -2, -3, -4};
        int te[6] = {0, 1, 2, 3, 4, 5};
        Tetrahedron *t = new Tetrahedron(2, tp, tn);
        t->setPointDictionary(pd);
        t->setEdgeDictionary(ed);
        t->setEdgesId(te);
        float ARG = t->getARG();
        ASSERT_FLOAT_EQ(1, ARG);
    }

    TEST_F(TetrahedronTest, getSliverARG)
    {
        // sliverish tetrahedra
        float O[4] = {-2.26, 0.0, 0.0};
        float B[4] = {0.0, -6.0 / 57.0, 0.0};
        float A[4] = {3.4, 0.0, 0.0};
        float C[4] = {0.0, 4.0, 0.5};

        PointDictionary *pd = new PointDictionary();
        EdgeDictionary *ed = new EdgeDictionary();
        ed->setPointDictionary(pd);
        ed->add(0, 1, 2);
        ed->add(0, 2, 2);
        ed->add(0, 3, 2);
        ed->add(1, 2, 2);
        ed->add(1, 3, 2);
        ed->add(2, 3, 2);

        pd->add(O);
        pd->add(A);
        pd->add(B);
        pd->add(C);
        int tp[4] = {0, 1, 2, 3};
        int tn[4] = {-1, -2, -3, -4};
        int te[6] = {0, 1, 2, 3, 4, 5};
        Tetrahedron *t = new Tetrahedron(2, tp, tn);
        t->setPointDictionary(pd);
        t->setEdgeDictionary(ed);
        t->setEdgesId(te);
        float ARG = t->getARG();
        ASSERT_GT(ARG, 3);
    }
}
