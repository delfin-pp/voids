#include "gtest/gtest.h"
#include "../src/Tetrahedron.hpp"
#include "../src/TetrahedronDictionary.hpp"

namespace TetrahedronDictionaryTest
{
    class TetrahedronDictionaryTest : public ::testing::Test
    {
    protected:
        TetrahedronDictionaryTest() {}
        virtual ~TetrahedronDictionaryTest() {}

        virtual void SetUp()
        {
            dictPoints = new PointDictionary();
            dictTetrahedra = new TetrahedronDictionary();

            string output_file = "dictPointsTestFile.dat";
            ofstream ofs(output_file.c_str());
            if (ofs.good())
            {
                ofs << "3\n";
                ofs << "6\n";
                ofs << "1.\t1.\t1.\n";
                ofs << "1.\t1.\t3.\n";
                ofs << "3.\t1.\t1.\n";
                ofs << "3.\t1.\t3.\n";
                ofs << "2.\t1.\t2.\n";
                ofs << "2.\t3.\t2.\n";
                ofs.close();

                dictPoints->load("dictPointsTestFile");
                remove(output_file.c_str());
            }
        }

        virtual void TearDown()
        {
        }

        TetrahedronDictionary *dictTetrahedra;
        PointDictionary *dictPoints;
    };

    TEST_F(TetrahedronDictionaryTest, initState)
    {
        ASSERT_EQ(0, dictTetrahedra->getDataLength());
    }

    TEST_F(TetrahedronDictionaryTest, pointDictionary)
    {
        ASSERT_NE(dictPoints, dictTetrahedra->getPointDictionary());
        dictTetrahedra->setPointDictionary(dictPoints);
        ASSERT_EQ(dictPoints, dictTetrahedra->getPointDictionary());
    }

    TEST_F(TetrahedronDictionaryTest, add)
    {
        // add doesn't do anything
        Tetrahedron *tetra = new Tetrahedron();
        ASSERT_EQ(0, dictTetrahedra->add(tetra));
        ASSERT_EQ(0, dictTetrahedra->getDataLength());
    }

    TEST_F(TetrahedronDictionaryTest, goodFullLoad)
    {
        string output_file_neighbours = "dictTetrahedraTestFile_neighbours.dat";
        string output_file_vertexes = "dictTetrahedraTestFile_vertex.dat";
        ofstream neighbours_ofs(output_file_neighbours.c_str());
        ofstream vertexes_ofs(output_file_vertexes.c_str());
        if (neighbours_ofs.good())
        {
            neighbours_ofs << "4\n";
            neighbours_ofs << "4 -11 -18 1 2\n";
            neighbours_ofs << "4 -10 0 -19 3\n";
            neighbours_ofs << "4 0 -11 -18 3\n";
            neighbours_ofs << "4 1 -10 2 -19\n";
            neighbours_ofs.close();
        }

        if (vertexes_ofs.good())
        {
            vertexes_ofs << "4\n";
            vertexes_ofs << "4 4 5 2 0\n";
            vertexes_ofs << "4 4 1 5 0\n";
            vertexes_ofs << "4 3 4 5 2\n";
            vertexes_ofs << "4 3 4 1 5\n";
            vertexes_ofs.close();
        }

        dictTetrahedra->load("dictTetrahedraTestFile");
        remove("dictTetrahedraTestFile_vertex.dat");
        remove("dictTetrahedraTestFile_neighbours.dat");

        dictTetrahedra->setPointDictionary(dictPoints);

        ASSERT_EQ(4, dictTetrahedra->getDataLength());
    }

    TEST_F(TetrahedronDictionaryTest, failLoad)
    {
        string output_file_neighbours = "dictTetrahedraTestFile_neighbours.dat";
        string output_file_vertexes = "dictTetrahedraTestFile_vertex.dat";
        ofstream neighbours_ofs(output_file_neighbours.c_str());
        ofstream vertexes_ofs(output_file_vertexes.c_str());
        if (neighbours_ofs.good())
        {
            neighbours_ofs << "4\n";
            neighbours_ofs << "4 -11 -18 1 2\n";
            neighbours_ofs << "4 -10 0 -19 3\n";
            neighbours_ofs << "4 0 -11 -18 3\n";
            neighbours_ofs << "4 1 -10 2 -19\n";
            neighbours_ofs.close();
        }

        if (vertexes_ofs.good())
        {
            vertexes_ofs << "4\n";
            vertexes_ofs << "4 4 5 2 0\n";
            vertexes_ofs << "4 4 1 5 0\n";
            vertexes_ofs << "4 3 4 5 2\n";
            vertexes_ofs << "4 3 4 1 5\n";
            vertexes_ofs.close();
        }

        dictTetrahedra->load("XdictTetrahedraTestFile");
        remove("dictTetrahedraTestFile_vertex.dat");
        remove("dictTetrahedraTestFile_neighbours.dat");

        ASSERT_EQ(0, dictTetrahedra->getDataLength());
    }

}