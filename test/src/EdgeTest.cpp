#include "../src/Edge.hpp"
#include "../src/TetrahedronDictionary.hpp"
#include "../src/PointDictionary.hpp"
#include "gtest/gtest.h"
#include <stdlib.h>
#include <vector>
#include <thread>
#include <cmath>

namespace EdgeTest
{
    class EdgeTest : public ::testing::Test
    {
    protected:
        EdgeTest() {}
        virtual ~EdgeTest() {}

        virtual void SetUp()
        {
            edge = new Edge(10, 15);
            //pointsDict = new PointDictionary();
        }
        virtual void TearDown()
        {
            // Code here will be called immediately after each test (right
            // before the destructor).
        }

        Edge *edge;
    };

    TEST_F(EdgeTest, edgeConstructor)
    {
        Edge *edge_2 = new Edge(10, 20);
        int *result = edge_2->getPointsId();
        ASSERT_EQ(10, result[0]);
        ASSERT_EQ(20, result[1]);
    }

    TEST_F(EdgeTest, Id)
    {
        edge->setId(3);
        ASSERT_EQ(3, edge->getId());
    }

    TEST_F(EdgeTest, setPointsId)
    {
        int aux[2] = {34, 56};
        edge->setPointsId(aux);
        int *result = edge->getPointsId();
        ASSERT_EQ(aux[0], result[0]);
        ASSERT_EQ(aux[1], result[1]);
    }

    TEST_F(EdgeTest, getPointsId)
    {
        int aux[2] = {10, 15};
        int *result = edge->getPointsId();
        ASSERT_EQ(aux[0], result[0]);
        ASSERT_EQ(aux[1], result[1]);
    }

    TEST_F(EdgeTest, tetrahedraId)
    {
        vector<int> aux = {90, 41, 2, -9, 23, 56, 1, 21};
        edge->setTetrahedraId(aux);
        vector<int> result = edge->getTetrahedraId();
        for (int it = 0; it < 8; it++)
        {
            ASSERT_EQ(aux[it], result[it]);
        }
    }

    TEST_F(EdgeTest, addTetrahedronId)
    {
        vector<int> aux = {90, 41, -10};
        edge->addTetrahedronId(90);
        edge->addTetrahedronId(41);
        edge->addTetrahedronId(-10);
        vector<int> result = edge->getTetrahedraId();
        for (int it = 0; it < 4; it++)
        {
            ASSERT_EQ(aux[it], result[it]);
        }
    }

    TEST_F(EdgeTest, setLength)
    {
        edge->setLength(20.5);
        ASSERT_EQ(20.5, edge->getLength());
    }

    TEST_F(EdgeTest, getLength)
    {
        PointDictionary *dictPoints = new PointDictionary();
        string output_file = "dictPointsTestFile.dat";
        ofstream ofs(output_file.c_str());
        if (ofs.good())
        {
            ofs << "3\n";
            ofs << "2\n";
            ofs << "0.0\t0.0\t0.0\n";
            ofs << "1.0\t1.0\t1.0\n";
            ofs.close();

            dictPoints->load("dictPointsTestFile");
            remove(output_file.c_str());
        }
        edge->setPointDictionary(dictPoints);
        int aux[2] = {0, 1};
        edge->setPointsId(aux);
        ASSERT_FLOAT_EQ(sqrt(3), edge->getLength());
    }

    TEST_F(EdgeTest, isInBorderFalse)
    {
        ASSERT_EQ(0, edge->isInBorder());
    }

    TEST_F(EdgeTest, isInBorderTrue)
    {
        edge->setInBorder(1);
        ASSERT_EQ(1, edge->isInBorder());
    }

}
